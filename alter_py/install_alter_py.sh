python_ver=${1:-3.7.9}
alter_py_dir=$2
dev_venv_pth=$3

echo "Making new venv for python ${python_ver}"

py_src_dir="Python-${python_ver}"
py_inst_dir="py${python_ver//./}"
installed_py_pth="${py_inst_dir}/bin/python${python_ver:0:1}"

echo  "${installed_py_pth}"

mkdir -p py_src

cd py_src

des_py_pkg_pth="${py_src_dir}.tgz"

if [ -f "${des_py_pkg_pth}" ]; then
  echo "${des_py_pkg_pth} has already been downloaded"
else
  echo "${des_py_pkg_pth} is being downloaded"
  wget "https://www.python.org/ftp/python/${python_ver}/Python-${python_ver}.tgz"
fi

rm -rf "${py_src_dir}"
tar -zxvf "${des_py_pkg_pth}"

cd ../



rm -rf "${py_inst_dir}"

cd "py_src/${py_src_dir}"

make clean
make distclean

./configure "--prefix=${alter_py_dir}/${py_inst_dir}"

make
make install -j12

cd ../../


$installed_py_pth -m venv "${dev_venv_pth}"
cd ../
echo "Activating venv"
source "${dev_venv_pth}/bin/activate"
echo "Upgrading pip"
pip install --upgrade pip --no-cache-dir
echo "Installing pendulum"
pip install pendulum --no-cache-dir
echo "Installing reqs"
pip install -r requirements.txt --no-cache-dir
deactivate
