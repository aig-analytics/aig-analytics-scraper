FROM python:3.7.9-slim-buster

RUN apt-get update -y && apt-get install gcc python3-dev -y \
    && apt-get clean && apt-get autoremove && rm -rf /var/lib/apt/lists/* \
    && mkdir -p /opt/aig-analytics-scraper/aig_app

COPY src /opt/aig-analytics-scraper/src
COPY requirements.txt /opt/aig-analytics-scraper/
COPY setup.py /opt/aig-analytics-scraper/
COPY setup_aig_venv.sh /opt/aig-analytics-scraper/
#RUN python -m venv /opt/aig-analytics-scraper/aig_analytics_scraper_venv
#RUN /bin/bash -c "source /opt/aig-analytics-scraper/aig_analytics_scraper_venv/bin/activate \
#RUN  /bin/bash -c "pip3 install --upgrade pip  \
#    && cd /opt/aig-analytics-scraper \
#    && pip install -r requirements.txt \
#    && python setup.py build_ext --inplace \
#    && python setup.py install \
#    && apt-get clean && apt-get autoremove"

#    && python setup.py sdist \
#    && python setup.py bdist_wheel \
#    && python setup.py build \
#    && python setup.py install \
#    && rm -rf src setup.py requirements.txt build dist src.egg-info /var/lib/apt/lists/* \

#ENTRYPOINT ["/bin/bash"]
ENTRYPOINT ["/opt/aig-analytics-scraper/setup_aig_venv.sh"]
