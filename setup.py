from setuptools import setup, find_packages

setup(
    name='src',
    version='0.1.0',
    description='Application to pull gaming sector information from many different sources',
    long_description='I would just open("README.md").read() here',
    author='n/a',
    author_email='n/a',
    url='https://github.com/simmyyy/aig-analytics-scrapper',
    packages=find_packages(exclude=['*tests*', 'db_scripts'])
)

## example usages:
# python setup.py bdist_egg

## example run on cluster (put in run_daily_scrapper.sh)
# !/bin/sh

# PYTHONPATH=gstock-0.1.0-py3.6.egg python3 -m src
# --app.run.wse.comp.kp True
# --app.run.steam.lkp.refresh False
# --app.run.steam.lkp False
# --app.run.steam.data False
# --app.run.steam.stats False
# --app.run.ms.data False
# --app.run.steam.members False
# --app.run.config.file /home/simmy-admin/gstock_scrapper/config.yaml


## example cron entry
# 12 23 * * * psql -d gamesanalytics -U simmy < /home/simmy-admin/daily.sql
# 0 18 * * * /home/simmy-admin/gstock_scrapper/run_daily_scrapper.sh

