#!/bin/bash
#apt-get update -y
#apt-get install gcc python3-dev -y

if [ -d "/opt/aig-analytics-scraper/src" ]
then
    echo "Rebuilding from source!"
    python -m venv /opt/aig-analytics-scraper/aig_analytics_scraper_venv
    source /opt/aig-analytics-scraper/aig_analytics_scraper_venv/bin/activate
    pip install --upgrade pip
    cd /opt/aig-analytics-scraper
    pip install -r requirements.txt
    pip install numba==0.48
    python setup.py build_ext --inplace
    python setup.py install
    rm -rf src setup.py requirements.txt build dist src.egg-info /var/lib/apt/lists/*
    apt-get clean
    apt-get autoremove
else
    echo "Package already built!"

fi

tail -f /dev/null