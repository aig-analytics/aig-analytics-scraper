create sequence datamart.company_company_id_seq
	as integer;

alter sequence datamart.company_company_id_seq owner to simmy;

grant select, update, usage on sequence datamart.company_company_id_seq to kw;

grant select, update, usage on sequence datamart.company_company_id_seq to public;

grant select, update, usage on sequence datamart.company_company_id_seq to simmystage;

grant select, update, usage on sequence datamart.company_company_id_seq to simmydatamart;

create sequence datamart.steam_top_whishlist_id_seq
	as integer;

alter sequence datamart.steam_top_whishlist_id_seq owner to simmy;

create sequence datamart.steam_top_bestsellers_id_seq
	as integer;

alter sequence datamart.steam_top_bestsellers_id_seq owner to simmy;

create sequence datamart.steam_app_info_id_seq
	as integer;

alter sequence datamart.steam_app_info_id_seq owner to simmy;

create sequence datamart.whishlist_estimated_adds_id_seq
	as integer;

alter sequence datamart.whishlist_estimated_adds_id_seq owner to simmy;

create sequence datamart.steam_app_live_stats_id_seq
	as integer;

alter sequence datamart.steam_app_live_stats_id_seq owner to simmy;

create sequence datamart.process_log_log_id_seq
	as integer;

alter sequence datamart.process_log_log_id_seq owner to simmy;

