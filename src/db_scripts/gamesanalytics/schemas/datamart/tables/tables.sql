create table if not exists datamart.company
(
	company_id serial not null
		constraint company_pkey
			primary key,
	company_name varchar(256),
	steam_company_name varchar(256),
	country varchar(256),
	created_time varchar(256)
);

alter table datamart.company owner to simmy;

grant select, update, usage on sequence datamart.company_company_id_seq to kw;

grant select, update, usage on sequence datamart.company_company_id_seq to public;

grant select, update, usage on sequence datamart.company_company_id_seq to simmystage;

grant select, update, usage on sequence datamart.company_company_id_seq to simmydatamart;

grant insert, select, update, delete, truncate, references, trigger on datamart.company to kw;

create table if not exists datamart.steam_top_whishlist
(
	id serial not null,
	appid varchar(256),
	itemkey varchar(256),
	position varchar(10),
	href varchar(1024),
	title varchar(256),
	release_date varchar(256),
	price varchar(256),
	estimated_adds varchar(256),
	estimated_sellof_ratio varchar(256),
	estimated_sells varchar(256),
	created_date varchar(256),
	created_dy varchar(256),
	last_modified_date varchar(256),
	last_modified_by varchar(256),
	constraint steam_top_whishlist_appid_created_date_key
		unique (appid, created_date)
);

alter table datamart.steam_top_whishlist owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_top_whishlist to kw;

create table if not exists datamart.steam_top_bestsellers
(
	id serial not null,
	appid varchar(256),
	itemkey varchar(256),
	position varchar(10),
	href varchar(1024),
	title varchar(256),
	release_date varchar(256),
	price varchar(256),
	created_date varchar(256),
	created_dy varchar(256),
	last_modified_date varchar(256),
	last_modified_by varchar(256),
	constraint steam_top_bestsellers_appid_itemkey_created_date_key
		unique (appid, itemkey, created_date)
);

alter table datamart.steam_top_bestsellers owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_top_bestsellers to kw;

create table if not exists datamart.steam_app_info
(
	id serial not null,
	appid varchar(2048)
		constraint steam_app_info_appid_key
			unique,
	title varchar(2048),
	app_general_type varchar(2048),
	app_category varchar(2048),
	game_description_snippet varchar(2048),
	producer varchar(2048),
	publisher varchar(2048),
	steam_reviews_summary varchar(2048),
	steam_reviews_description varchar(2048),
	review_count varchar(2048),
	rating_value varchar(2048),
	best_rating varchar(2048),
	worst_rating varchar(2048),
	popular_tags varchar(2048),
	created_date varchar(2048)
);

alter table datamart.steam_app_info owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_app_info to kw;

create table if not exists datamart.whishlist_estimated_adds
(
	id serial not null,
	position varchar(256),
	estimated_adds varchar(256),
	created_date varchar(256)
);

alter table datamart.whishlist_estimated_adds owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on datamart.whishlist_estimated_adds to kw;

create table if not exists datamart.company_market_information
(
	company_id integer
		constraint company_market_information_company_id_fkey
			references datamart.company,
	free_float double precision,
	stock_price numeric(16,2),
	stock_amount integer,
	capitalization numeric(16,2),
	created_time varchar(256)
);

alter table datamart.company_market_information owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on datamart.company_market_information to kw;

create table if not exists datamart.steam_app_live_stats
(
	id serial not null
		constraint steam_app_live_stats_pkey
			primary key,
	appid varchar(256),
	active_players varchar(256),
	active_group_chat varchar(256),
	created_date varchar(256),
	constraint steam_app_live_stats_appid_created_date_key
		unique (appid, created_date)
);

alter table datamart.steam_app_live_stats owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_app_live_stats to kw;

create table if not exists datamart.steam_app_info_lkp
(
	appid varchar(128) not null
		constraint steam_app_info_lkp_pkey
			primary key,
	title varchar(1024),
	header_image varchar(2048),
	publishers varchar(2048),
	developers varchar(2048),
	created_date varchar(128),
	created_by varchar(128),
	last_modified_date varchar(128),
	last_modified_by varchar(128)
);

alter table datamart.steam_app_info_lkp owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_app_info_lkp to kw;

create table if not exists datamart.ms_app_info
(
	pid varchar(256) not null
		constraint ms_app_info_pkey
			primary key,
	title varchar(2048),
	prod_category varchar(2048),
	tags varchar(2048),
	publisher varchar(2048),
	developer varchar(2048),
	release_date varchar(256),
	estimated_size varchar(256),
	price varchar(256),
	created_date varchar(256),
	created_by varchar(256),
	last_modified_date varchar(256),
	last_modified_by varchar(256)
);

alter table datamart.ms_app_info owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on datamart.ms_app_info to kw;

create table if not exists datamart.bankier_company_pull_lkp
(
	wse_comp_shortcut varchar(16) not null
		constraint bankier_company_pull_lkp_pkey
			primary key,
	wse_comp_name varchar(1024),
	wse_market varchar(1024),
	bnk_comp_shortcut varchar(16),
	bnk_comp_name varchar(1024),
	search_cmp_name varchar(1024),
	comment varchar(1024),
	created_date varchar(1024),
	created_by varchar(1024),
	last_updated_time varchar(1024),
	last_updated_by varchar(1024)
);

alter table datamart.bankier_company_pull_lkp owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on datamart.bankier_company_pull_lkp to kw;

create table if not exists datamart.ms_ranked_title
(
	data_aid varchar(256),
	app_info_href varchar(2048),
	pid varchar(256),
	title varchar(2048),
	rating_value varchar(256),
	best_rating varchar(256),
	review_count varchar(256),
	position varchar(256),
	country varchar(256),
	ranking_type varchar(256),
	device_type varchar(256),
	created_date varchar(256),
	created_by varchar(256),
	last_modified_date varchar(256),
	last_modified_by varchar(256),
	constraint ms_ranked_title_pid_created_date_key
		unique (pid, created_date)
);

alter table datamart.ms_ranked_title owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on datamart.ms_ranked_title to kw;

create table if not exists datamart.wse_company_stock_info
(
	banker_id varchar(128),
	banker_shortcut varchar(128),
	stock_price numeric,
	capitalization numeric,
	free_float double precision,
	stock_amount integer,
	price_to_profit numeric,
	price_to_book_value numeric,
	created_date varchar(1024),
	created_by varchar(1024),
	last_updated_time varchar(1024),
	last_updated_by varchar(1024),
	constraint wse_company_stock_info_banker_shortcut_created_date_key
		unique (banker_shortcut, created_date)
);

alter table datamart.wse_company_stock_info owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on datamart.wse_company_stock_info to kw;

create table if not exists datamart.free_proxy_list
(
	hostname varchar(100),
	port varchar(16),
	code varchar(16),
	country varchar(200),
	anonimity varchar(200),
	google varchar(16),
	https varchar(16),
	last_checked varchar(100),
	is_active varchar(16),
	created_date varchar(16),
	created_by varchar(16),
	last_modified_date varchar(16),
	last_modified_by varchar(16)
);

alter table datamart.free_proxy_list owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on datamart.free_proxy_list to kw;

create table if not exists datamart.steam_members_reviews
(
	player_id varchar not null,
	appid varchar not null,
	review varchar,
	posted_dts varchar,
	edited_dts varchar,
	hours_played varchar,
	hours_when_reved varchar,
	created_date varchar not null,
	created_by varchar,
	last_modified_date varchar,
	last_modified_by varchar,
	constraint steam_members_reviews_pkey
		primary key (player_id, appid, created_date)
);

alter table datamart.steam_members_reviews owner to kw;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_reviews to simmystage;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_reviews to simmydatamart;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_reviews to simmy;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_reviews to public;

create table if not exists datamart.steam_members_fllwd_games
(
	game_name varchar not null,
	player_id varchar not null,
	created_date varchar not null,
	created_by varchar,
	last_modified_date varchar,
	last_modified_by varchar,
	constraint steam_members_fllwd_games_pkey
		primary key (player_id, game_name, created_date)
);

alter table datamart.steam_members_fllwd_games owner to kw;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_fllwd_games to public;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_fllwd_games to simmy;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_fllwd_games to simmydatamart;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_fllwd_games to simmystage;

create table if not exists datamart.steam_members_rec_played
(
	appid varchar not null,
	name varchar,
	last_played varchar,
	hours varchar,
	hours_forever varchar,
	friendlyurl varchar,
	player_id varchar not null,
	created_date varchar not null,
	created_by varchar,
	last_modified_date varchar,
	last_modified_by varchar,
	constraint steam_members_rec_played_pkey
		primary key (player_id, appid, created_date)
);

alter table datamart.steam_members_rec_played owner to kw;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_rec_played to public;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_rec_played to simmy;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_rec_played to simmydatamart;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_rec_played to simmystage;

create table if not exists datamart.steam_members_games
(
	appid varchar not null,
	name varchar,
	friendly_name varchar,
	has_adult_content varchar,
	friendlyurl varchar,
	hours varchar,
	hours_forever varchar,
	last_played varchar,
	player_id varchar not null,
	created_date varchar not null,
	created_by varchar,
	last_modified_date varchar,
	last_modified_by varchar,
	constraint steam_members_games_pkey
		primary key (player_id, appid, created_date)
);

alter table datamart.steam_members_games owner to kw;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_games to simmydatamart;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_games to simmy;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_games to public;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_games to simmystage;

create table if not exists datamart.steam_mebers_players
(
	player_url varchar,
	player_id varchar not null
		constraint steam_mebers_players_pkey
			primary key,
	last_time_scraped varchar,
	to_be_scraped varchar,
	created_date varchar,
	created_by varchar,
	last_modified_date varchar,
	last_modified_by varchar
);

alter table datamart.steam_mebers_players owner to kw;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_mebers_players to simmydatamart;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_mebers_players to simmy;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_mebers_players to public;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_mebers_players to simmystage;

create table if not exists datamart.steam_members_players
(
	player_url varchar,
	player_id varchar not null
		constraint steam_members_players_pkey
			primary key,
	to_be_scraped varchar,
	player_lvl varchar,
	player_location varchar,
	created_date varchar,
	created_by varchar,
	last_modified_date varchar,
	last_modified_by varchar
);

alter table datamart.steam_members_players owner to kw;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_players to simmydatamart;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_players to simmystage;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_players to public;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_members_players to simmy;

create table if not exists datamart.steam_members_wishlist
(
	appid varchar not null,
	added varchar,
	priority varchar,
	player_id varchar not null,
	created_date varchar not null,
	created_by varchar,
	last_modified_date varchar,
	last_modified_by varchar,
	constraint steam_members_wishlist_pkey
		primary key (player_id, appid, created_date)
);

alter table datamart.steam_members_wishlist owner to kw;

create table if not exists datamart.process_log
(
	log_id serial not null
		constraint process_log_pkey
			primary key,
	run_id integer,
	process_name varchar(256),
	method_name varchar(256),
	created_date timestamp,
	created_by varchar(256),
	last_modified_date timestamp,
	last_modified_by varchar(256),
	log_message varchar(2048)
);

alter table datamart.process_log owner to simmy;

