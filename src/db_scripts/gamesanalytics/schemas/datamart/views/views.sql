create view datamart.steam_app_info_vw(id, appid, title, app_general_type, app_category, game_description_snippet, producer, publisher, steam_reviews_summary, steam_reviews_description, review_count, rating_value, best_rating, worst_rating, popular_tags, created_date) as
	SELECT steam_app_info.id,
       steam_app_info.appid,
       steam_app_info.title,
       steam_app_info.app_general_type,
       steam_app_info.app_category,
       steam_app_info.game_description_snippet,
       steam_app_info.producer,
       steam_app_info.publisher,
       steam_app_info.steam_reviews_summary,
       steam_app_info.steam_reviews_description,
       steam_app_info.review_count,
       steam_app_info.rating_value,
       steam_app_info.best_rating,
       steam_app_info.worst_rating,
       steam_app_info.popular_tags,
       steam_app_info.created_date
FROM datamart.steam_app_info;

alter table datamart.steam_app_info_vw owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on datamart.steam_app_info_vw to kw;

