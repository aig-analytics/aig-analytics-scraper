create table steam_top_whishlist_stg
(
	appid varchar(256),
	itemkey varchar(256),
	position varchar(10),
	href varchar(1024),
	title varchar(256),
	release_date varchar(256),
	price varchar(256),
	created_date varchar(256)
);

alter table steam_top_whishlist_stg owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on steam_top_whishlist_stg to kw;

create table steam_top_bestsellers_stg
(
	appid varchar(256),
	itemkey varchar(256),
	position varchar(10),
	href varchar(1024),
	title varchar(256),
	release_date varchar(256),
	price varchar(256),
	created_date varchar(256)
);

alter table steam_top_bestsellers_stg owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on steam_top_bestsellers_stg to kw;

create table steam_app_info_stg
(
	appid varchar(2048),
	title varchar(2048),
	app_general_type varchar(2048),
	app_category varchar(2048),
	game_description_snippet varchar(2048),
	producer varchar(2048),
	publisher varchar(2048),
	steam_reviews_summary varchar(2048),
	steam_reviews_description varchar(2048),
	review_count varchar(2048),
	rating_value varchar(2048),
	best_rating varchar(2048),
	worst_rating varchar(2048),
	popular_tags varchar(2048),
	created_date varchar(2048)
);

alter table steam_app_info_stg owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on steam_app_info_stg to kw;

create table steam_app_live_stats
(
	appid varchar(256),
	active_players varchar(256),
	active_group_chat varchar(256),
	created_date varchar(256)
);

alter table steam_app_live_stats owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on steam_app_live_stats to kw;

create table ms_app_info
(
	pid varchar(2048),
	prod_category varchar(2048),
	tags varchar(2048),
	publisher varchar(2048),
	developer varchar(2048),
	release_date varchar(2048),
	estimated_size varchar(2048),
	price varchar(2048),
	created_date varchar(2048),
	title varchar(2048)
);

alter table ms_app_info owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on ms_app_info to kw;

create table ms_ranked_title
(
	data_aid varchar(2048),
	app_info_href varchar(2048),
	pid varchar(2048),
	title varchar(2048),
	rating_value varchar(2048),
	best_rating varchar(2048),
	review_count varchar(2048),
	position varchar(2048),
	country varchar(2048),
	ranking_type varchar(2048),
	device_type varchar(2048),
	created_date varchar(2048)
);

alter table ms_ranked_title owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on ms_ranked_title to kw;

create table wse_company_stock_info
(
	banker_id varchar(128),
	banker_shortcut varchar(128),
	stock_price varchar(128),
	capitalization varchar(128),
	free_float varchar(128),
	stock_amount varchar(128),
	price_to_profit varchar(128),
	price_to_book_value varchar(128),
	created_date varchar(128)
);

alter table wse_company_stock_info owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on wse_company_stock_info to kw;

create table steam_members_reviews
(
	player_id varchar not null,
	appid varchar not null,
	scraped_dt varchar not null,
	review varchar,
	posted_dts varchar,
	edited_dts varchar,
	hours_played varchar,
	hours_when_reved varchar,
	constraint steam_members_reviews_pkey
		primary key (player_id, appid, scraped_dt)
);

alter table steam_members_reviews owner to kw;

grant insert, select, update, delete, truncate, references, trigger on steam_members_reviews to simmy;

grant insert, select, update, delete, truncate, references, trigger on steam_members_reviews to simmystage;

grant insert, select, update, delete, truncate, references, trigger on steam_members_reviews to public;

grant insert, select, update, delete, truncate, references, trigger on steam_members_reviews to simmydatamart;

create table steam_members_fllwd_games
(
	game_name varchar not null,
	player_id varchar not null,
	scraped_dt varchar not null,
	constraint steam_members_fllwd_games_pkey
		primary key (player_id, game_name, scraped_dt)
);

alter table steam_members_fllwd_games owner to kw;

grant insert, select, update, delete, truncate, references, trigger on steam_members_fllwd_games to public;

grant insert, select, update, delete, truncate, references, trigger on steam_members_fllwd_games to simmy;

grant insert, select, update, delete, truncate, references, trigger on steam_members_fllwd_games to simmydatamart;

grant insert, select, update, delete, truncate, references, trigger on steam_members_fllwd_games to simmystage;

create table steam_members_rec_played
(
	appid varchar not null,
	scraped_dt varchar not null,
	name varchar,
	last_played varchar,
	hours varchar,
	hours_forever varchar,
	friendlyurl varchar,
	player_id varchar not null,
	constraint steam_members_rec_played_pkey
		primary key (player_id, appid, scraped_dt)
);

alter table steam_members_rec_played owner to kw;

grant insert, select, update, delete, truncate, references, trigger on steam_members_rec_played to simmystage;

grant insert, select, update, delete, truncate, references, trigger on steam_members_rec_played to public;

grant insert, select, update, delete, truncate, references, trigger on steam_members_rec_played to simmy;

grant insert, select, update, delete, truncate, references, trigger on steam_members_rec_played to simmydatamart;

create table steam_members_games
(
	appid varchar not null,
	scraped_dt varchar not null,
	name varchar,
	friendly_name varchar,
	has_adult_content varchar,
	friendlyurl varchar,
	hours varchar,
	hours_forever varchar,
	last_played varchar,
	player_id varchar not null,
	constraint steam_members_games_pkey
		primary key (player_id, appid, scraped_dt)
);

alter table steam_members_games owner to kw;

grant insert, select, update, delete, truncate, references, trigger on steam_members_games to simmydatamart;

grant insert, select, update, delete, truncate, references, trigger on steam_members_games to simmystage;

grant insert, select, update, delete, truncate, references, trigger on steam_members_games to public;

grant insert, select, update, delete, truncate, references, trigger on steam_members_games to simmy;

create table steam_members_wishlist
(
	appid varchar not null,
	scraped_dt varchar not null,
	added varchar,
	priority varchar,
	player_id varchar not null,
	constraint steam_members_wishlist_pkey
		primary key (player_id, appid, scraped_dt)
);

alter table steam_members_wishlist owner to kw;

grant insert, select, update, delete, truncate, references, trigger on steam_members_wishlist to simmy;

grant insert, select, update, delete, truncate, references, trigger on steam_members_wishlist to public;

grant insert, select, update, delete, truncate, references, trigger on steam_members_wishlist to simmystage;

grant insert, select, update, delete, truncate, references, trigger on steam_members_wishlist to simmydatamart;

create table steam_members_players
(
	player_url varchar,
	player_id varchar,
	player_lvl varchar,
	player_location varchar,
	created_date varchar,
	last_modified_date varchar,
	to_be_scraped varchar
);

alter table steam_members_players owner to kw;

grant insert, select, update, delete, truncate, references, trigger on steam_members_players to simmy;

grant insert, select, update, delete, truncate, references, trigger on steam_members_players to public;

grant insert, select, update, delete, truncate, references, trigger on steam_members_players to simmystage;

grant insert, select, update, delete, truncate, references, trigger on steam_members_players to simmydatamart;

create table steam_app_info_lkp_stg
(
	appid varchar(2048),
	title varchar(2048),
	header_image varchar(2048),
	publishers varchar(2048),
	developers varchar(2048),
	created_date varchar(2048),
	price varchar(2048)
);

alter table steam_app_info_lkp_stg owner to simmy;

grant insert, select, update, delete, truncate, references, trigger on steam_app_info_lkp_stg to kw;

