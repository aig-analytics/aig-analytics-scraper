create or replace function update_log_table(p_run_id integer, p_process_name character varying, p_method_name character varying, p_log_msg character varying) returns integer
	language plpgsql
as $$
declare
    d int;
BEGIN
    INSERT INTO datamart.PROCESS_LOG (run_id, process_name, method_name, created_date, created_by, last_modified_date, last_modified_by, log_message)
    VALUES (
            p_run_id,
            p_process_name,
            p_method_name,
            current_timestamp,
            'SYSTEM',
            current_timestamp,
            'SYSTEM',
            p_log_msg
           );

    RETURN 1;

END;
$$;

alter function update_log_table(integer, varchar, varchar, varchar) owner to simmy;

