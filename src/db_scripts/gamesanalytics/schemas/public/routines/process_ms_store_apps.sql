create or replace function process_ms_store_apps(p_run_id integer) returns integer
	language plpgsql
as $$
declare
    _message TEXT;
    _context TEXT;
BEGIN

    PERFORM update_log_table(p_run_id, 'PROCESS_MS_STORE_APPS', 'MS_APP_INFO', 'STARTED');

    INSERT INTO datamart.MS_APP_INFO (PID, TITLE, PROD_CATEGORY, TAGS, PUBLISHER, DEVELOPER, RELEASE_DATE,
                                      ESTIMATED_SIZE, PRICE, CREATED_DATE, CREATED_BY, LAST_MODIFIED_DATE,
                                      LAST_MODIFIED_BY)
    SELECT PID,
           TITLE,
           PROD_CATEGORY,
           TAGS,
           PUBLISHER,
           DEVELOPER,
           RELEASE_DATE,
           ESTIMATED_SIZE,
           PRICE,
           CREATED_DATE,
           'STG_PROCESS',
           CREATED_DATE,
           'STG_PROCESS'
    FROM (SELECT *
          FROM (SELECT row_number() over (PARTITION BY PID ORDER BY created_date DESC) AS rnk, *
                FROM stage.MS_APP_INFO) AS tmp
          WHERE rnk = 1) AS res
    ON CONFLICT (PID) DO UPDATE SET title              = excluded.title,
                                    prod_category      = excluded.prod_category,
                                    tags               = excluded.tags,
                                    publisher          = excluded.publisher,
                                    developer          = excluded.developer,
                                    release_date       = excluded.release_date,
                                    estimated_size     = excluded.estimated_size,
                                    price              = excluded.price,
                                    last_modified_date = excluded.created_date,
                                    last_modified_by   = excluded.last_modified_by;

    PERFORM update_log_table(p_run_id, 'PROCESS_MS_STORE_APPS', 'MS_APP_INFO', 'FINISHED');

    PERFORM update_log_table(p_run_id, 'PROCESS_MS_STORE_APPS', 'MS_RANKED_TITLE', 'STARTED');

    INSERT INTO datamart.MS_RANKED_TITLE (data_aid, app_info_href, pid, title, rating_value, best_rating, review_count,
                                          position, country, ranking_type, device_type, created_date, created_by,
                                          last_modified_date, last_modified_by)
    SELECT data_aid,
           app_info_href,
           pid,
           title,
           rating_value,
           best_rating,
           review_count,
           position,
           country,
           ranking_type,
           device_type,
           created_date,
           'STG_PROCESS',
           created_date,
           'STG_PROCESS'
    FROM (SELECT *
          FROM (SELECT row_number() over (PARTITION BY PID, created_date ORDER BY created_date DESC) AS rnk, *
                FROM stage.MS_RANKED_TITLE) AS tmp
          WHERE rnk = 1) AS res
    ON CONFLICT (pid, created_date) DO UPDATE SET data_aid           = excluded.data_aid,
                                                  app_info_href      = excluded.app_info_href,
                                                  title              = excluded.title,
                                                  rating_value       = excluded.rating_value,
                                                  best_rating        = excluded.best_rating,
                                                  review_count       = excluded.review_count,
                                                  position           = excluded.position,
                                                  country            = excluded.country,
                                                  ranking_type       = excluded.ranking_type,
                                                  device_type        = excluded.device_type,
                                                  last_modified_date = excluded.created_date,
                                                  last_modified_by   = excluded.last_modified_by;

    PERFORM update_log_table(p_run_id, 'PROCESS_MS_STORE_APPS', 'MS_RANKED_TITLE', 'FINISHED');

    PERFORM update_log_table(p_run_id, 'PROCESS_MS_STORE_APPS', 'MS_APP_INFO', 'STARTED');

    UPDATE datamart.MS_APP_INFO tr
    SET title = (SELECT TITLE
                 FROM (SELECT *
                       FROM (SELECT row_number()
                                    over (PARTITION BY PID ORDER BY created_date DESC) AS rnk,
                                    *
                             FROM stage.MS_RANKED_TITLE) AS tmp
                       WHERE rnk = 1) AS res
                 WHERE tr.pid = pid);

    PERFORM update_log_table(p_run_id, 'PROCESS_MS_STORE_APPS', 'MS_RANKED_TITLE', 'FINISHED');

    RETURN 0;

EXCEPTION
    WHEN OTHERS THEN
        GET STACKED DIAGNOSTICS
            _message := MESSAGE_TEXT,
            _context := PG_EXCEPTION_CONTEXT;
        PERFORM update_log_table(p_run_id, 'PROCESS_MS_STORE_APPS', NULL, _message || _context);
        RETURN 1;

END;
$$;

alter function process_ms_store_apps(integer) owner to simmy;

