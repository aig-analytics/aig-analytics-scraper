create or replace function process_stg_scrapper_daily_data() returns integer
    language plpgsql
as
$$
declare
    log_msg              varchar;
    p_run_id             int;
    p_last_process_state int;
    _message             TEXT;
    _context             TEXT;
BEGIN

    SELECT COALESCE(log_message, 'FINISHED')
    INTO log_msg
    FROM datamart.process_log
    WHERE log_id = (SELECT MAX(log_id) FROM datamart.process_log);

    IF log_msg != 'FINISHED'
    THEN
        RETURN 2;
    END IF;

    SELECT nextval('process_stg_scrapper_daily_data_process_id_seq') INTO p_run_id;

    PERFORM update_log_table(p_run_id, 'STG_SCRAPPER_DAILY_LOAD', NULL, 'STARTED');

    PERFORM update_log_table(p_run_id, 'PROCESS_STOCK_DATA', NULL, 'STARTED');

    SELECT process_stock_data(p_run_id) INTO p_last_process_state;

    IF p_last_process_state != 0
    THEN
        RETURN 2;
    END IF;

    PERFORM update_log_table(p_run_id, 'PROCESS_STOCK_DATA', NULL, 'FINISHED');


    PERFORM update_log_table(p_run_id, 'PROCESS_STEAM_APPS', NULL, 'STARTED');

    SELECT process_steam_apps(p_run_id) INTO p_last_process_state;

    IF p_last_process_state != 0
    THEN
        RETURN 2;
    END IF;

    PERFORM update_log_table(p_run_id, 'PROCESS_STEAM_APPS', NULL, 'FINISHED');


    PERFORM update_log_table(p_run_id, 'PROCESS_STEAM_MEMBERS', NULL, 'STARTED');

    SELECT process_steam_members(p_run_id) INTO p_last_process_state;

    IF p_last_process_state != 0
    THEN
        RETURN 2;
    END IF;

    PERFORM update_log_table(p_run_id, 'PROCESS_STEAM_MEMBERS', NULL, 'FINISHED');


    PERFORM update_log_table(p_run_id, 'PROCESS_MS_STORE_APPS', NULL, 'STARTED');

    SELECT process_ms_store_apps(p_run_id) INTO p_last_process_state;

    IF p_last_process_state != 0
    THEN
        RETURN 2;
    END IF;

    PERFORM update_log_table(p_run_id, 'PROCESS_MS_STORE_APPS', NULL, 'FINISHED');

    PERFORM update_log_table(p_run_id, 'STG_SCRAPPER_DAILY_LOAD', NULL, 'FINISHED');

    RETURN 0;

EXCEPTION
    WHEN OTHERS THEN
        GET STACKED DIAGNOSTICS
            _message := MESSAGE_TEXT,
            _context := PG_EXCEPTION_CONTEXT;
        PERFORM update_log_table(p_run_id, 'STG_SCRAPPER_DAILY_LOAD', NULL, _message || _context);
        RETURN 1;
END ;
$$;

alter function process_stg_scrapper_daily_data() owner to simmy;

