create or replace function process_stock_data(p_run_id integer) returns integer
	language plpgsql
as $$
declare
    _message TEXT;
    _context TEXT;
BEGIN

    PERFORM update_log_table(p_run_id, 'PROCESS_STOCK_DATA', 'WSE_COMPANY_STOCK_INFO', 'STARTED');

    UPDATE stage.wse_company_stock_info
    SET stock_price         = replace(replace(stock_price, ' ', ''), ',', '.'),
        capitalization      = replace(replace(capitalization, ' ', ''), ',', '.'),
        free_float          = replace(replace(free_float, ' ', ''), ',', '.'),
        price_to_profit     = replace(replace(price_to_profit, ' ', ''), ',', '.'),
        price_to_book_value = replace(replace(price_to_book_value, ' ', ''), ',', '.');

    INSERT INTO datamart.WSE_COMPANY_STOCK_INFO (banker_id, banker_shortcut, stock_price, capitalization, free_float,
                                                 stock_amount, price_to_profit, price_to_book_value, created_date,
                                                 created_by, last_updated_time, last_updated_by)
    SELECT banker_id,
           banker_shortcut,
           CAST(stock_price AS DECIMAL),
           CAST(capitalization AS DECIMAL),
           CAST(free_float AS FLOAT),
           CAST(stock_amount AS INT),
           CAST(nullif(replace(price_to_profit, 'UNK', ''), '') AS DECIMAL),
           CAST(nullif(replace(price_to_book_value, 'UNK', ''), '') AS DECIMAL),
           created_date,
           'STG_PROCESS',
           created_date,
           'STG_PROCESS'
    FROM (SELECT *
          FROM (SELECT row_number() over (PARTITION BY banker_shortcut, created_date ORDER BY created_date DESC) AS rnk,
                       *
                FROM stage.wse_company_stock_info) AS tmp
          WHERE rnk = 1) AS res
    ON CONFLICT (banker_shortcut, created_date) DO UPDATE SET banker_id           = excluded.banker_id,
                                                              stock_price         = excluded.stock_price,
                                                              capitalization      = excluded.capitalization,
                                                              free_float          = excluded.free_float,
                                                              stock_amount        = excluded.stock_price,
                                                              price_to_profit     = excluded.price_to_profit,
                                                              price_to_book_value = excluded.price_to_book_value,
                                                              last_updated_time   = excluded.created_date,
                                                              last_updated_by     = 'STG_PROCESS';

    PERFORM update_log_table(p_run_id, 'PROCESS_STOCK_DATA', 'WSE_COMPANY_STOCK_INFO', 'FINISHED');

    RETURN 0;

EXCEPTION
    WHEN OTHERS THEN
        GET STACKED DIAGNOSTICS
            _message := MESSAGE_TEXT,
            _context := PG_EXCEPTION_CONTEXT;
        PERFORM update_log_table(p_run_id, 'PROCESS_STOCK_DATA', NULL, _message || _context);
        RETURN 1;

END;
$$;

alter function process_stock_data(integer) owner to simmy;

