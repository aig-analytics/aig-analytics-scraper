create or replace function process_steam_apps(p_run_id integer) returns integer
	language plpgsql
as $$
declare
    _message TEXT;
    _context TEXT;
BEGIN

    PERFORM update_log_table(p_run_id, ''PROCESS_STEAM_APPS'', ''STEAM_TOP_WHISHLIST'', ''STARTED'');

    INSERT INTO datamart.STEAM_TOP_WHISHLIST (APPID, ITEMKEY, POSITION, HREF, TITLE, RELEASE_DATE, PRICE, CREATED_DATE,
                                              CREATED_DY, LAST_MODIFIED_DATE, LAST_MODIFIED_BY)
    SELECT APPID,
           ITEMKEY,
           POSITION,
           HREF,
           TITLE,
           RELEASE_DATE,
           CASE
               WHEN (POSITION(''zł'' in PRICE) > 0) THEN SUBSTRING(PRICE, POSITION(''zł'' in PRICE) + 2, LENGTH(PRICE))
               WHEN (POSITION(''FREE'' in UPPER(PRICE)) > 0) THEN ''0''
               ELSE ''UNK''
               END AS PRICE,
           CREATED_DATE,
           ''STG_PROCESS'',
           CREATED_DATE,
           ''STG_PROCESS''
    FROM (SELECT *
          FROM (SELECT row_number() over (PARTITION BY appid, created_date ORDER BY created_date DESC) AS rnk, *
                FROM stage.steam_top_whishlist_stg) tmp
          WHERE rnk = 1) dt
    ON CONFLICT (APPID, CREATED_DATE) DO UPDATE SET ITEMKEY      = excluded.itemkey,
                                                    POSITION     = excluded.position,
                                                    HREF         = excluded.href,
                                                    TITLE        = excluded.title,
                                                    RELEASE_DATE = excluded.release_date,
                                                    PRICE        = excluded.price;

    PERFORM update_log_table(p_run_id, ''PROCESS_STEAM_APPS'', ''STEAM_TOP_WHISHLIST'', ''FINISHED'');

    PERFORM update_log_table(p_run_id, ''PROCESS_STEAM_APPS'', ''STEAM_APP_INFO'', ''STARTED'');

    INSERT INTO datamart.STEAM_APP_INFO (appid, title, app_general_type, app_category, game_description_snippet,
                                         producer,
                                         publisher, steam_reviews_summary, steam_reviews_description, review_count,
                                         rating_value, best_rating, worst_rating, popular_tags, created_date)
    SELECT appid,
           title,
           app_general_type,
           app_category,
           game_description_snippet,
           producer,
           publisher,
           steam_reviews_summary,
           steam_reviews_description,
           review_count,
           rating_value,
           best_rating,
           worst_rating,
           popular_tags,
           created_date
    FROM (SELECT *
          FROM (SELECT row_number() over (PARTITION BY appid ORDER BY created_date DESC) AS rnk, *
                FROM stage.steam_app_info_stg) AS tmp
          WHERE rnk = 1) AS res
    ON CONFLICT (appid) DO UPDATE SET title                     = excluded.title,
                                      app_general_type          = excluded.app_general_type,
                                      app_category              = excluded.app_category,
                                      game_description_snippet  = excluded.game_description_snippet,
                                      producer                  = excluded.producer,
                                      publisher                 = excluded.publisher,
                                      steam_reviews_summary     = excluded.steam_reviews_summary,
                                      steam_reviews_description = excluded.steam_reviews_description,
                                      review_count              = excluded.review_count,
                                      rating_value              = excluded.rating_value,
                                      best_rating               = excluded.best_rating,
                                      worst_rating              = excluded.worst_rating,
                                      popular_tags              = excluded.popular_tags,
                                      created_date              = excluded.created_date;

    PERFORM update_log_table(p_run_id, ''PROCESS_STEAM_APPS'', ''STEAM_APP_INFO'', ''FINISHED'');

    PERFORM update_log_table(p_run_id, ''PROCESS_STEAM_APPS'', ''STEAM_TOP_BESTSELLERS'', ''STARTED'');

    INSERT INTO datamart.STEAM_TOP_BESTSELLERS (APPID, ITEMKEY, POSITION, HREF, TITLE, RELEASE_DATE, PRICE,
                                                CREATED_DATE,
                                                CREATED_DY, LAST_MODIFIED_DATE, LAST_MODIFIED_BY)
    SELECT APPID,
           ITEMKEY,
           POSITION,
           HREF,
           TITLE,
           RELEASE_DATE,
           CASE
               WHEN (POSITION(''zł'' in PRICE) > 0) THEN SUBSTRING(PRICE, POSITION(''zł'' in PRICE) + 2, LENGTH(PRICE))
               WHEN (POSITION(''FREE'' in UPPER(PRICE)) > 0) THEN ''0''
               ELSE ''UNK''
               END AS PRICE,
           CREATED_DATE,
           ''STG_PROCESS'',
           CREATED_DATE,
           ''STG_PROCESS''
    FROM (SELECT *
          FROM (SELECT row_number() over (PARTITION BY appid, created_date ORDER BY created_date DESC) AS rnk, *
                FROM stage.STEAM_TOP_BESTSELLERS_STG) tmp
          WHERE rnk = 1) dt
    WHERE appid != ''UNK'' -- this needs to be fixed in parser!!!
    ON CONFLICT (APPID, ITEMKEY, CREATED_DATE) DO UPDATE SET ITEMKEY      = excluded.itemkey,
                                                             POSITION     = excluded.position,
                                                             HREF         = excluded.href,
                                                             TITLE        = excluded.title,
                                                             RELEASE_DATE = excluded.release_date,
                                                             PRICE        = excluded.price;


    PERFORM update_log_table(p_run_id, ''PROCESS_STEAM_APPS'', ''STEAM_TOP_BESTSELLERS'', ''FINISHED'');

    PERFORM update_log_table(p_run_id, ''PROCESS_STEAM_APPS'', ''STEAM_LIVE_STATS'', ''STARTED'');

    INSERT INTO datamart.STEAM_APP_LIVE_STATS (appid, active_players, active_group_chat, created_date)
    SELECT appid, active_players, active_group_chat, created_date
    FROM (SELECT *
          FROM (SELECT row_number() over (PARTITION BY appid, created_date ORDER BY created_date DESC) AS rnk, *
                FROM stage.STEAM_APP_LIVE_STATS) tmp
          WHERE rnk = 1) dt
    ON CONFLICT (APPID, CREATED_DATE) DO UPDATE SET active_players    = excluded.active_players,
                                                    active_group_chat = excluded.active_group_chat;

    PERFORM update_log_table(p_run_id, ''PROCESS_STEAM_APPS'', ''STEAM_LIVE_STATS'', ''FINISHED'');

    PERFORM update_log_table(p_run_id, ''PROCESS_STEAM_APPS'', ''STEAM_APP_INFO_LKP'', ''STARTED'');

    INSERT INTO datamart.STEAM_APP_INFO_LKP (appid, title, header_image, publishers, developers, created_date,
                                             created_by,
                                             last_modified_date, last_modified_by)
    SELECT appid,
           title,
           header_image,
           publishers,
           developers,
           created_date,
           ''STG_PROCESS'',
           created_date,
           ''STG_PROCESS''
    FROM (SELECT *
          FROM (SELECT row_number() over (PARTITION BY appid ORDER BY created_date DESC) AS rnk, *
                FROM stage.steam_app_info_lkp_stg) AS tmp
          WHERE rnk = 1) AS res
    ON CONFLICT (APPID) DO UPDATE SET title              = excluded.title,
                                      header_image       = excluded.header_image,
                                      publishers         = excluded.publishers,
                                      developers         = excluded.developers,
                                      last_modified_date = excluded.created_date,
                                      last_modified_by   = ''STG_PROCESS'';

    PERFORM update_log_table(p_run_id, ''PROCESS_STEAM_APPS'', ''STEAM_LIVE_STATS'', ''STARTED'');

    INSERT INTO datamart.MS_APP_INFO (PID, TITLE, PROD_CATEGORY, TAGS, PUBLISHER, DEVELOPER, RELEASE_DATE,
                                      ESTIMATED_SIZE, PRICE, CREATED_DATE, CREATED_BY, LAST_MODIFIED_DATE,
                                      LAST_MODIFIED_BY)
    SELECT PID,
           TITLE,
           PROD_CATEGORY,
           TAGS,
           PUBLISHER,
           DEVELOPER,
           RELEASE_DATE,
           ESTIMATED_SIZE,
           PRICE,
           CREATED_DATE,
           ''STG_PROCESS'',
           CREATED_DATE,
           ''STG_PROCESS''
    FROM (SELECT *
          FROM (SELECT row_number() over (PARTITION BY PID ORDER BY created_date DESC) AS rnk, *
                FROM stage.MS_APP_INFO) AS tmp
          WHERE rnk = 1) AS res
    ON CONFLICT (PID) DO UPDATE SET title              = excluded.title,
                                    prod_category      = excluded.prod_category,
                                    tags               = excluded.tags,
                                    publisher          = excluded.publisher,
                                    developer          = excluded.developer,
                                    release_date       = excluded.release_date,
                                    estimated_size     = excluded.estimated_size,
                                    price              = excluded.price,
                                    last_modified_date = excluded.created_date,
                                    last_modified_by   = excluded.last_modified_by;

    PERFORM update_log_table(p_run_id, ''PROCESS_STEAM_APPS'', ''STEAM_LIVE_STATS'', ''FINISHED'');

    RETURN 0;

EXCEPTION
    WHEN OTHERS THEN
        GET STACKED DIAGNOSTICS
            _message := MESSAGE_TEXT,
            _context := PG_EXCEPTION_CONTEXT;
        PERFORM update_log_table(p_run_id, ''PROCESS_STEAM_APPS'', NULL, _message || _context);
        RETURN 1;

END;
$$;

alter function process_steam_apps(integer) owner to simmy;

