drop table if exists gamesanalytics.stage.steam_members_reviews;
create table if not exists gamesanalytics.stage.steam_members_reviews (
    player_id varchar,
    appid varchar,
    scraped_dt varchar,
    review varchar,
    posted_dts varchar,
    edited_dts varchar,
    hours_played varchar,
    hours_when_reved varchar,
    primary key (player_id, appid, scraped_dt)

);
drop table if exists gamesanalytics.stage.steam_members_fllwd_games;
create table if not exists gamesanalytics.stage.steam_members_fllwd_games (
    game_name varchar,
    player_id varchar,
    scraped_dt varchar,
    primary key (player_id, game_name, scraped_dt)
);

drop table if exists gamesanalytics.stage.steam_members_rec_played;
create table if not exists gamesanalytics.stage.steam_members_rec_played(
    appid varchar,
    scraped_dt varchar,
    name varchar,
    last_played varchar,
    hours varchar,
    hours_forever varchar,
    friendlyurl varchar,
    player_id varchar,
    primary key (player_id, appid, scraped_dt)
);

grant all privileges on table gamesanalytics.stage.steam_members_reviews to kw, "simmy-postgres",simmy, simmydatamart, simmystage;
grant all privileges on table gamesanalytics.stage.steam_members_fllwd_games to kw, "simmy-postgres",simmy, simmydatamart, simmystage;
grant all privileges on table gamesanalytics.stage.steam_members_rec_played to kw, "simmy-postgres",simmy, simmydatamart, simmystage;


drop table if exists gamesanalytics.stage.steam_members_games;
create table if not exists gamesanalytics.stage.steam_members_games (
    appid varchar,
    scraped_dt varchar,
    name varchar,
    friendly_name varchar,
    has_adult_content varchar,
    friendlyurl varchar,
    hours varchar,
    hours_forever varchar,
    last_played varchar,
    player_id varchar,
    primary key (player_id, appid, scraped_dt)
);

grant all privileges on table gamesanalytics.stage.steam_members_games to kw, simmy, "simmy-postgres", simmystage, simmydatamart;

drop table if exists gamesanalytics.stage.steam_members_wishlist;
create table if not exists gamesanalytics.stage.steam_members_wishlist (
    appid varchar,
    added varchar,
    scraped_dt varchar,
    priority varchar,
    player_id varchar,
    primary key (player_id, appid, scraped_dt)
);

grant all privileges on table gamesanalytics.stage.steam_members_wishlist to kw, simmy, "simmy-postgres", simmystage, simmydatamart;


drop table if exists gamesanalytics.stage.steam_members_players;
create table if not exists gamesanalytics.stage.steam_members_players (
    player_url varchar,
    player_id varchar,
    player_lvl varchar,
    player_location varchar,
    created_date varchar,
    last_modified_date varchar,
    to_be_scraped varchar
);

grant all privileges on table gamesanalytics.stage.steam_mebers_players to kw, simmy, "simmy-postgres", simmystage, simmydatamart;


-- DATAMART

drop table if exists gamesanalytics.datamart.steam_members_reviews;
create table if not exists gamesanalytics.datamart.steam_members_reviews (
    player_id varchar,
    appid varchar,
    review varchar,
    posted_dts varchar,
    edited_dts varchar,
    hours_played varchar,
    hours_when_reved varchar,
    CREATED_DATE varchar,
    CREATED_BY varchar,
    LAST_MODIFIED_DATE varchar,
    LAST_MODIFIED_BY varchar
    primary key (player_id, appid, CREATED_DATE)
);

drop table if exists gamesanalytics.datamart.steam_members_fllwd_games;
create table if not exists gamesanalytics.datamart.steam_members_fllwd_games (
    game_name varchar,
    player_id varchar,
    CREATED_DATE varchar,
    CREATED_BY varchar,
    LAST_MODIFIED_DATE varchar,
    LAST_MODIFIED_BY varchar

    primary key (player_id, game_name, CREATED_DATE)
);

drop table if exists gamesanalytics.datamart.steam_members_rec_played;
create table if not exists gamesanalytics.datamart.steam_members_rec_played(
    appid varchar,
    name varchar,
    last_played varchar,
    hours varchar,
    hours_forever varchar,
    friendlyurl varchar,
    player_id varchar,
    CREATED_DATE varchar,
    CREATED_BY varchar,
    LAST_MODIFIED_DATE varchar,
    LAST_MODIFIED_BY varchar

    primary key (player_id, appid, CREATED_DATE)
);

grant all privileges on table gamesanalytics.datamart.steam_members_reviews to kw, "simmy-postgres",simmy, simmydatamart, simmystage;
grant all privileges on table gamesanalytics.datamart.steam_members_fllwd_games to kw, "simmy-postgres",simmy, simmydatamart, simmystage;
grant all privileges on table gamesanalytics.datamart.steam_members_rec_played to kw, "simmy-postgres",simmy, simmydatamart, simmystage;


drop table if exists gamesanalytics.datamart.steam_members_games;
create table if not exists gamesanalytics.datamart.steam_members_games (
    appid varchar,
    name varchar,
    friendly_name varchar,
    has_adult_content varchar,
    friendlyurl varchar,
    hours varchar,
    hours_forever varchar,
    last_played varchar,
    player_id varchar,
    CREATED_DATE varchar,
    CREATED_BY varchar,
    LAST_MODIFIED_DATE varchar,
    LAST_MODIFIED_BY varchar

    primary key (player_id, appid, CREATED_DATE)
);

grant all privileges on table gamesanalytics.datamart.steam_members_games to kw, simmy, "simmy-postgres", simmystage, simmydatamart;

drop table if exists gamesanalytics.datamart.steam_members_wishlist;
create table if not exists gamesanalytics.datamart.steam_members_wishlist (
    appid varchar,
    added varchar,
    priority varchar,
    player_id varchar,
    CREATED_DATE varchar,
    CREATED_BY varchar,
    LAST_MODIFIED_DATE varchar,
    LAST_MODIFIED_BY varchar

    primary key (player_id, appid, CREATED_DATE)
);

grant all privileges on table gamesanalytics.datamart.steam_members_wishlist to kw, simmy, "simmy-postgres", simmystage, simmydatamart;


drop table if exists gamesanalytics.datamart.steam_members_players;

create table if not exists gamesanalytics.datamart.steam_members_players (
    player_url varchar,
    player_id varchar primary key,
    to_be_scraped varchar,
    player_lvl varchar,
    player_location varchar,
    CREATED_DATE varchar,
    CREATED_BY varchar,
    LAST_MODIFIED_DATE varchar,
    LAST_MODIFIED_BY varchar,

);

grant all privileges on table gamesanalytics.datamart.steam_members_players to kw, simmy, "simmy-postgres", simmystage, simmydatamart;

-- MERGE PLAYERS INFO


with steam_members_players_last_modif_times as (
    select
           player_id,
           cast(max(
            case
                when last_modified_date = '' then null
                when last_modified_date != '' then cast(last_modified_date as timestamp)
            end) as varchar) as most_recent_modified_date
    from gamesanalytics.stage.steam_members_players
group by player_id),

-- drop table if exists steam_members_players_last_modif_nulls_filled;
steam_members_players_last_modif_nulls_filled as (
    select
           player_id,
           case
            when most_recent_modified_date is null then ''
            when most_recent_modified_date is not null then most_recent_modified_date
           end as most_recent_modified_date
    from steam_members_players_last_modif_times),

-- drop table if exists steam_members_players_tmp;
steam_members_players_tmp as (
    select DISTINCT ON (smp.player_id)
        smp.*
    from gamesanalytics.stage.steam_members_players as smp
        left join steam_members_players_last_modif_nulls_filled as smplmt
            on smp.player_id = smplmt.player_id
    where smp.last_modified_date = smplmt.most_recent_modified_date)

INSERT INTO gamesanalytics.datamart.steam_members_players as dsmp
    SELECT
           ssmp.player_url,
           ssmp.player_id,
           ssmp.to_be_scraped,
           ssmp.player_lvl ,
           ssmp.player_location,
           -- ssmp.created_date,
           case
               when ssmp.created_date is null then cast(current_timestamp as varchar)
                when ssmp.created_date is not null then ssmp.created_date
               end as created_date,
           'scraper' as created_by,
           case
               when ssmp.last_modified_date is null then ssmp.created_date
                when ssmp.last_modified_date is not null then ssmp.last_modified_date
               end as last_modified_date,
           'scraper' as last_modified_by
        FROM steam_members_players_tmp as ssmp
    ON CONFLICT (player_id) DO UPDATE SET player_url           = excluded.player_url,
                                          to_be_scraped              = excluded.to_be_scraped,
                                          player_lvl               = excluded.player_lvl,
                                          player_location          = excluded.player_location,
                                          last_modified_date       = excluded.last_modified_date,
                                          last_modified_by        = excluded.last_modified_by;


INSERT INTO gamesanalytics.datamart.steam_members_wishlist as dsmw
    SELECT DISTINCT ON (smw.player_id, smw.appid, smw.scraped_dt)
           smw.appid,
           smw.added,
           smw.priority,
           smw.player_id,
           smw.scraped_dt as CREATED_DATE,
           'scraper' as CREATED_BY,
           smw.scraped_dt as LAST_MODIFIED_DATE,
           'scraper' as  LAST_MODIFIED_BY

        FROM gamesanalytics.stage.steam_members_wishlist as smw
    ON CONFLICT (player_id, appid, CREATED_DATE) DO UPDATE SET added              = excluded.added,
                                            priority          = excluded.priority,
                                            last_modified_date       = excluded.last_modified_date,
                                            last_modified_by        = excluded.last_modified_by;



INSERT INTO gamesanalytics.datamart.steam_members_reviews as dsmr
SELECT DISTINCT ON (smr.player_id, smr.appid, smr.scraped_dt)
    smr.player_id,
    smr.appid,
    smr.review,
    smr.posted_dts,
    smr.edited_dts,
    smr.hours_played,
    smr.hours_when_reved,
    smr.scraped_dt as CREATED_DATE,
    'scraper' as CREATED_BY,
    smr.scraped_dt as LAST_MODIFIED_DATE,
    'scraper' as LAST_MODIFIED_BY
FROM gamesanalytics.stage.steam_members_reviews as smr
ON CONFLICT (player_id, appid, CREATED_DATE) DO UPDATE SET
    review = excluded.review,
    posted_dts = excluded.posted_dts,
    edited_dts = excluded.edited_dts,
    hours_played = excluded.hours_played,
    hours_when_reved = excluded.hours_when_reved;


INSERT INTO gamesanalytics.datamart.steam_members_games as dsmg
    SELECT DISTINCT ON (smg.player_id, smg.appid, smg.scraped_dt)
            smg.appid,
            smg.name,
            smg.friendly_name,
            smg.has_adult_content,
            smg.friendlyurl,
            smg.hours,
            smg.hours_forever,
            smg.last_played,
            smg.player_id,
            smg.scraped_dt as CREATED_DATE,
            'scraper' as CREATED_BY,
            smg.scraped_dt as LAST_MODIFIED_DATE,
            'scraper' as LAST_MODIFIED_BY
        FROM gamesanalytics.stage.steam_members_games  as smg
    ON CONFLICT (player_id, appid, CREATED_DATE) DO UPDATE SET
                                          name           = excluded.name,
                                          friendly_name              = excluded.friendly_name,
                                          has_adult_content               = excluded.has_adult_content,
                                          friendlyurl          = excluded.friendlyurl,
                                          hours       = excluded.hours,
                                          hours_forever        = excluded.hours_forever,
                                          last_played          = excluded.last_played;

INSERT INTO gamesanalytics.datamart.steam_members_rec_played as dsmrc
    SELECT DISTINCT ON (ssmg.player_id, ssmg.appid, ssmg.scraped_dt)
        ssmg.appid,
        ssmg.name,
        ssmg.last_played,
        ssmg.hours,
        ssmg.hours_forever,
        ssmg.friendlyurl,
        ssmg.player_id,
        ssmg.scraped_dt as CREATED_DATE,
        'scraper' as CREATED_BY,
        ssmg.scraped_dt as LAST_MODIFIED_DATE,
        'scraper' as LAST_MODIFIED_BY
    FROM gamesanalytics.stage.steam_members_games  as ssmg
    ON CONFLICT (player_id, appid, CREATED_DATE) DO UPDATE SET
                                          name          = excluded.name,
                                          last_played   = excluded.last_played,
                                          hours         = excluded.hours,
                                          hours_forever = excluded.hours_forever,
                                          friendlyurl   = excluded.last_played;


INSERT INTO gamesanalytics.datamart.steam_members_fllwd_games as dsmfg
    SELECT
           ssfg.game_name,
           ssfg.player_id,
           ssfg.scraped_dt as created_date,
           'scraper' as created_by,
           ssfg.scraped_dt as last_modified_date,
           'scraper' as last_modified_by
        FROM gamesanalytics.stage.steam_members_fllwd_games as ssfg
    ON CONFLICT (game_name, player_id, created_date) DO UPDATE SET
                                          last_modified_date               = excluded.last_modified_date


