import pandas as pd
import psycopg2
import sqlalchemy
if __name__ == "__main__":
    # friends_df = pd.DataFrame(
    #     {'player_url': friends_urls,
    #      'player_id': friends_ids})
    #
    # friends_df['last_time_scraped'] = ''
    # friends_df['to_be_scraped'] = 0
    user_list_one = pd.read_csv("../../engine/sample_parsed_users_links.csv",
                                names=["id", "url"])
    user_list_two = pd.read_csv("../../engine/parsed_friends_not_recursive.csv",
                                names=["id", "url"])

    user_list = pd.concat([user_list_two, user_list_one]).rename(columns={"url": "player_url"})
    # print(user_list.head().to_string())
    # print(len(user_list))

    user_list["player_id"] = user_list["player_url"].apply(lambda x: x.split("/")[-1])

    user_list['last_time_scraped'] = ''
    user_list['to_be_scraped'] = 0
    print(user_list.head().to_string())

    conn = \
        sqlalchemy.create_engine(
            'postgresql://{0}:{1}@{2}:{3}/{4}'
            .format())

    user_list.drop(columns=['id']).to_sql(
        name="steam_members_players", con=conn, if_exists="append",
        schema="stage", index=False, method='multi')



