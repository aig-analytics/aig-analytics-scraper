from src.data_uploaders.db_data_loaders import PostgresDataLoader
from src.data_uploaders.db_data_uploaders import PostgresDataUploader
from src.engine.microsoft_store_processing_engine import MicrosoftStoreProcessingEngine
from src.engine.steam_members_engine import SteamMembersEngine
from src.engine.steam_processing_engine import SteamProcessingEngine
from src.engine.stock_data_processing_engine import StockDataProcessingEngine


class ScrappingWorkflows:

    @property
    def global_config(self) -> dict:
        return self._global_config

    @global_config.setter
    def global_config(self, new_val):
        self._global_config = new_val

    @property
    def steam_processing_engine(self) -> SteamProcessingEngine:
        return self._steam_processing_engine

    @steam_processing_engine.setter
    def steam_processing_engine(self, new_val):
        self._steam_processing_engine = new_val

    @property
    def steam_members_processing_engine(self) -> SteamMembersEngine:
        return self._steam_members_processing_engine

    @steam_members_processing_engine.setter
    def steam_members_processing_engine(self, new_val: SteamMembersEngine):
        self._steam_members_processing_engine = new_val

    @property
    def microsoft_store_processing_engine(self) -> MicrosoftStoreProcessingEngine:
        return self._microsoft_store_processing_engine

    @microsoft_store_processing_engine.setter
    def microsoft_store_processing_engine(self, new_val):
        self._microsoft_store_processing_engine = new_val

    @property
    def postgres_data_uploader(self) -> PostgresDataUploader:
        return self._postgres_data_uploader

    @postgres_data_uploader.setter
    def postgres_data_uploader(self, new_val):
        self._postgres_data_uploader = new_val

    @property
    def postgres_data_loader(self) -> PostgresDataLoader:
        return self._postgres_data_loader

    @postgres_data_loader.setter
    def postgres_data_loader(self, new_val):
        self._postgres_data_loader = new_val

    @property
    def stock_data_processing_engine(self) -> StockDataProcessingEngine:
        return self._stock_data_processing_engine

    @stock_data_processing_engine.setter
    def stock_data_processing_engine(self, new_val):
        self._stock_data_processing_engine = new_val

    def __init__(self, global_cfg: dict, postgres_data_uploader, postgres_data_loader):
        self.global_config = global_cfg
        self.postgres_data_uploader = postgres_data_uploader
        self.postgres_data_loader = postgres_data_loader

    def process_wse_comp_data(self):
        banker_shortcuts = self.postgres_data_loader.get_bankier_shortcuts()
        wse_companies = self.stock_data_processing_engine.get_banker_company_data(banker_shortcuts=banker_shortcuts)
        self.postgres_data_uploader.pd_upload_wse_company_stock_info(wse_companies_list=wse_companies)

    # def process_steam_data(self):
    #     whichlist_titles_ids, whishlist_titles = self.steam_processing_engine.run_steam_whishlist(exec_type="WHISHLIST")
    #     self.postgres_data_uploader.upload_whishlist_titles(whishlist_entries=whishlist_titles)
    #
    #     bestseller_ids, bestseller_titles = self.steam_processing_engine.run_steam_whishlist(exec_type="BESTSELLER")
    #     self.postgres_data_uploader.upload_bestsellers_titles(bestseller_entries=bestseller_titles)
    #
    #     app_ids = list(set(whichlist_titles_ids + bestseller_ids))
    #     app_id_info_list = self.steam_processing_engine.run_steam_app(app_id_list=app_ids)
    #
    #     self.postgres_data_uploader.upload_steam_app_info(app_id_info_list)

    def pull_live_stats(self):
        ids_list = self.postgres_data_loader.get_all_steam_app_ids()
        live_stats = self.steam_processing_engine.run_steam_active_players(ids_list)
        self.postgres_data_uploader.upload_active_stats(live_stats)

    def pull_ms_store_info(self):
        mspe = self.microsoft_store_processing_engine
        res = mspe.get_country_top_paid_list()

        self.postgres_data_uploader.upload_ms_ranked_titles(res)

        app_ids_hrefs_dict = mspe.get_distinct_hrefs(res)

        ms_apps = mspe.get_microsoft_store_app_info(app_ids_hrefs_dict)

        self.postgres_data_uploader.upload_ms_app_info(ms_apps)

    def steam_app_info_lkp_batch_process(self,
                                         batch_size_limit: int = 250,
                                         ids_limit_size: int = 250):
        ids = self.steam_processing_engine.get_all_steam_apps_ids()

        ids_list = self.postgres_data_loader.get_steam_app_lkp_all_ids()

        new_ids = list(set(ids) - set([int(x) for x in ids_list]))

        ## NOTE ## @TODO
        ## At some point we would need to handle such cases:
        ## https://store.steampowered.com/api/appdetails?appids=1313440
        ## response:
        ## {"1313440":{"success":false}}
        ## because if not - then it may happen start pulling mostly missing links
        ## which may leads to huge processing time increase

        if ids_limit_size > 0:
            new_ids = new_ids[0:ids_limit_size + 1]
        self._process_steam_app_info_lkp(ids=new_ids, batch_size_limit=batch_size_limit)


    def _process_steam_app_info_lkp(self, ids: list,
                                    batch_size_limit: int = 250
                                    ):
        for i in range(0, len(ids), batch_size_limit):
            ids_to_process = ids[i:i + batch_size_limit]
            sail_list = self.steam_processing_engine.get_all_steam_apps_lookup_info(ids_to_process)
            self.postgres_data_uploader.upload_steam_app_info_lkp(sail_list)

    def process_steam_members(
            self, fresh_members: int = 1000, updated_members: int = 200,
            benchmark_run: bool = False):

        if benchmark_run:
            fresh_members = int(fresh_members / 100)
            updated_members = int(updated_members / 100)

        scraped_player_ids = \
            self.postgres_data_loader.get_steam_members_scraped_ids(
                fresh_members=fresh_members, updated_members=updated_members)

        self.steam_members_processing_engine\
            .parse_desired_players(players_ids=scraped_player_ids)

        smpe = self.steam_members_processing_engine

        # self.postgres_data_uploader.pg_drop_steam_members_players_duplicates(
        #     friends_df_tabn='steam_mebers_players',
        #     friends_tab_cn='player_url',
        #     scraped_urls=smpe.parsed_steam_members_urls)

        # print(smpe.friends_df.to_string())
        # input('sanity check')

        self.postgres_data_uploader.pd_upload_steam_members_info(
            dfs=[smpe.friends_df, smpe.games_df, smpe.wl_df,
                 smpe.fllwd_games_df, smpe.rec_played_df, smpe.revs_df],
            tab_ns=['steam_members_players', 'steam_members_games',
                    'steam_members_wishlist', 'steam_members_fllwd_games',
                    'steam_members_rec_played', 'steam_members_reviews'])
        # TODO update processing date for scraped users

        # self._upload_scraped_steam_members_info()

