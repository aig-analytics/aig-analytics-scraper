import os

from dagster import solid, pipeline, execute_pipeline, execute_solid, \
    InputDefinition
from dagster.core.instance import DagsterInstance
from pprint import pprint

from src.dagster_objects.commons.types import DataMartConnectionType
from src.dagster_objects.solids.db.pg_connections import get_datamart_connection, \
    get_datamart_connection_w_env
from src.dagster_objects.solids.utils.configs import get_config
from src.data_uploaders.db_data_loaders import PostgresDataLoader
from src.data_uploaders.db_data_uploaders import PostgresDataUploader
from src.engine.steam_members_engine import SteamMembersEngine
from src.engine.executor.proxy_engines import PublicProxyEngine
from src.engine.executor.web_request_executors import SingleWebRequestExecutor

from src.utils.meta_utils import get_run_metadata


@solid
def get_fresh_members_count(_):
    return 100


@solid
def get_updated_members_count(_):
    return 10


@solid
def get_cfg_pth(_):
    return '../utils/app.yaml'


@solid(
    input_defs=[
        InputDefinition(
            name='fresh_members', dagster_type=int, default_value=100,
            description='how many new members should be scraped'),
        InputDefinition(
            name='updated_members', dagster_type=int, default_value=100,
            description='how many updated members should be scraped'),
        InputDefinition(
            name='datamart_connection', dagster_type=DataMartConnectionType,
            description='DataMart connection object'),
    ]
)
def get_steam_members_scraped_ids(
        context, fresh_members: int, updated_members: int,
        datamart_connection):

    pd_loader = PostgresDataLoader(datamart_connection=datamart_connection)

    # context.solid_config["reverse"]

    return pd_loader.get_steam_members_scraped_ids(
        fresh_members=fresh_members, updated_members=updated_members)


@solid
def parse_desired_players(
        context, datamart_connection, scraped_ids: list):
    pe = PublicProxyEngine(datamart_connection)
    wre = SingleWebRequestExecutor(pe)
    sme = SteamMembersEngine(request_execution_engine=wre)

    sme.parse_desired_players(scraped_ids)
    return sme


@solid
def pd_upload_steam_members_info(
        context, datamart_connection,
        steam_members_engine):
    smpe = steam_members_engine
    pdu = PostgresDataUploader(datamart_connection)
    context.log.info(smpe.games_df.head().to_string())
    pdu.pd_upload_steam_members_info(
        dfs=[smpe.friends_df, smpe.games_df, smpe.wl_df,
             smpe.fllwd_games_df, smpe.rec_played_df, smpe.revs_df],
        tab_ns=['steam_members_players', 'steam_members_games',
                'steam_members_wishlist', 'steam_members_fllwd_games',
                'steam_members_rec_played', 'steam_members_reviews'])


@pipeline
def process_steam_members_pipeline():
    datamart_connection = get_datamart_connection_w_env()
    smi = get_steam_members_scraped_ids(datamart_connection=datamart_connection)
    pdp = parse_desired_players(datamart_connection, smi)
    pd_upload_steam_members_info(datamart_connection, pdp)


if __name__ == '__main__':

    # os.environ["DAGSTER_HOME"] = '/home/os/dagster_home'
    # print(DagsterInstance.get().__dict__)
    # print(_is_dagster_home_set())
    # input('sanity check')

    # run_config = {
    #     "solids": {
    #         "get_config":
    #             {"inputs":
    #                  {"cfg_path":
    #                       {"value": "../utils/app.yaml"}}}
    #     },
    # }

    # run_config = \
    #     {'solids': {
    #         'get_datamart_connection_w_env':
    #             {'inputs': {
    #                 'db_envarn': {'value': 'GAMES_ANALYTICS_DATAMART_DATABASE'},
    #                 'ip_envarn': {'value': 'GAMES_ANALYTICS_IP'},
    #                 'pass_envarn': {'value': 'GAMES_ANALYTICS_USER_PASSWORD'},
    #                 'port_envarn': {'value': 'GAMES_ANALYTICS_PORT'},
    #                 'uname_envarn': {'value': 'GAMES_ANALYTICS_USER'}}},
    #         'get_steam_members_scraped_ids': {
    #             'inputs': {
    #                 'fresh_members': {'value': 10},
    #                 'updated_members': {'value': 10}}}}}

    run_config = get_run_metadata(
        pipeline_name='process_steam_members_pipeline', schedule_name='hourly')

    # cfg = execute_solid(
    #     get_config, input_values={
    #         "cfg_path": "../utils/app.yaml"})

    execute_pipeline(
        process_steam_members_pipeline, run_config=run_config,
        instance=DagsterInstance.get())
