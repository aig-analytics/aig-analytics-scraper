class SteamAppLiveStats:

    @property
    def appid(self) -> str:
        return self._appid

    @appid.setter
    def appid(self, new_val):
        self._appid = new_val

    @property
    def active_players(self) -> str:
        return self._active_players

    @active_players.setter
    def active_players(self, new_val):
        self._active_players = new_val

    @property
    def active_group_chat(self) -> str:
        return self._active_group_chat

    @active_group_chat.setter
    def active_group_chat(self, new_val):
        self._active_group_chat = new_val

    @property
    def created_date(self) -> str:
        return self._created_date

    @created_date.setter
    def created_date(self, new_val):
        self._created_date = new_val

    def __init__(self, appid: str,
                 active_players: str,
                 active_group_chat: str,
                 created_date: str):
        self.appid = appid
        self.active_players = active_players
        self.active_group_chat = active_group_chat
        self.created_date = created_date

    def __str__(self):
        return "appid: " + self.appid + \
               ", active_players: " + self.active_players + \
               ", active_group_chat" + self.active_group_chat + \
               ", created_date" + self.created_date
