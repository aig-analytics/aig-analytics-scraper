class MicrosoftAppInfo:

    @property
    def pid(self) -> str:
        return self._pid

    @pid.setter
    def pid(self, new_val):
        self._pid = new_val

    @property
    def title(self) -> str:
        return self._title

    @title.setter
    def title(self, new_val):
        self._title = new_val

    @property
    def prod_category(self) -> str:
        return self._prod_category

    @prod_category.setter
    def prod_category(self, new_val):
        self._prod_category = new_val

    @property
    def tags(self) -> str:
        return self._tags

    @tags.setter
    def tags(self, new_val):
        self._tags = new_val

    @property
    def publisher(self) -> str:
        return self._publisher

    @publisher.setter
    def publisher(self, new_val):
        self._publisher = new_val

    @property
    def developer(self) -> str:
        return self._developer

    @developer.setter
    def developer(self, new_val):
        self._developer = new_val

    @property
    def release_date(self) -> str:
        return self._release_date

    @release_date.setter
    def release_date(self, new_val):
        self._release_date = new_val

    @property
    def estimated_size(self) -> str:
        return self._estimated_size

    @estimated_size.setter
    def estimated_size(self, new_val):
        self._estimated_size = new_val

    @property
    def price(self) -> str:
        return self._price

    @price.setter
    def price(self, new_val):
        self._price = new_val

    @property
    def created_date(self) -> str:
        return self._created_date

    @created_date.setter
    def created_date(self, new_val):
        self._created_date = new_val

    def __init__(self, pid: str,
                 title: str,
                 prod_category: str,
                 tags: str,
                 publisher: str,
                 developer: str,
                 release_date: str,
                 estimated_size: str,
                 price: str,
                 created_date: str):
        self.pid = pid
        self.title = title
        self.prod_category = prod_category
        self.tags = tags
        self.publisher = publisher
        self.developer = developer
        self.release_date = release_date
        self.estimated_size = estimated_size
        self.price = price
        self.created_date = created_date

    def __str__(self):
        return "pid: " + self.pid + \
               ", title: " + self.title + \
               ", prod_category: " + self.prod_category + \
               ", tags: " + self.tags + \
               ", publisher: " + self.publisher + \
               ", developer: " + self.developer + \
               ", release_date: " + self.release_date + \
               ", estimated_date: " + self.estimated_size + \
               ", price: " + self.price + \
               ", created_date: " + self.created_date
