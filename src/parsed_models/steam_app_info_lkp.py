class SteamAppInfoLkp:

    @property
    def appid(self) -> str:
        return self._appid

    @appid.setter
    def appid(self, new_val):
        self._appid = new_val

    @property
    def title(self) -> str:
        return self._title

    @title.setter
    def title(self, new_val):
        self._title = new_val

    @property
    def header_image(self) -> str:
        return self._header_image

    @header_image.setter
    def header_image(self, new_val):
        self._header_image = new_val

    @property
    def publishers(self) -> str:
        return self._publishers

    @publishers.setter
    def publishers(self, new_val):
        self._publishers = new_val

    @property
    def developers(self) -> str:
        return self._developers

    @developers.setter
    def developers(self, new_val):
        self._developers = new_val

    @property
    def created_date(self) -> str:
        return self._created_date

    @created_date.setter
    def created_date(self, new_val):
        self._created_date = new_val

    @property
    def price(self) -> str:
        return self._price

    @price.setter
    def price(self, new_val):
        self._price = new_val

    def __init__(self, appid: str,
                 title: str,
                 header_image: str,
                 publishers: str,
                 developers: str,
                 created_date: str,
                 price: str):
        self.appid = appid
        self.title = title
        self.header_image = header_image
        self.publishers = publishers
        self.developers = developers
        self.created_date = created_date
        self.price = price

    def __str__(self):
        return "appid: " + self.appid + \
               ", title: " + self.title + \
               ", header_image: " + self.header_image + \
               ", publishers: " + self.publishers + \
               ", developers: " + self.developers + \
               ", created_date: " + self.created_date + \
               ", price" + self.price
