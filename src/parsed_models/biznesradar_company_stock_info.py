class BiznesRadarCompanyStockInfo:

    @property
    def shortcut(self) -> str:
        return self._shortcut

    @shortcut.setter
    def shortcut(self, value):
        self._shortcut = value

    @property
    def capitalization(self) -> str:
        return self._capitalization

    @capitalization.setter
    def capitalization(self, value):
        self._capitalization = value

    @property
    def number_of_stocks(self) -> str:
        return self._number_of_stocks

    @number_of_stocks.setter
    def number_of_stocks(self, value):
        self._number_of_stocks = value

    @property
    def actual_stock_price(self) -> str:
        return self._actual_stock_price

    @actual_stock_price.setter
    def actual_stock_price(self, value):
        self._actual_stock_price = value

    @property
    def open_stock_price(self) -> str:
        return self._open_stock_price

    @open_stock_price.setter
    def open_stock_price(self, value):
        self._open_stock_price = value

    @property
    def min_stock_price(self) -> str:
        return self._min_stock_price

    @min_stock_price.setter
    def min_stock_price(self, value):
        self._min_stock_price = value

    @property
    def max_stock_price(self) -> str:
        return self._max_stock_price

    @max_stock_price.setter
    def max_stock_price(self, value):
        self._max_stock_price = value

    @property
    def volume(self) -> str:
        return self._volume

    @volume.setter
    def volume(self, value):
        self._volume = value

    @property
    def total_trading(self) -> str:
        return self._total_trading

    @total_trading.setter
    def total_trading(self, value):
        self._total_trading = value

    @property
    def number_of_transactions(self) -> str:
        return self._number_of_transactions

    @number_of_transactions.setter
    def number_of_transactions(self, value):
        self._number_of_transactions = value

    @property
    def sales_revenue(self) -> str:
        return self._sales_revenue

    @sales_revenue.setter
    def sales_revenue(self, value):
        self._sales_revenue = value

    @property
    def operational_profit(self):
        return self._operational_profit

    @operational_profit.setter
    def operational_profit(self, value):
        self._operational_profit = value

    @property
    def profit_before_tax(self) -> str:
        return self._profit_before_tax

    @profit_before_tax.setter
    def profit_before_tax(self, value):
        self._profit_before_tax = value

    @property
    def assets(self):
        return self._assets

    @assets.setter
    def assets(self, value):
        self._assets = value

    @property
    def equity_capital(self):
        return self._equity_capital

    @equity_capital.setter
    def equity_capital(self, value):
        self._equity_capital = value

    @property
    def cwk(self) -> str:
        return self._cwk

    @cwk.setter
    def cwk(self, value):
        self._cwk = value

    @property
    def cp(self) -> str:
        return self._cp

    @cp.setter
    def cp(self, value):
        self._cp = value

    @property
    def roe(self) -> str:
        return self._roe

    @roe.setter
    def roe(self, value):
        self._roe = value

    @property
    def roa(self) -> str:
        return self._roa

    @roa.setter
    def roa(self, value):
        self._roa = value

    @property
    def altman(self) -> str:
        return self._altman

    @altman.setter
    def altman(self, value):
        self._altman = value

    @property
    def piotroski(self) -> str:
        return self._piotroski

    @piotroski.setter
    def piotroski(self, value):
        self._piotroski = value

    @property
    def execution_date(self) -> str:
        return self._execution_date

    @execution_date.setter
    def execution_date(self, value):
        self._execution_date = value

    def __init__(self,
                 shortcut,
                 capitalization,
                 number_of_stocks,
                 actual_stock_price,
                 open_stock_price,
                 min_stock_price,
                 max_stock_price,
                 volume,
                 total_trading,
                 number_of_transactions,
                 sales_revenue,
                 operational_profit,
                 profit_before_tax,
                 assets,
                 equity_capital,
                 cwk,
                 cp,
                 roe,
                 roa,
                 altman,
                 piotroski,
                 execution_date):
        self.shortcut = shortcut
        self.capitalization = capitalization
        self.number_of_stocks = number_of_stocks
        self.actual_stock_price = actual_stock_price
        self.open_stock_price = open_stock_price
        self.min_stock_price = min_stock_price
        self.max_stock_price = max_stock_price
        self.volume = volume
        self.total_trading = total_trading
        self.number_of_transactions = number_of_transactions
        self.sales_revenue = sales_revenue
        self.operational_profit = operational_profit
        self.profit_before_tax = profit_before_tax
        self.assets = assets
        self.equity_capital = equity_capital
        self.cwk = cwk
        self.cp = cp
        self.roe = roe
        self.roa = roa
        self.altman = altman
        self.piotroski = piotroski
        self.execution_date = execution_date

    def __str__(self):
        return "shortcut: " + self.shortcut + \
               ", actual_stock_price: " + self.actual_stock_price + \
               ", capitalization: " + self.capitalization + \
               ", number_of_stocks: " + self.number_of_stocks + \
               ", open_stock_price: " + self.open_stock_price + \
               ", min_stock_price: " + self.min_stock_price + \
               ", max_stock_price: " + self.max_stock_price + \
               ", total_trading: " + self.total_trading + \
               ", sales_revenue: " + self.sales_revenue + \
               ", cwk: " + self.cwk + \
               ", altman: " + self.altman
