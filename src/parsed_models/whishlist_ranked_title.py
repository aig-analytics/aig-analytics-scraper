class WhishlistRankedTitle:

    @property
    def appid(self) -> str:
        return self._appid

    @appid.setter
    def appid(self, new_val):
        self._appid = new_val

    @property
    def itemkey(self) -> str:
        return self._itemkey

    @itemkey.setter
    def itemkey(self, new_val):
        self._itemkey = new_val

    @property
    def position(self) -> str:
        return self._position

    @position.setter
    def position(self, new_val):
        self._position = new_val

    @property
    def href(self) -> str:
        return self._position

    @href.setter
    def href(self, new_val):
        self._href = new_val

    @property
    def title(self) -> str:
        return self._title

    @title.setter
    def title(self, new_val):
        self._title = new_val

    @property
    def release_date(self) -> str:
        return self._release_date

    @release_date.setter
    def release_date(self, new_val):
        self._release_date = new_val

    @property
    def price(self) -> str:
        return self._price

    @price.setter
    def price(self, new_val):
        self._price = new_val

    @property
    def created_date(self) -> str:
        return self._created_date

    @created_date.setter
    def created_date(self, new_val):
        self._created_date = new_val

    def __init__(self,
                 appid: str,
                 itemkey: str,
                 position: str,
                 href: str,
                 title: str,
                 release_date: str,
                 price: str,
                 created_date: str):
        self.appid = appid
        self.itemkey = itemkey
        self.position = position
        self.href = href
        self.title = title
        self.release_date = release_date
        self.price = price
        self.created_date = created_date

    def __str__(self):
        return "Appid: " + self.appid + ", itemkey: " + self.itemkey + ", position: " + self.position + \
               ", title: " + self.title + ", release_date: " + self.release_date + ", price: " + self.price + \
               ", created_date: " + self.created_date
