class SteamApplicationInfo:

    @property
    def appid(self) -> str:
        return self._appid

    @appid.setter
    def appid(self, new_val):
        self._appid = new_val

    @property
    def title(self) -> str:
        return self._title

    @title.setter
    def title(self, new_val):
        self._title = new_val

    @property
    def app_general_type(self) -> str:
        return self._app_general_type

    @app_general_type.setter
    def app_general_type(self, new_val):
        self._app_general_type = new_val

    @property
    def release_date(self) -> str:
        return self._release_date

    @release_date.setter
    def release_date(self, value):
        self._release_date = value

    @property
    def app_category(self) -> str:
        return self._app_category

    @app_category.setter
    def app_category(self, new_val):
        self._app_category = new_val

    @property
    def game_description_snippet(self) -> str:
        return self._game_description_snippet

    @game_description_snippet.setter
    def game_description_snippet(self, new_val):
        self._game_description_snippet = new_val

    @property
    def producer(self) -> str:
        return self._producer

    @producer.setter
    def producer(self, new_val):
        self._producer = new_val

    @property
    def publisher(self) -> str:
        return self._publisher

    @publisher.setter
    def publisher(self, new_val):
        self._publisher = new_val

    @property
    def steam_reviews_summary(self) -> str:
        return self._steam_reviews_summary

    @steam_reviews_summary.setter
    def steam_reviews_summary(self, new_val):
        self._steam_reviews_summary = new_val

    @property
    def steam_reviews_description(self) -> str:
        return self._steam_reviews_description

    @steam_reviews_description.setter
    def steam_reviews_description(self, new_val):
        self._steam_reviews_description = new_val

    @property
    def review_count(self) -> str:
        return self._review_count

    @review_count.setter
    def review_count(self, new_val):
        self._review_count = new_val

    @property
    def rating_value(self) -> str:
        return self._rating_value

    @rating_value.setter
    def rating_value(self, new_val):
        self._rating_value = new_val

    @property
    def best_rating(self) -> str:
        return self._best_rating

    @best_rating.setter
    def best_rating(self, new_val):
        self._best_rating = new_val

    @property
    def worst_rating(self) -> str:
        return self._worst_rating

    @worst_rating.setter
    def worst_rating(self, new_val):
        self._worst_rating = new_val

    @property
    def popular_tags(self) -> str:
        return self._popular_tags

    @popular_tags.setter
    def popular_tags(self, new_val):
        self._popular_tags = new_val

    @property
    def members_count(self) -> str:
        return self._members_count

    @members_count.setter
    def members_count(self, new_val):
        self._members_count = new_val

    @property
    def in_game_players(self) -> str:
        return self._in_game_players

    @in_game_players.setter
    def in_game_players(self, new_val):
        self._in_game_players = new_val

    @property
    def online_players_cnt(self) -> str:
        return self._online_players_cnt

    @online_players_cnt.setter
    def online_players_cnt(self, new_val):
        self._online_players_cnt = new_val

    @property
    def on_groups_chat(self) -> str:
        return self._on_group_chat

    @on_groups_chat.setter
    def on_groups_chat(self, new_val):
        self._on_group_chat = new_val

    @property
    def created_date(self) -> str:
        return self._created_date

    @created_date.setter
    def created_date(self, new_val):
        self._created_date = new_val

    def __init__(self, appid: str,
                 title: str,
                 app_general_type: str,
                 release_date: str,
                 app_category: str,
                 game_description_snippet: str,
                 producer: str,
                 publisher: str,
                 steam_reviews_summary: str,
                 steam_reviews_description: str,
                 review_count: str,
                 rating_value: str,
                 best_rating: str,
                 worst_rating: str,
                 popular_tags: str,
                 members_count: str,
                 in_game_players: str,
                 online_players_cnt: str,
                 on_groups_chat: str,
                 created_date: str):
        self.appid = appid
        self.title = title
        self.app_general_type = app_general_type
        self.release_date = release_date
        self.app_category = app_category
        self.game_description_snippet = game_description_snippet
        self.producer = producer
        self.publisher = publisher
        self.steam_reviews_summary = steam_reviews_summary
        self.steam_reviews_description = steam_reviews_description
        self.review_count = review_count
        self.rating_value = rating_value
        self.best_rating = best_rating
        self.worst_rating = worst_rating
        self.popular_tags = popular_tags
        self.created_date = created_date
        self.members_count = members_count
        self.in_game_players = in_game_players
        self.online_players_cnt = online_players_cnt
        self.on_groups_chat = on_groups_chat

    def __str__(self):
        return "appid: " + self.appid + \
               ", title: " + self.title + \
               ", release_date: " + self.release_date + \
               ", app_general_type: " + self.app_general_type + \
               ", app_category: " + self.app_category + \
               ", game_description_snippet: " + self.game_description_snippet + \
               ", producer: " + self.producer + \
               ", publisher: " + self.publisher + \
               ", reviews_summary: " + self.steam_reviews_summary + \
               ", reviews_description: " + self.steam_reviews_description + \
               ", review_count: " + self.review_count + \
               ", rating_value: " + self.rating_value + \
               ", best_rating: " + self.best_rating + \
               ", worst_rating: " + self.worst_rating + \
               ", popular_tags: " + self.popular_tags + \
               ", members_count: " + self.members_count + \
               ", in_game_players: " + self.in_game_players + \
               ", online_players_cnt: " + self.online_players_cnt + \
               ", on_groups_chat: " + self.on_groups_chat + \
               ", created_date: " + self.created_date
