class BankierCompanyStockInfo:

    @property
    def banker_id(self) -> str:
        return self._banker_id

    @banker_id.setter
    def banker_id(self, new_val):
        self._banker_id = new_val

    @property
    def banker_shortcut(self) -> str:
        return self._banker_shortcut

    @banker_shortcut.setter
    def banker_shortcut(self, new_val):
        self._banker_shortcut = new_val

    @property
    def stock_price(self) -> str:
        return self._stock_price

    @stock_price.setter
    def stock_price(self, new_val):
        self._stock_price = new_val

    @property
    def capitalization(self) -> str:
        return self._capitalization

    @capitalization.setter
    def capitalization(self, new_val):
        self._capitalization = new_val

    @property
    def free_float(self) -> str:
        return self._free_float

    @free_float.setter
    def free_float(self, new_val):
        self._free_float = new_val

    @property
    def stock_amount(self) -> str:
        return self._stock_amount

    @stock_amount.setter
    def stock_amount(self, new_val):
        self._stock_amount = new_val

    @property
    def price_to_profit(self) -> str:
        return self._price_to_profit

    @price_to_profit.setter
    def price_to_profit(self, new_val):
        self._price_to_profit = new_val

    @property
    def price_to_book_value(self) -> str:
        return self._price_to_book_value

    @price_to_book_value.setter
    def price_to_book_value(self, new_val):
        self._price_to_book_value = new_val

    @property
    def created_date(self) -> str:
        return self._created_date

    @created_date.setter
    def created_date(self, new_val):
        self._created_date = new_val

    def __init__(self,
                 banker_id: str,
                 banker_shortcut: str,
                 stock_price: str,
                 capitalization: str,
                 free_float: str,
                 stock_amount: str,
                 price_to_profit: str,
                 price_to_book_value: str,
                 created_date: str):
        self.banker_id = banker_id
        self.banker_shortcut = banker_shortcut
        self.stock_price = stock_price
        self.capitalization = capitalization
        self.free_float = free_float
        self.stock_amount = stock_amount
        self.price_to_profit = price_to_profit
        self.price_to_book_value = price_to_book_value
        self.created_date = created_date

    def __str__(self):
        return "banker_id: " + self.banker_id + \
               ", banker_shortcut: " + self.banker_shortcut + \
               ", stock_price: " + self.stock_price + \
               ", capitalization: " + self.capitalization + \
               ", free_float: " + self.free_float + \
               ", stock_amount: " + self.stock_amount + \
               ", price_to_profit: " + self.price_to_profit + \
               ", price_to_book_value: " + self.price_to_book_value + \
               ", created_date: " + self.created_date
