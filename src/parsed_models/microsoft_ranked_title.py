class MicrosoftRankedTitle:

    @property
    def data_aid(self) -> str:
        return self._data_aid

    @data_aid.setter
    def data_aid(self, new_val):
        self._data_aid = new_val

    @property
    def app_info_href(self) -> str:
        return self._app_info_href

    @app_info_href.setter
    def app_info_href(self, new_val):
        self._app_info_href = new_val

    @property
    def pid(self) -> str:
        return self._pid

    @pid.setter
    def pid(self, new_val):
        self._pid = new_val

    @property
    def title(self) -> str:
        return self._title

    @title.setter
    def title(self, new_val):
        self._title = new_val

    @property
    def rating_value(self) -> str:
        return self._rating_value

    @rating_value.setter
    def rating_value(self, new_value):
        self._rating_value = new_value

    @property
    def best_rating(self) -> str:
        return self._best_rating

    @best_rating.setter
    def best_rating(self, new_val):
        self._best_rating = new_val

    @property
    def review_count(self) -> str:
        return self._review_count

    @review_count.setter
    def review_count(self, new_val):
        self._review_count = new_val

    @property
    def position(self) -> str:
        return self._position

    @position.setter
    def position(self, new_val):
        self._position = new_val

    @property
    def country(self) -> str:
        return self._country

    @country.setter
    def country(self, new_val):
        self._country = new_val

    @property
    def ranking_type(self) -> str:
        return self._ranking_type

    @ranking_type.setter
    def ranking_type(self, new_val):
        self._ranking_type = new_val

    @property
    def device_type(self) -> str:
        return self._device_type

    @device_type.setter
    def device_type(self, new_val):
        self._device_type = new_val

    @property
    def created_date(self) -> str:
        return self._created_date

    @created_date.setter
    def created_date(self, new_val):
        self._created_date = new_val

    def __init__(self, data_aid: str,
                 app_info_href: str,
                 pid: str,
                 title: str,
                 rating_value: str,
                 best_rating: str,
                 review_count: str,
                 position: str,
                 country: str,
                 ranking_type: str,
                 device_type: str,
                 created_date: str):
        self.data_aid = data_aid
        self.app_info_href = app_info_href
        self.pid = pid
        self.title = title
        self.rating_value = rating_value
        self.best_rating = best_rating
        self.review_count = review_count
        self.position = position
        self.country = country
        self.ranking_type = ranking_type
        self.device_type = device_type
        self.created_date = created_date

    def __str__(self):
        return "data_aid: " + self.data_aid + \
               ", app_info_href: " + self.app_info_href + \
               ", pid: " + self.pid + \
               ", title: " + self.title + \
               ", rating_value: " + self.rating_value + \
               ", best_rating: " + self.best_rating + \
               ", review_count: " + self.review_count + \
               ", position: " + self.position + \
               ", country: " + self.country + \
               ", ranking_type: " + self.ranking_type + \
               ", device_type: " + self.device_type + \
               ", created_date: " + self.created_date
