from dagster import solid, InputDefinition
import os

from src.dagster_objects.commons.types import DataMartConnectionType
from src.data_uploaders.datamart_connection import DataMartConnection
from src.data_uploaders.db_data_loaders import PostgresDataLoader
from src.data_uploaders.db_data_uploaders import PostgresDataUploader


@solid
def get_datamart_connection(_, app_config):
    # TODO myby change name to PGDataMartConnection
    datamart_connection = DataMartConnection(
        dbname=app_config['database.games-analytics.database'],
        dbuser=app_config['database.games-analytics.user'],
        dbhost=app_config['database.games-analytics.ip'],
        dbpass=app_config['database.games-analytics.password'],
        dbport=app_config['database.games-analytics.port'])
    return datamart_connection


@solid
def get_datamart_connection_w_env(
        context, db_envarn: str, uname_envarn: str,
        ip_envarn: str, pass_envarn: str, port_envarn: str):
    # TODO myby change name to PGDataMartConnection
    context.log.info('Attempting to connect to datamart')
    datamart_connection = DataMartConnection(
        dbname=os.getenv(db_envarn),
        dbuser=os.getenv(uname_envarn),
        dbhost=os.getenv(ip_envarn),
        dbpass=os.getenv(pass_envarn),
        dbport=os.getenv(port_envarn))
    context.log.info('Connection successfull')
    return datamart_connection


@solid(
    input_defs=[
        InputDefinition(
            name='datamart_connection', dagster_type=DataMartConnectionType,
            description='DataMart connection object'),
    ]
)
def get_db_data_uploader(_, datamart_connection):
    return PostgresDataUploader(datamart_connection=datamart_connection)


@solid(
    input_defs=[
        InputDefinition(
            name='datamart_connection', dagster_type=DataMartConnectionType,
            description='DataMart connection object'),
    ]
)
def get_db_data_loader(_, datamart_connection):
    return PostgresDataLoader(datamart_connection=datamart_connection)
