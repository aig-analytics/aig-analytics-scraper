from dagster import solid, InputDefinition

from src.dagster_objects.commons.types import DataMartConnectionType, WebExecutorType, ProxyEngineType
from src.engine.executor.proxy_engines import PublicProxyEngine
from src.engine.executor.web_request_executors import WebRequestExecutor, \
    SingleWebRequestExecutor


@solid(
    input_defs=[
        InputDefinition(
            name='datamart_connection', dagster_type=DataMartConnectionType,
            description='DataMart connection object'),
    ]
)
def get_proxy_engine(_, datamart_connection: DataMartConnectionType):
    return PublicProxyEngine(datamart_connection=datamart_connection)


@solid(
    input_defs=[
        InputDefinition(
            name='proxy_engine', dagster_type=ProxyEngineType,
            description='Proxy engine')
    ]
)
def get_single_web_executor(_, proxy_engine):
    return SingleWebRequestExecutor(proxy_engine=proxy_engine)
