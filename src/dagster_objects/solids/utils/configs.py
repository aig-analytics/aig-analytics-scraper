from dagster import solid

from src.data_uploaders.datamart_connection import DataMartConnection
from src.engine.executor.proxy_engines import PublicProxyEngine
from src.engine.executor.web_request_executors import WebRequestExecutor, \
    SingleWebRequestExecutor
from src.utils.configs import ApplicationConfig


@solid
def get_config(_, cfg_path: str):
    return ApplicationConfig(
        cfg_path=cfg_path).config


# @solid
# def get_proxy_engine(_, datamart_connection):
#     return PublicProxyEngine(datamart_connection=datamart_connection)
#
#
# @solid
# def get_single_web_executor(_, proxy_engine):
#     return SingleWebRequestExecutor(proxy_engine=proxy_engine)
#
#
# @solid
# def get_datamart_connection(_, app_config):
#     # TODO myby change name to PGDataMartConnection
#     datamart_connection = DataMartConnection(
#         dbname=app_config['database.games-analytics.database'],
#         dbuser=app_config['database.games-analytics.user'],
#         dbhost=app_config['database.games-analytics.ip'],
#         dbpass=app_config['database.games-analytics.password'],
#         dbport=app_config['database.games-analytics.port'])
#     return datamart_connection
