from dagster import InputDefinition, solid

from src.dagster_objects.commons.types import DataMartConnectionType
from src.data_uploaders.db_data_loaders import PostgresDataLoader
from src.engine.steam_processing_engine import SteamProcessingEngine
from src.data_uploaders.db_data_uploaders import PostgresDataUploader
from src.engine.steam_members_engine import SteamMembersEngine
from src.engine.executor.proxy_engines import PublicProxyEngine
from src.engine.executor.web_request_executors import SingleWebRequestExecutor


# @solid()
# def get_new_ids_limit_size(_):
#     return 200
#
# @solid()
# def get_valid_limit_ids_size(_):
#     return 100
#
# @solid()
# def get_invalid_limit_ids_size(_):
#     return 10
#
# @solid()
# def get_batch_size_limit(_):
#     return 200


@solid()
def get_steam_data_processing_engine(_,
                                     web_executor):
    return SteamProcessingEngine(request_execution_engine=web_executor)


@solid()
def get_steam_app_lkp_info_ids_for_refresh(context,
                                           postgres_data_loader,
                                           valid_limit_size: int = 250,
                                           invalid_limit_size=250):
    valid_ids = \
        postgres_data_loader.get_valid_steam_app_info_lkp_ids(
            valid_ids_limit=valid_limit_size)
    invalid_ids = \
        postgres_data_loader.get_invalid_steam_app_info_lkp_ids(
            invalid_ids_limit=invalid_limit_size)
    return valid_ids + invalid_ids


@solid()
def get_new_steam_app_lkp_ids(context,
                              postgres_data_loader,
                              steam_processing_engine,
                              ids_limit,
                              ids_to_exclude):
    ids = steam_processing_engine.get_all_steam_apps_ids()
    ids_list = postgres_data_loader.get_steam_app_lkp_all_ids()
    for i in ids_to_exclude:
        ids_list += i

    return list(set(ids) - set([int(x) for x in ids_list if x.isdigit()]))[0:ids_limit]


@solid()
def process_steam_whishlist(context,
                            steam_processing_engine,
                            postgres_data_uploader):
    wishlist_titles_ids, whishlist_titles = \
        steam_processing_engine.run_steam_ranked_title(exec_type="WHISHLIST")
    postgres_data_uploader.upload_whishlist_titles(whishlist_entries=whishlist_titles)
    return wishlist_titles_ids


@solid()
def process_steam_bestsellers(context,
                              steam_processing_engine,
                              postgres_data_uploader):
    bestsellers_titles_ids, bestsellers_titles = \
        steam_processing_engine.run_steam_ranked_title(exec_type="BESTSELLER")
    postgres_data_uploader.upload_bestsellers_titles(
        bestseller_entries=bestsellers_titles)
    return bestsellers_titles_ids


@solid()
def upload_steam_app_lkp_info_batch(context,
                                    postgres_data_uploader,
                                    steam_processing_engine,
                                    ids: list,
                                    batch_size_limit: int = 200):
    for i in range(0, len(ids), batch_size_limit):
        ids_to_process = ids[i:i + batch_size_limit]
        sail_list = \
            steam_processing_engine.get_all_steam_apps_lookup_info(ids_to_process)
        postgres_data_uploader.upload_steam_app_info_lkp(sail_list)


@solid()
def upload_steam_app_info_batch(context,
                                postgres_data_uploader,
                                steam_processing_engine,
                                ids: list,
                                batch_size_limit: int = 200):
    for i in range(0, len(ids), batch_size_limit):
        ids_to_process = ids[i:i + batch_size_limit]
        app_id_info_list = \
            steam_processing_engine.run_steam_app(app_id_list=ids_to_process)
        postgres_data_uploader.upload_steam_app_info(app_id_info_list)


@solid()
def merge_ids(context,
              ids_list: list):
    # ls = ["400", "270150"]
    ls = []
    for i in ids_list:
        ls += i
    return ls


@solid(
    input_defs=[
        InputDefinition(
            name='fresh_members', dagster_type=int, default_value=100,
            description='how many new members should be scraped'),
        InputDefinition(
            name='updated_members', dagster_type=int, default_value=100,
            description='how many updated members should be scraped'),
        InputDefinition(
            name='datamart_connection', dagster_type=DataMartConnectionType,
            description='DataMart connection object'),
    ]
)
def get_steam_members_scraped_ids(
        context, fresh_members: int, updated_members: int,
        datamart_connection):

    pd_loader = PostgresDataLoader(datamart_connection=datamart_connection)

    # context.solid_config["reverse"]

    return pd_loader.get_steam_members_scraped_ids(
        fresh_members=fresh_members, updated_members=updated_members)


@solid
def parse_desired_players(
        context, datamart_connection, scraped_ids: list):
    pe = PublicProxyEngine(datamart_connection)
    wre = SingleWebRequestExecutor(pe)
    sme = SteamMembersEngine(request_execution_engine=wre)

    sme.parse_desired_players(scraped_ids)
    return sme


@solid
def pd_upload_steam_members_info(
        context, datamart_connection,
        steam_members_engine):
    smpe = steam_members_engine
    pdu = PostgresDataUploader(datamart_connection)
    context.log.info(smpe.games_df.head().to_string())
    pdu.pd_upload_steam_members_info(
        dfs=[smpe.friends_df, smpe.games_df, smpe.wl_df,
             smpe.fllwd_games_df, smpe.rec_played_df, smpe.revs_df],
        tab_ns=['steam_members_players', 'steam_members_games',
                'steam_members_wishlist', 'steam_members_fllwd_games',
                'steam_members_rec_played', 'steam_members_reviews'])


if __name__ == '__main__':
    def m(*kwargs):
        res = []
        for i in kwargs:
            res += i
        return res


    ls1 = [1, 2, 3]
    ls2 = [3, 4, 5]
    ls3 = [4, 5, 6]
    w = m(ls1, ls2, ls3)
    print(w)
