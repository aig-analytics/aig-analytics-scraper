from dagster import solid

from src.engine.stock_data_processing_engine import StockDataProcessingEngine


@solid()
def get_stock_data_processing_engine(_,
                                     web_executor):
    return StockDataProcessingEngine(request_execution_engine=web_executor)


@solid()
def process_wse_comp_data(_,
                          postgres_data_loader,
                          postgres_data_uploader,
                          stock_data_processing_engine):
    banker_shortcuts = postgres_data_loader.get_bankier_shortcuts()
    wse_companies = stock_data_processing_engine.get_banker_company_data(
        banker_shortcuts=banker_shortcuts)
    postgres_data_uploader.pd_upload_wse_company_stock_info(wse_companies_list=wse_companies)


@solid
def process_biznesradar_comp_data(_,
                                  postgres_data_loader,
                                  postgres_data_uploader,
                                  stock_data_processing_engine):
    biznesradar_shortcuts = postgres_data_loader.get_biznesradar_shortcuts()
    wse_companies = \
        stock_data_processing_engine.get_biznesradar_wse_company_data(
            shortcuts=biznesradar_shortcuts)

    # get financial reposrts
    fin_reports_df = \
        stock_data_processing_engine.\
        get_biznesradar_wse_company_financial_report(
            shortcuts=biznesradar_shortcuts)

    postgres_data_uploader.pd_upload_wse_company_biznesradar_info(
        biznesradar_companies_list=wse_companies, reports_df=fin_reports_df)




