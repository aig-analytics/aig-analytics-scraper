from dagster import InputDefinition, solid, Any

from src.dagster_objects.commons.types import DataMartConnectionType
from src.data_uploaders.db_data_uploaders import PostgresDataUploader
from src.engine.proxy_engine import ProxyParsingEngine


@solid()
def get_proxy_df(context, datamart_connection):

    proxy_engine = ProxyParsingEngine(datamart_connection=datamart_connection)
    proxy_df = proxy_engine.get_proxy_df()
    return proxy_df


@solid()
def upload_proxy_df(context, proxy_df, datamart_connection):
    pup = PostgresDataUploader(datamart_connection=datamart_connection)
    pup.upload_proxy_df(proxy_df=proxy_df)
