import os

from dagster import pipeline, execute_pipeline, DagsterInstance

from src.dagster_objects.solids.db.pg_connections import \
    get_datamart_connection_w_env, get_db_data_uploader, get_db_data_loader
from src.dagster_objects.solids.requests.rq_executors import \
    get_single_web_executor, get_proxy_engine
from src.dagster_objects.solids.stock_data import \
    get_stock_data_processing_engine, process_wse_comp_data, \
    process_biznesradar_comp_data
from src.utils.meta_utils import get_run_metadata


@pipeline
def process_wse_stock_data_pipeline():
    datamart_connection = get_datamart_connection_w_env()
    db_data_uploader = \
        get_db_data_uploader(datamart_connection=datamart_connection)
    db_data_loader = get_db_data_loader(datamart_connection=datamart_connection)
    proxy_engine = get_proxy_engine(datamart_connection=datamart_connection)
    web_executor = get_single_web_executor(proxy_engine=proxy_engine)
    stock_data_processing_engine = \
        get_stock_data_processing_engine(web_executor=web_executor)
    process_wse_comp_data(
        postgres_data_loader=db_data_loader,
        postgres_data_uploader=db_data_uploader,
        stock_data_processing_engine=stock_data_processing_engine)
    process_biznesradar_comp_data(
        postgres_data_loader=db_data_loader,
        postgres_data_uploader=db_data_uploader,
        stock_data_processing_engine=stock_data_processing_engine)


if __name__ == '__main__':
    os.environ["DAGSTER_HOME"] = '/home/simmy/Desktop/dagster_home2'
    # run_config = \
    #     {'solids': {
    #         'get_datamart_connection_w_env':
    #             {'inputs': {
    #                 'db_envarn': {'value': 'GAMES_ANALYTICS_DATAMART_DATABASE'},
    #                 'ip_envarn': {'value': 'GAMES_ANALYTICS_IP'},
    #                 'pass_envarn': {'value': 'GAMES_ANALYTICS_USER_PASSWORD'},
    #                 'port_envarn': {'value': 'GAMES_ANALYTICS_PORT'},
    #                 'uname_envarn': {'value': 'GAMES_ANALYTICS_USER'}}},
    #         'get_steam_members_scraped_ids': {
    #             'inputs': {
    #                 'fresh_members': {'value': 10},
    #                 'updated_members': {'value': 10}}}}}
    run_cfg = get_run_metadata(
        pipeline_name='process_wse_stock_data_pipeline', schedule_name='daily')
    # cfg = execute_solid(
    #     get_config, input_values={
    #         "cfg_path": "../utils/app.yaml"})
    # print(run_cfg)
    execute_pipeline(
        process_wse_stock_data_pipeline, run_config=run_cfg,
        instance=DagsterInstance.get())
