from dagster import pipeline

from src.dagster_objects.solids.proxy_processing import get_proxy_df, \
    upload_proxy_df
from src.dagster_objects.solids.db.pg_connections import \
    get_datamart_connection_w_env


@pipeline()
def proxy_update_pipeline():
    datamart_connection = get_datamart_connection_w_env()
    proxy_df = get_proxy_df(datamart_connection=datamart_connection)
    upload_proxy_df(datamart_connection=datamart_connection, proxy_df=proxy_df)

