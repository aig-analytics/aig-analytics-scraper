import os

from dagster import pipeline, execute_pipeline, DagsterInstance

from src.dagster_objects.solids.db.pg_connections import \
    get_datamart_connection_w_env, get_db_data_uploader, get_db_data_loader
from src.dagster_objects.solids.requests.rq_executors import \
    get_single_web_executor, get_proxy_engine
from src.dagster_objects.solids.steam_processing import \
    get_steam_members_scraped_ids
from src.dagster_objects.solids.steam_processing import \
    get_steam_data_processing_engine, get_new_steam_app_lkp_ids, \
    process_steam_whishlist, process_steam_bestsellers, \
    upload_steam_app_lkp_info_batch, merge_ids, \
    upload_steam_app_info_batch, get_steam_app_lkp_info_ids_for_refresh, \
    parse_desired_players, pd_upload_steam_members_info
from src.utils.meta_utils import get_run_metadata


@pipeline
def process_steam_data_pipeline():
    datamart_connection = get_datamart_connection_w_env()
    db_data_uploader = \
        get_db_data_uploader(datamart_connection=datamart_connection)
    db_data_loader = get_db_data_loader(datamart_connection=datamart_connection)
    proxy_engine = get_proxy_engine(datamart_connection=datamart_connection)
    web_executor = get_single_web_executor(proxy_engine=proxy_engine)
    steam_processing_engine = \
        get_steam_data_processing_engine(web_executor=web_executor)
    ids_to_refresh = get_steam_app_lkp_info_ids_for_refresh(
        postgres_data_loader=db_data_loader)

    wishlist_titles_ids = \
        process_steam_whishlist(
            steam_processing_engine=steam_processing_engine,
            postgres_data_uploader=db_data_uploader)
    bestseller_ids = \
        process_steam_bestsellers(
            steam_processing_engine=steam_processing_engine,
            postgres_data_uploader=db_data_uploader)
    new_ids = \
        get_new_steam_app_lkp_ids(
            postgres_data_loader=db_data_loader,
            steam_processing_engine=steam_processing_engine,
            ids_to_exclude=[ids_to_refresh, wishlist_titles_ids, bestseller_ids]
            # ids_to_exclude=[ids_to_refresh]

        )

    ids_to_load = merge_ids(
        ids_list=[ids_to_refresh, new_ids, wishlist_titles_ids, bestseller_ids]
        # ids_list=[ids_to_refresh, new_ids]
    )

    upload_steam_app_lkp_info_batch(
        postgres_data_uploader=db_data_uploader,
        steam_processing_engine=steam_processing_engine, ids=ids_to_load)

    upload_steam_app_info_batch(
        postgres_data_uploader=db_data_uploader,
        steam_processing_engine=steam_processing_engine, ids=ids_to_load)


@pipeline
def process_steam_members_pipeline():
    datamart_connection = get_datamart_connection_w_env()
    smi = get_steam_members_scraped_ids(datamart_connection=datamart_connection)
    pdp = parse_desired_players(datamart_connection, smi)
    pd_upload_steam_members_info(datamart_connection, pdp)


if __name__ == '__main__':
    os.environ["DAGSTER_HOME"] = '/home/kw/dagster_home'
    # run_config = \
    #     {'solids': {
    #         'get_datamart_connection_w_env':
    #             {'inputs': {
    #                 'db_envarn': {'value': 'GAMES_ANALYTICS_DATAMART_DATABASE'},
    #                 'ip_envarn': {'value': 'GAMES_ANALYTICS_IP'},
    #                 'pass_envarn': {'value': 'GAMES_ANALYTICS_USER_PASSWORD'},
    #                 'port_envarn': {'value': 'GAMES_ANALYTICS_PORT'},
    #                 'uname_envarn': {'value': 'GAMES_ANALYTICS_USER'}}},
    #         'get_steam_members_scraped_ids': {
    #             'inputs': {
    #                 'fresh_members': {'value': 10},
    #                 'updated_members': {'value': 10}}}}}

    from stock_pipelines import process_wse_stock_data_pipeline

    # run_cfg = get_run_metadata(
    #     pipeline_name='process_wse_stock_data_pipeline', schedule_name='daily')
    # # cfg = execute_solid(
    # #     get_config, input_values={
    # #         "cfg_path": "../utils/app.yaml"})
    # print(run_cfg)
    # execute_pipeline(
    #     process_wse_stock_data_pipeline, run_config=run_cfg)  # ,
    #     # instance=DagsterInstance.get())

    run_cfg = get_run_metadata(
        pipeline_name='process_steam_members_pipeline', schedule_name='daily')
    # cfg = execute_solid(
    #     get_config, input_values={
    #         "cfg_path": "../utils/app.yaml"})

    from pprint import pprint
    print(run_cfg)
    input('sanity check')

    from src.dagster_objects.pipelines.proxy_pipelines import proxy_update_pipeline

    execute_pipeline(
        process_steam_members_pipeline, run_config=run_cfg)  # ,
        # instance=DagsterInstance.get())

