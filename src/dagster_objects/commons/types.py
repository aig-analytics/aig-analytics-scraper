from dagster import DagsterType

from src.data_uploaders.datamart_connection import DataMartConnection
from src.data_uploaders.db_data_loaders import PostgresDataLoader
from src.data_uploaders.db_data_uploaders import PostgresDataUploader
from src.engine.executor.proxy_engines import BaseProxyEngine
from src.engine.executor.web_request_executors import WebRequestExecutor
from src.utils.configs import ApplicationConfig

AppCFGConfType = DagsterType(
    name='AppCFGConf',
    type_check_fn=lambda _, value: isinstance(value, ApplicationConfig),
    description='Application config type')

DataMartConnectionType = DagsterType(
    name='DataMartConnectionDag',
    type_check_fn=lambda _, value: isinstance(value, DataMartConnection),
    description='PG DataMart connection type')

WebExecutorType = DagsterType(
    name='WebExecutor',
    type_check_fn=lambda _, value: isinstance(value, WebRequestExecutor),
    description='Instance of WebExecutor')

ProxyEngineType = DagsterType(
    name='Proxy engine',
    type_check_fn=lambda _, value: isinstance(value, BaseProxyEngine),
    description='Instance of Proxy Engine')

PostgresDataLoaderType = DagsterType(
    name='PostgresDataLoader',
    type_check_fn=lambda _, value: isinstance(value, PostgresDataUploader),
    description='Instance of Postgres loader to query data from datamart')

PostgresDataUploaderType = DagsterType(
    name='PostgresDataUploader',
    type_check_fn=lambda _, value: isinstance(value, PostgresDataLoader),
    description='Instance of Postgres uploader to query data frrom datamart')
