from datetime import datetime
import pandas as pd
import psycopg2

from src.data_uploaders.datamart_connection import DataMartConnection
from src.utils.configs import ApplicationConfig


class PostgresDataLoader:

    @property
    def datamart_connection(self) -> DataMartConnection:
        return self._datamart_connection

    @datamart_connection.setter
    def datamart_connection(self, new_val):
        self._datamart_connection = new_val

    def __init__(self, datamart_connection):
        self._datamart_connection = datamart_connection

    def get_biznesradar_shortcuts(self):
        conn = self.datamart_connection.create_connection()
        cursor = conn.cursor()
        cmp_shortcuts_query = \
            "SELECT biznesradar_comp_shortcut " \
            "FROM datamart.wse_company_pull_lkp " \
            "WHERE biznesradar_comp_shortcut IS NOT NULL"
        cursor.execute(cmp_shortcuts_query)
        shortcuts = cursor.fetchall()
        shortcuts_list = [row[0] for row in shortcuts]
        conn.close()
        return shortcuts_list

    def get_bankier_shortcuts(self):
        conn = self.datamart_connection.create_connection()
        cursor = conn.cursor()
        cmp_shortcuts_query = "SELECT DISTINCT bnk_comp_shortcut FROM datamart.wse_company_pull_lkp"
        cursor.execute(cmp_shortcuts_query)
        shortcuts = cursor.fetchall()
        shortcuts_list = [row[0] for row in shortcuts]
        conn.close()
        return shortcuts_list

    def get_valid_steam_app_info_lkp_ids(self, valid_ids_limit: int = 250):
        conn = self.datamart_connection.create_connection()
        cursor = conn.cursor()

        query = "SELECT DISTINCT appid, last_modified_date " \
                "FROM datamart.STEAM_APP_INFO_LKP " \
                "WHERE title = 'INVALID' " \
                "ORDER BY last_modified_date " \
                "LIMIT {0}"
        cursor.execute(query.format(valid_ids_limit))
        ids = cursor.fetchall()
        conn.close()
        return [row[0] for row in ids]

    def get_invalid_steam_app_info_lkp_ids(self, invalid_ids_limit: int = 250):
        conn = self.datamart_connection.create_connection()
        cursor = conn.cursor()

        query = "SELECT DISTINCT appid, last_modified_date " \
                "FROM datamart.STEAM_APP_INFO_LKP " \
                "WHERE title != 'INVALID' " \
                "ORDER BY last_modified_date " \
                "LIMIT {0}"
        cursor.execute(query.format(invalid_ids_limit))
        ids = cursor.fetchall()
        conn.close()
        return [row[0] for row in ids]

    def get_steam_app_lkp_all_ids(self):
        conn = self.datamart_connection.create_connection()
        cursor = conn.cursor()
        app_id_select_query = "SELECT DISTINCT appid FROM datamart.STEAM_APP_INFO_LKP"
        cursor.execute(app_id_select_query)
        ids = cursor.fetchall()
        ids_list = [row[0] for row in ids]
        conn.close()
        return ids_list

    def get_all_steam_app_ids(self):
        conn = self.datamart_connection.create_connection()
        cursor = conn.cursor()
        app_id_select_query = "SELECT DISTINCT appid FROM datamart.STEAM_APP_INFO_VW"
        cursor.execute(app_id_select_query)
        ids = cursor.fetchall()
        ids_list = [row[0] for row in ids]
        conn.close()
        return ids_list

    def get_steam_members_scraped_ids(
            self, fresh_members: int = 1000, updated_members: int = 200,
            refresh_after: pd.Timedelta = pd.to_timedelta(2, unit='w')):
        conn = self.datamart_connection.get_engine()

        fresh_members_ids_query = \
            "SELECT player_id FROM datamart.steam_members_players tablesample SYSTEM (0.1) where " \
            "to_be_scraped = '1' and created_date = last_modified_date limit {}" \
                .format(fresh_members)

        # print(fresh_members_ids_query)

        fresh_members_ids = \
            list(pd.read_sql(
                sql=fresh_members_ids_query, con=conn)['player_id'])

        limiting_date = \
            (datetime.now() - refresh_after) \
                .replace(hour=0, minute=0, second=0, microsecond=0)

        updated_members_ids_query = \
            "SELECT player_id FROM datamart.steam_members_players tablesample SYSTEM (0.1) where 1=1" \
            "and last_modified_date::timestamp < '{}'::timestamp " \
            "and created_date != last_modified_date" \
            " order by last_modified_date::timestamp ASC limit {}" \
                .format(str(limiting_date), updated_members)

        updated_members_ids = \
            list(
                pd.read_sql(
                    sql=updated_members_ids_query, con=conn)['player_id'])

        full_list = fresh_members_ids + updated_members_ids

        # print(full_list)

        return full_list

    # def get_steam_members_stage_ids(
    #         self, steam_members_tabns: tuple,
    #         player_id_cn: str = 'player_id') -> tuple:
    #     ids_vs_tabns = []
    #     for sm_tn in steam_members_tabns:
    #         curr_tab_player_id_query = \
    #             'select distinct {} from {}'.format(player_id_cn, sm_tn)


if __name__ == "__main__":
    app_config = ApplicationConfig(cfg_path='../utils/local-app-never-commit.yaml').config
    pdl = PostgresDataLoader()
    pdl.create_connection(dbname=app_config['database.games-analytics.database'],
                          dbuser=app_config['database.games-analytics.user'],
                          dbhost=app_config['database.games-analytics.ip'],
                          dbpass=app_config['database.games-analytics.password'],
                          dbport=app_config['database.games-analytics.port'])
    # res = pdl.get_all_steam_app_ids()
    res = pdl.get_steam_members_scraped_ids()
    print(res)
    print(str(len(res)))
