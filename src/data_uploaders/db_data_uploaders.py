import pandas as pd

from src.data_uploaders.datamart_connection import DataMartConnection
from src.parsed_models.steam_app import SteamApplicationInfo
from src.parsed_models.whishlist_ranked_title import WhishlistRankedTitle
from src.utils.configs import ApplicationConfig


class PostgresDataUploader:

    # @property
    # def active_connection(self):
    #     return self._active_connection
    #
    # @active_connection.setter
    # def active_connection(self, new_val):
    #     self._active_connection = new_val

    @property
    def datamart_connection(self) -> DataMartConnection:
        return self._datamart_connection

    @datamart_connection.setter
    def datamart_connection(self, new_val):
        self._datamart_connection = new_val

    def __init__(self, datamart_connection):
        self.datamart_connection = datamart_connection

    def upload_whishlist_titles(self, whishlist_entries: list):
        conn = self.datamart_connection.create_connection()
        cursor = conn.cursor()
        postgres_insert_query = """
        INSERT INTO stage.STEAM_TOP_WHISHLIST_STG 
        (APPID, ITEMKEY, POSITION, HREF, TITLE, RELEASE_DATE, PRICE, CREATED_DATE) 
        VALUES 
        (%s,%s,%s,%s,%s,%s,%s,%s)
        """

        for whishlist_entry in whishlist_entries:
            record_to_insert = (
                whishlist_entry.appid, whishlist_entry.itemkey, whishlist_entry.position, whishlist_entry.href,
                whishlist_entry.title,
                whishlist_entry.release_date, whishlist_entry.price, whishlist_entry.created_date)
            cursor.execute(postgres_insert_query, record_to_insert)

        conn.commit()
        conn.close()

    def upload_bestsellers_titles(self, bestseller_entries: list):
        conn = self.datamart_connection.create_connection()
        cursor = conn.cursor()
        postgres_insert_query = """
        INSERT INTO stage.STEAM_TOP_BESTSELLERS_STG 
        (APPID, ITEMKEY, POSITION, HREF, TITLE, RELEASE_DATE, PRICE, CREATED_DATE) 
        VALUES 
        (%s,%s,%s,%s,%s,%s,%s,%s)
        """

        for bestseller_entry in bestseller_entries:
            record_to_insert = (
                bestseller_entry.appid, bestseller_entry.itemkey, bestseller_entry.position, bestseller_entry.href,
                bestseller_entry.title,
                bestseller_entry.release_date, bestseller_entry.price, bestseller_entry.created_date)
            cursor.execute(postgres_insert_query, record_to_insert)

        conn.commit()
        conn.close()

    ## TODO Refactor to pandas upload
    def upload_steam_app_info(self, steam_app_entries: list, commit_chunk: int = 250):
        conn = self.datamart_connection.create_connection()
        cursor = conn.cursor()
        postgres_insert_query = """
                INSERT INTO stage.STEAM_APP_INFO_STG 
                (APPID, TITLE, APP_GENERAL_TYPE, APP_CATEGORY, GAME_DESCRIPTION_SNIPPET, PRODUCER, PUBLISHER, 
                STEAM_REVIEWS_SUMMARY, STEAM_REVIEWS_DESCRIPTION, REVIEW_COUNT, RATING_VALUE, BEST_RATING, 
                STEAM_MEMBERS_CNT, IN_GAME_PLAYERS, ONLINE_PLAYERS_CNT, ON_GROUPS_CHAT,
                WORST_RATING, POPULAR_TAGS, CREATED_DATE, RELEASE_DATE) 
                VALUES 
                (%s,%s,%s,%s,%s,%s,%s,
                %s,%s,%s,%s,%s,
                %s,%s,%s,%s,%s,%s,%s,%s)
                """

        commit_chunk_tmp = commit_chunk
        for steam_app in steam_app_entries:
            record_to_insert = (steam_app.appid, steam_app.title, steam_app.app_general_type,
                                steam_app.app_category, steam_app.game_description_snippet, steam_app.producer,
                                steam_app.publisher, steam_app.steam_reviews_summary,
                                steam_app.steam_reviews_description,
                                steam_app.review_count, steam_app.rating_value,
                                steam_app.best_rating,
                                steam_app.members_count, steam_app.in_game_players,
                                steam_app.online_players_cnt, steam_app.on_groups_chat,
                                steam_app.worst_rating, steam_app.popular_tags,
                                steam_app.created_date, steam_app.release_date)
            cursor.execute(postgres_insert_query, record_to_insert)
            commit_chunk_tmp -= 1
            if commit_chunk_tmp <= 0:
                conn.commit()
                print("Performed tempporary commit. Reset count.")
                commit_chunk_tmp = commit_chunk

        conn.commit()
        conn.close()

    def upload_active_stats(self, steam_app_live_stats: list):
        conn = self.datamart_connection.create_connection()
        cursor = conn.cursor()
        postgres_insert_query = """
        INSERT INTO stage.STEAM_APP_LIVE_STATS
        (APPID, ACTIVE_PLAYERS, ACTIVE_GROUP_CHAT, created_date) 
        VALUES 
        (%s,%s,%s,%s)
        """

        for steam_app_live_stats in steam_app_live_stats:
            record_to_insert = (
                steam_app_live_stats.appid, steam_app_live_stats.active_players,
                steam_app_live_stats.active_group_chat, steam_app_live_stats.created_date)
            cursor.execute(postgres_insert_query, record_to_insert)

        conn.commit()
        conn.close()

    def upload_ms_ranked_titles(self, ms_ranked_titles_list: list, commit_chunk: int = 250):
        conn = self.datamart_connection.create_connection()
        cursor = conn.cursor()
        postgres_insert_query = """
                        INSERT INTO stage.MS_RANKED_TITLE 
                        (DATA_AID, APP_INFO_HREF, PID, TITLE, RATING_VALUE, BEST_RATING, REVIEW_COUNT, 
                        POSITION, COUNTRY, RANKING_TYPE, DEVICE_TYPE, CREATED_DATE) 
                        VALUES 
                        (%s,%s,%s,%s,%s,%s, %s,
                        %s, %s,%s,%s,%s)
                        """

        commit_chunk_tmp = commit_chunk
        for ms_ranked_title in ms_ranked_titles_list:
            record_to_insert = (ms_ranked_title.data_aid,
                                ms_ranked_title.app_info_href,
                                ms_ranked_title.pid,
                                ms_ranked_title.title,
                                ms_ranked_title.rating_value,
                                ms_ranked_title.best_rating,
                                ms_ranked_title.review_count,
                                ms_ranked_title.position,
                                ms_ranked_title.country,
                                ms_ranked_title.ranking_type,
                                ms_ranked_title.device_type,
                                ms_ranked_title.created_date)
            cursor.execute(postgres_insert_query, record_to_insert)
            commit_chunk_tmp -= 1
            if commit_chunk_tmp <= 0:
                conn.commit()
                print("Performed tempporary commit. Reset count.")
                commit_chunk_tmp = commit_chunk

        conn.commit()
        conn.close()

    def upload_ms_app_info(self, ms_app_info_list: list, commit_chunk: int = 250):
        conn = self.datamart_connection.create_connection()
        cursor = conn.cursor()
        postgres_insert_query = """
                        INSERT INTO stage.MS_APP_INFO
                        (PID, TITLE, PROD_CATEGORY, TAGS, 
                        PUBLISHER, DEVELOPER, RELEASE_DATE, ESTIMATED_SIZE, 
                        PRICE, CREATED_DATE) 
                        VALUES 
                        (%s,%s, %s,%s,
                        %s,%s,%s,%s,
                        %s,%s)
                        """

        commit_chunk_tmp = commit_chunk
        print("Start uploading MS APP INFO")
        for ms_app_info in ms_app_info_list:
            record_to_insert = (ms_app_info.pid,
                                ms_app_info.title,
                                ms_app_info.prod_category,
                                ms_app_info.tags,
                                ms_app_info.publisher,
                                ms_app_info.developer,
                                ms_app_info.release_date,
                                ms_app_info.estimated_size,
                                ms_app_info.price,
                                ms_app_info.created_date)
            cursor.execute(postgres_insert_query, record_to_insert)
            commit_chunk_tmp -= 1
            if commit_chunk_tmp <= 0:
                conn.commit()
                print("Performed tempporary commit. Reset count.")
                commit_chunk_tmp = commit_chunk

        conn.commit()
        conn.close()

    def upload_steam_app_info_lkp(self, steam_app_info_lkp_list: list, commit_chunk: int = 250):
        conn = self.datamart_connection.create_connection()
        cursor = conn.cursor()
        postgres_insert_query = """
                        INSERT INTO stage.STEAM_APP_INFO_LKP_STG
                        (APPID, TITLE, HEADER_IMAGE, PUBLISHERS, DEVELOPERS, CREATED_DATE, PRICE) 
                        VALUES 
                        (%s,%s,%s,%s,%s,%s, %s)
                        """

        commit_chunk_tmp = commit_chunk
        for steam_app_info_lkp in steam_app_info_lkp_list:
            record_to_insert = (steam_app_info_lkp.appid,
                                steam_app_info_lkp.title,
                                steam_app_info_lkp.header_image,
                                steam_app_info_lkp.publishers,
                                steam_app_info_lkp.developers,
                                steam_app_info_lkp.created_date,
                                steam_app_info_lkp.price)
            cursor.execute(postgres_insert_query, record_to_insert)
            commit_chunk_tmp -= 1
            if commit_chunk_tmp <= 0:
                conn.commit()
                print("Performed tempporary commit. Reset count.")
                commit_chunk_tmp = commit_chunk

        conn.commit()
        conn.close()

    def pd_upload_wse_company_biznesradar_info(
            self, biznesradar_companies_list: list,
            reports_df: pd.DataFrame = pd.DataFrame()):

        engine = self.datamart_connection.get_engine()
        conn = engine.connect()
        df = pd.DataFrame([(f.shortcut,
                            f.capitalization,
                            f.number_of_stocks,
                            f.actual_stock_price,
                            f.open_stock_price,
                            f.min_stock_price,
                            f.max_stock_price,
                            f.volume,
                            f.total_trading,
                            f.number_of_transactions,
                            f.sales_revenue,
                            f.operational_profit,
                            f.profit_before_tax,
                            f.assets,
                            f.equity_capital,
                            f.cwk,
                            f.cp,
                            f.roe,
                            f.roa,
                            f.altman,
                            f.piotroski,
                            f.execution_date)
                           for f in biznesradar_companies_list],
                          columns=['shortcut',
                                   'capitalization',
                                   'number_of_stocks',
                                   'actual_stock_price',
                                   'open_stock_price',
                                   'min_stock_price',
                                   'max_stock_price',
                                   'volume',
                                   'total_trading',
                                   'number_of_transactions',
                                   'sales_revenue',
                                   'operational_profit',
                                   'profit_before_tax',
                                   'assets',
                                   'equity_capital',
                                   'cwk',
                                   'cp',
                                   'roe',
                                   'roa',
                                   'altman',
                                   'piotroski',
                                   'execution_date'])
        df.to_sql('wse_company_biznesradar_info_stg',
                  con=conn,
                  if_exists='append',
                  index=False,
                  schema='stage')

        if not reports_df.empty:
            reports_df.to_sql(
                'wse_company_biznesradar_reports_stg', con=conn,
                if_exists='replace', index=False, schema='stage')

        conn.close()

    # def pd_upload_wse_company_biznesradar_reports(
    #         self, reports_df: pd.DataFrame):
    #
    #     engine = self.datamart_connection.get_engine()
    #     conn = engine.connect()
    #
    #     reports_df.to_sql(
    #         'wse_company_biznesradar_reports_stg', con=conn,
    #         if_exists='replace', index=False, schema='stage')
    #     conn.close()

    def pd_upload_wse_company_stock_info(self, wse_companies_list: list):
        engine = self.datamart_connection.get_engine()
        conn = engine.connect()
        df = pd.DataFrame([(f.banker_id,
                            f.banker_shortcut,
                            f.stock_price,
                            f.capitalization,
                            f.free_float,
                            f.stock_amount,
                            f.price_to_profit,
                            f.price_to_book_value,
                            f.created_date)
                           for f in wse_companies_list], columns=['banker_id',
                                                                  'banker_shortcut',
                                                                  'stock_price',
                                                                  'capitalization',
                                                                  'free_float',
                                                                  'stock_amount',
                                                                  'price_to_profit',
                                                                  'price_to_book_value',
                                                                  'created_date'])
        df.to_sql('bankier_company_stock_info_stg',
                  con=conn,
                  if_exists='append',
                  index=False,
                  schema='stage')
        conn.close()

    def pg_drop_steam_members_players_duplicates(
            self, friends_df_tabn: str, friends_tab_cn: str,
            scraped_urls: list):

        deled_urls_str = ', '.join(["'" + url + "'" for url in scraped_urls])

        del_query = \
            'delete from {} where {} in ({})' \
                .format(friends_df_tabn, friends_tab_cn, deled_urls_str)

        engine = self.datamart_connection.get_engine()
        with engine.connect() as cursor:
            cursor.execute(del_query)

        pass

    def pd_upload_steam_members_info(self, dfs: list, tab_ns: list):
        engine = self.datamart_connection.get_engine()

        for df, tab_n in zip(dfs, tab_ns):
            df.columns = [str(cn).lower() for cn in df.columns]
            df.drop_duplicates().to_sql(
                tab_n, con=engine, index=False, method='multi',
                if_exists='append', schema='stage')

    def upload_wse_company_stock_info(self, wse_companies_list: list):
        conn = self.datamart_connection.create_connection()
        cursor = conn.cursor()
        postgres_insert_query = """
                                INSERT INTO stage.WSE_COMPANY_STOCK_INFO
                                (BANKER_ID, BANKER_SHORTCUT, STOCK_PRICE, CAPITALIZATION, FREE_FLOAT, STOCK_AMOUNT, 
                                PRICE_TO_PROFIT, PRICE_TO_BOOK_VALUE, CREATED_DATE) 
                                VALUES 
                                (%s,%s,%s,%s,%s,%s,%s,%s,%s)
                                """

        for wse_company in wse_companies_list:
            record_to_insert = (wse_company.banker_id,
                                wse_company.banker_shortcut,
                                wse_company.stock_price,
                                wse_company.capitalization,
                                wse_company.free_float,
                                wse_company.stock_amount,
                                wse_company.price_to_profit,
                                wse_company.price_to_book_value,
                                wse_company.created_date)
            cursor.execute(postgres_insert_query, record_to_insert)

        conn.commit()
        conn.close()

    def upload_proxy_df(self, proxy_df: pd.DataFrame):
        engine = self.datamart_connection.get_engine()
        conn = engine.connect()

        del_query = 'truncate datamart.free_proxy_list'

        with engine.connect() as cursor:
            cursor.execute(del_query)

        proxy_df.to_sql(
            'free_proxy_list',
            con=conn,
            if_exists='append',
            index=False,
            schema='datamart')


if __name__ == '__main__':
    app_config = ApplicationConfig(cfg_path="../utils/local-app-never-commit.yaml").config
    pdu = PostgresDataUploader(dbname=app_config['database.games-analytics.database'],
                               dbuser=app_config['database.games-analytics.user'],
                               dbhost=app_config['database.games-analytics.ip'],
                               dbpass=app_config['database.games-analytics.password'],
                               dbport=app_config['database.games-analytics.port'])

    rec1 = WhishlistRankedTitle("1", "1", "1", "1", "1", "1", "1", "1")
    rec2 = WhishlistRankedTitle("2", "2", "2", "2", "2", "2", "2", "2")
    ls = [rec1, rec2]
    pdu.upload_whishlist_titles(ls)
    pdu.upload_bestsellers_titles(ls)

    app1 = SteamApplicationInfo("1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1")
    app2 = SteamApplicationInfo("2", "2", "2", "2", "2", "2", "2", "2", "1", "1", "1", "1", "1", "1", "1")
    apps = [app1, app2]
    pdu.upload_steam_app_info(apps)
