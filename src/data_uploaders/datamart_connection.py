import psycopg2

from sqlalchemy import create_engine


class DataMartConnection:

    @property
    def dbname(self) -> str:
        return self._dbname

    @dbname.setter
    def dbname(self, new_val):
        self._dbname = new_val

    @property
    def dbuser(self) -> str:
        return self._dbuser

    @dbuser.setter
    def dbuser(self, new_val):
        self._dbuser = new_val

    @property
    def dbhost(self) -> str:
        return self._dbhost

    @dbhost.setter
    def dbhost(self, new_val) -> str:
        self._dbhost = new_val

    @property
    def dbport(self) -> str:
        return self._dbport

    @dbport.setter
    def dbport(self, new_val):
        self._dbport = new_val

    def __init__(self, dbname: str, dbuser: str, dbhost: str, dbpass: str, dbport: str = '5432'):
        self.dbname = dbname
        self.dbuser = dbuser
        self.dbhost = dbhost
        self._dbpass = dbpass
        self.dbport = dbport

    def create_connection(self):
        return psycopg2.connect(dbname=self.dbname, user=self.dbuser, host=self.dbhost, password=self._dbpass,
                                port=self.dbport)

    def get_engine(self):
        return create_engine(
            'postgresql://{0}:{1}@{2}:{3}/{4}'.format(self._dbuser, self._dbpass, self.dbhost, self.dbport,
                                                      self.dbname))
