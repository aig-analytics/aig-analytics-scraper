import os
import numpy as np
import pandas as pd

from src.data_uploaders.datamart_connection import DataMartConnection


# GAMES_ANALYTICS_IP="games_analytics_ip_ph"
# GAMES_ANALYTICS_USER="games_analytics_uname_ph"
# GAMES_ANALYTICS_USER_PASSWORD="games_analytics_pass_ph"
# GAMES_ANALYTICS_USER_DATABASE="games_analytics_dbname_ph"


def get_process_name(pipeline_name: str, schedule_name: str) -> str:
    return f'{pipeline_name}_{schedule_name}'


EXPECTED_META_CNS = [
    'process_id',
    'process_name',
    'pipeline_name',
    'schedule_name',
    'param_name',
    'param_value',
    'solid_name',
    'param_config_type',
    'type_name']

TYPE_CASTING_MAP = {
    'int': int,
    'float': float,
    'str': str,
    'list': list,
    'dict': dict
}


def append_param_to_run_cfg(pd_row: pd.Series, curr_solid_cfg: dict):
    # print('### pd_row ###')
    # print(pd_row)

    param_config_type = pd_row['param_config_type']

    if param_config_type not in curr_solid_cfg:
        curr_solid_cfg[param_config_type] = {}

    casting_callable = TYPE_CASTING_MAP.get(pd_row['type_name'], None)
    if casting_callable is None:
        raise TypeError(
            'Not supported type: {}. Check `TYPE_CASTING_MAP`'
            .format(pd_row['type_name']))

    curr_solid_cfg[param_config_type][pd_row['param_name']] = \
        {"value": casting_callable(pd_row['param_value'])}


def get_meta_db_connection(
        meta_db_envarn: str = 'GAMES_ANALYTICS_META_DATABASE',
        meta_uname_envarn: str = 'GAMES_ANALYTICS_USER',
        meta_ip_envarn: str = 'GAMES_ANALYTICS_IP',
        meta_pass_envarn: str = 'GAMES_ANALYTICS_USER_PASSWORD') -> DataMartConnection:

    return DataMartConnection(
        dbname=os.getenv(meta_db_envarn), dbuser=os.getenv(meta_uname_envarn),
        dbhost=os.getenv(meta_ip_envarn), dbpass=os.getenv(meta_pass_envarn),
        dbport='5432')


def get_run_metadata(
        pipeline_name: str, schedule_name: str,
        src_meta_schema: str = 'dagitdata',
        src_meta_view_name: str = 'v_process_param_lkp'):

    datamart_connection = get_meta_db_connection()

    process_name = get_process_name(
        pipeline_name=pipeline_name, schedule_name=schedule_name)

    q = \
        f"select * from {src_meta_schema}.{src_meta_view_name} "  \
        f"where process_name='{process_name}'"

    print(q)

    cfg_tab = pd.read_sql(q, datamart_connection.get_engine())

    np.testing.assert_array_equal(list(cfg_tab.columns), EXPECTED_META_CNS)

    run_cfg = {
        "solids": {}}

    grpd_cfg_tab = cfg_tab.groupby('solid_name')

    for solid_n, solid_params_df in grpd_cfg_tab:

        if solid_n not in run_cfg["solids"]:
            run_cfg["solids"][solid_n] = {}

        solid_params_df.apply(
            append_param_to_run_cfg,
            **{'curr_solid_cfg': run_cfg["solids"][solid_n]}, axis=1)

    return run_cfg


if __name__ == '__main__':

    run_config = \
        get_run_metadata(
            pipeline_name='proxy_update_pipeline',
            schedule_name='daily')

    from dagster import execute_pipeline
    from pprint import pprint

    pprint(run_config)
    input('sanity check')

    os.getenv('')

    from src.workflows.dagster_example import process_steam_members_pipeline
    from src.dagster_objects.pipelines.proxy_pipelines import proxy_update_pipeline

    execute_pipeline(
        pipeline=proxy_update_pipeline, run_config=run_config)
