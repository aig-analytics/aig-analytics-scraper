import pandas as pd
import yaml
from warnings import warn


def constant(f):
    """
    Makes desired property a constant when used on it as a decorator
    Parameters
    ----------
    f: object

    Returns
    -------
    property
        desired property as a constant

    """

    def fset(self, value):
        raise TypeError

    def fget(self):
        return f()

    return property(fget, fset)


def warn_and_set_default(
        warn_msg: str, trgt_obj: object, set_attrn: str, default_val: str,
        verbose: bool =False):
    """
    Warns and sets provided default value

    Parameters
    ----------
    warn_msg: str
        message to be displayed when setting default
    trgt_obj: object
        object which property will be set to deafult
    set_attrn: str
        attribute to be set name
    default_val: object
        default value to be set

    Returns
    -------
    None
        None

    """
    if verbose:
        warn(warn_msg)
    if not default_val:
        raise ValueError('The provided default is empty!')
    setattr(trgt_obj, set_attrn, default_val)


def safe_to_int(input_val: object) -> int or None:
    try:
        return int(input_val)
    except Exception as exc:
        # print(
        #     'During conversion the following error occurred: {} \n for value: {}'
        #     .format(exc, input_val))
        #
        # print('Returning None')
        return None


def read_yaml_safely(pth_to_yaml: str) -> dict:
    """
    Reads yaml safely as dict. Returns empty dict on fail.

    Parameters
    ----------
    pth_to_yaml: str
        str pth to desired yaml

    Returns
    -------
    dict
        Read yaml on success. empty on fail

    """
    try:
        with open(pth_to_yaml, 'r') as des_yml:
            return des_yml.read()
    except FileNotFoundError as fnf_exc:
        print('File was not found under: {}'.format(pth_to_yaml))
        return {}


def drop_df_cns_safely(input_df: pd.DataFrame, drpd_cns: list):

    df_w_cols_drpd = input_df
    for drpd_cn in drpd_cns:
        if drpd_cn in df_w_cols_drpd.columns:
            df_w_cols_drpd = df_w_cols_drpd.drop(columns=[drpd_cn])
    return df_w_cols_drpd


# input_str_dt => analysed_txt.split('.')[1].strip().replace('Last edited', '').strip()
def append_curr_year_when_missing(input_str_dt: str) -> str:
    """
    Attempts to fix steam dt string -> convert i.e. 1 July into pandas
    connvertible string - 1 July, <curr year>

    Parameters
    ----------
    input_str_dt: str
        string to be checked and fixed

    Returns
    -------
    str
        fixed string

    """

    fxd_str_dt = input_str_dt
    if len(fxd_str_dt.split(' ')) < 3:
        fxd_str_dt = '{}, {}'.format(input_str_dt, pd.datetime.now().year)

    return fxd_str_dt


def safe_to_pd_dt(input_str: str) -> pd.datetime or None:
    """
    Safely casts string to datetime. On fail returns None

    Parameters
    ----------
    input_str: str
        str to be converted into datetime

    Returns
    -------
    pd.datetime or None
        conversion result or None

    """

    res_dt = None
    try:
        res_dt = pd.to_datetime(input_str)
    except Exception as exc:
        pass
        # print(
        #     'While attempting to convert {} to datetime following error '
        #     'occured: {}'.format(input_str, exc))

    return res_dt
