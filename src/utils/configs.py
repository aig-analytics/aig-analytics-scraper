import yaml
from envyaml import EnvYAML


class ApplicationConfig:

    @property
    def config(self) -> dict:
        return self._config

    @config.setter
    def config(self, new_val):
        self._config = new_val

    def __init__(self, cfg_path: str = 'app.yaml'):
        self.config = EnvYAML(cfg_path)


if __name__ == '__main__':
    app_config = ApplicationConfig(cfg_path='app.yaml').config
    print(str(app_config['database.games-analytics.ip']))
    print(str(app_config['database.games-analytics.port']))
