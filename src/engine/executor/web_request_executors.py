import time
import traceback
from abc import ABC, abstractmethod

import requests
from bs4 import BeautifulSoup

from src.data_uploaders.datamart_connection import DataMartConnection
from src.engine.executor.proxy_engines import PublicProxyEngine
from src.utils.configs import ApplicationConfig


class WebRequestExecutor(ABC):
    @abstractmethod
    def get_response(self,
                     url: str,
                     use_local_machine: bool = True,
                     use_proxy: bool = False,
                     wait_upfront: int = 0):
        pass

    @abstractmethod
    def get_soup(self,
                 url: str,
                 use_local_machine: bool = True,
                 use_proxy: bool = False,
                 wait_upfront: int = 0,
                 content_type: str = "html.parser"):
        pass


class SimpleWebRequestExecutor(WebRequestExecutor):

    def get_response(self, url: str,
                     use_local_machine: bool = True,
                     wait_upfront: int = 0,
                     use_proxy: bool = False):
        if wait_upfront > 0:
            time.sleep(wait_upfront)
        return requests.get(url=url)

    def get_soup(self, url: str,
                 use_local_machine: bool = True,
                 use_proxy: bool = False,
                 wait_upfront: int = 0,
                 content_type: str = "html.parser"):
        if wait_upfront > 0:
            time.sleep(wait_upfront)
        response = self.get_response(url=url)
        return BeautifulSoup(response.text, content_type)


class SingleWebRequestExecutor(WebRequestExecutor):

    @property
    def proxy_engine(self) -> PublicProxyEngine:
        return self._proxy_engine

    @proxy_engine.setter
    def proxy_engine(self, new_val):
        self._proxy_engine = new_val

    def __init__(self, proxy_engine: PublicProxyEngine = None):
        if proxy_engine:
            self.proxy_engine = proxy_engine
        else:
            print("Proxy engine not set in SingleWebRequestExecutor.")

    # @TODO add kwargs for requests.get
    def get_response(self,
                     url: str,
                     use_local_machine: bool = True,
                     use_proxy: bool = False,
                     wait_upfront: int = 0):
        try:
            if wait_upfront > 0:
                time.sleep(wait_upfront)
            return \
                self._make_request(
                    url=url, use_local_machine=use_local_machine,
                    use_proxy=use_proxy)
        except Exception:
            # assuming that single request should not break the execution so
            # letting caller handle None response
            print("Critical failure during request execution.")
            traceback.print_exc()
            return None

    def get_soup(self,
                 url: str,
                 use_local_machine: bool = True,
                 use_proxy: bool = False,
                 wait_upfront: int = 0,
                 content_type: str = "html.parser"):
        response = self.get_response(url=url,
                                     use_local_machine=use_local_machine,
                                     use_proxy=use_proxy,
                                     wait_upfront=wait_upfront)
        return self._to_soup(response=response.text, content_type=content_type)

    def _is_valid_response(self, response_text):
        return response_text != '' and response_text != 'null' and response_text is not None

    def _make_request(self,
                      url: str,
                      use_local_machine: bool = True,
                      use_proxy: bool = False):
        if use_proxy and self.proxy_engine is None:
            raise ValueError("Cannot use proxy as proxyEngine is None")
        proxies = ''
        if use_proxy:
            proxies = self.proxy_engine.get_proxies()
        response = None
        ## try 3 times
        for i in range(0, 3):
            if use_local_machine:
                response = self._get(url=url, proxies='')
                if i == 1 and use_proxy:
                    use_local_machine = False
            else:
                print("Using proxies: " + str(proxies))
                response = self._get(url=url, proxies=proxies)
            response_text = response.text
            response_status_code = response.status_code
            if response_status_code == 200 and self._is_valid_response(response_text):
                return response
        return response

    def _get(self, url: str, proxies):
        if proxies:
            return requests.get(url, proxies=proxies)
        else:
            return requests.get(url)

    def _to_soup(self, response: str, content_type: str):
        return BeautifulSoup(response, content_type)


if __name__ == '__main__':
    app_config = ApplicationConfig(cfg_path='../../utils/local-app-never-commit.yaml').config

    datamart_connection = DataMartConnection(dbname=app_config['database.games-analytics.database'],
                                             dbuser=app_config['database.games-analytics.user'],
                                             dbhost=app_config['database.games-analytics.ip'],
                                             dbpass=app_config['database.games-analytics.password'],
                                             dbport=app_config['database.games-analytics.port'])

    pe = PublicProxyEngine(datamart_connection=datamart_connection)

    swre = SingleWebRequestExecutor(proxy_engine=pe)
    response = swre.get_response(
        url='https://store.steampowered.com/search/?ignore_preferences=1&filter=popularwishlist',
        use_local_machine=False,
        use_proxy=True)
    print(response)
