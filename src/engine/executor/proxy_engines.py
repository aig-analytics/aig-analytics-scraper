import random
from abc import ABC, abstractmethod

import pandas as pd
import pandas.io.sql as sqlio
from telnetlib import Telnet

from src.data_uploaders.datamart_connection import DataMartConnection
from src.utils.configs import ApplicationConfig
from src.utils.gen_purp_utils import constant


class BaseProxyEngine(ABC):

    @abstractmethod
    def get_proxies(self):
        pass


class PublicProxyEngine(BaseProxyEngine):
    @property
    def datamart_connection(self) -> DataMartConnection:
        return self._datamart_connection

    @datamart_connection.setter
    def datamart_connection(self, new_val):
        self._datamart_connection = new_val

    @property
    def free_proxy_list(self) -> pd.DataFrame:
        return self._free_proxy_list

    @free_proxy_list.setter
    def free_proxy_list(self, new_val):
        self._free_proxy_list = new_val

    @constant
    def PROXY_SQL():
        return "SELECT * FROM datamart.free_proxy_list"

    def __init__(self, datamart_connection):
        self.datamart_connection = datamart_connection
        self._refresh_proxy_list()

    def get_proxies(self):
        proxy_alive = False
        while not proxy_alive:
            idx = random.randint(0, len(self.free_proxy_list) - 1)
            des_proxy_row = self.free_proxy_list.iloc[idx]
            # hostname, port
            host_address = des_proxy_row['hostname']
            port = des_proxy_row['port']
            try:
                with Telnet(host_address, port, timeout=3) as tn:
                    proxy_alive = True
            except:
                print('proxy: {}:{} is dead'.format(host_address, port))

        des_proxy_str = 'http://{}:{}'.format(host_address, port)
        proxies = {
            'http:': des_proxy_str,
            'https:': des_proxy_str}
        return proxies

    def _get_free_proxy_list(self):
        active_conn = self.datamart_connection.create_connection()
        dat = sqlio.read_sql_query(self.PROXY_SQL, active_conn)
        active_conn.close()
        return dat

    def _filter_active_proxies(self, proxy_df: pd.DataFrame):
        return proxy_df[proxy_df['is_active'] == '1']

    def _refresh_proxy_list(self):
        self.free_proxy_list = self._filter_active_proxies(self._get_free_proxy_list())


if __name__ == '__main__':
    app_config = ApplicationConfig(cfg_path='../../utils/local-app-never-commit.yaml').config

    datamart_connection = DataMartConnection(dbname=app_config['database.games-analytics.database'],
                                             dbuser=app_config['database.games-analytics.user'],
                                             dbhost=app_config['database.games-analytics.ip'],
                                             dbpass=app_config['database.games-analytics.password'],
                                             dbport=app_config['database.games-analytics.port'])

    swre = PublicProxyEngine(datamart_connection=datamart_connection)
    print(swre.free_proxy_list.to_string())
    print(swre.get_proxies())
