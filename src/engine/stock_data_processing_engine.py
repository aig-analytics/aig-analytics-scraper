from datetime import datetime

from bs4.element import Tag
import pandas as pd
import re

from src.engine.executor.web_request_executors import WebRequestExecutor, \
    SingleWebRequestExecutor, \
    SimpleWebRequestExecutor
from src.parsed_models.bankier_company_stock_info import BankierCompanyStockInfo
from src.parsed_models.biznesradar_company_stock_info import \
    BiznesRadarCompanyStockInfo
from src.utils.gen_purp_utils import constant


class StockDataProcessingEngine:

    @constant
    def _BANKER_URL_BASE():
        return "https://www.bankier.pl/inwestowanie/profile/quote.html?symbol="

    @constant
    def _BIZNESRADAR_URL_BASE():
        return "https://www.biznesradar.pl/notowania/"

    @property
    def request_execution_engine(self) -> WebRequestExecutor:
        return self._request_execution_engine

    @request_execution_engine.setter
    def request_execution_engine(self, new_val):
        self._request_execution_engine = new_val

    def __init__(self, request_execution_engine: WebRequestExecutor = SimpleWebRequestExecutor()):
        self.request_execution_engine = request_execution_engine

    def get_banker_company_data(self, banker_shortcuts: list):
        execution_date = datetime.today().strftime('%Y-%m-%d')
        cmp_stock_infos = []
        for banker_shortcut in banker_shortcuts:
            try:
                actual_url = self._BANKER_URL_BASE + banker_shortcut
                # simple requests - never use proxy for that
                soup = self.request_execution_engine.get_soup(url=actual_url,
                                                              use_local_machine=True,
                                                              use_proxy=False,
                                                              content_type="html.parser",
                                                              wait_upfront=1)
                # response = requests.get(actual_url)
                # soup = BeautifulSoup(response.text, "html.parser")
                curr_stock_price = \
                    soup.find("div", {"id": "pageMainContainer975"}).find("div", {"class": "profilLast"}).text.split(
                        " ")[0]
                curr_capitalization = "UNK"
                curr_free_float = "UNK"
                curr_stock_amount = "UNK"
                curr_price_to_profit = "UNK"
                curr_price_to_book = "UNK"

                elements = \
                    soup.find("div", {"id": "boxStockRations"}).find("div", {"class": "boxContent boxTable"}).contents[
                        1].contents[3].contents

                for element in elements:
                    try:
                        if 'Kapitalizacja' in element.contents[1].text:
                            curr_capitalization = str(element.contents[2].text.split("z")[0]).replace(" ", "").replace(
                                ",", ".")
                        if 'Free float' in element.contents[1].text:
                            curr_free_float = str(element.contents[2].text[:-1]).replace(",", ".")
                        if 'Liczba akcji' in element.contents[1].text:
                            curr_stock_amount = str(element.contents[2].text.split("szt")[0]).replace(" ", "").replace(
                                ",", ".")
                        if 'Cena / Zysk' in element.contents[1].text:
                            curr_price_to_profit = element.contents[2].text.replace(",", ".")
                        if 'Cena / WK' in element.contents[1].text:
                            curr_price_to_book = element.contents[2].text.replace(",", ".")
                    except Exception:
                        pass

                wse_comp_info = BankierCompanyStockInfo(banker_id=banker_shortcut,
                                                        banker_shortcut=banker_shortcut,
                                                        stock_price=curr_stock_price,
                                                        capitalization=curr_capitalization,
                                                        free_float=curr_free_float,
                                                        stock_amount=curr_stock_amount,
                                                        price_to_profit=curr_price_to_profit,
                                                        price_to_book_value=curr_price_to_book,
                                                        created_date=execution_date)

                cmp_stock_infos.append(wse_comp_info)
                print("Correctly processed: " + str(banker_shortcut))
            except Exception:
                print("Failed for: " + str(banker_shortcut))
        return cmp_stock_infos

    def get_biznesradar_wse_company_data(self, shortcuts: list):
        execution_date = datetime.today().strftime('%Y-%m-%d')
        cmp_stock_infos = []
        for shortcut in shortcuts:
            try:
                actual_url = self._BIZNESRADAR_URL_BASE + shortcut
                print("ACTUAL_URL: " + str(actual_url))
                soup = self.request_execution_engine.get_soup(url=actual_url,
                                                              use_local_machine=True,
                                                              use_proxy=False,
                                                              content_type="html.parser",
                                                              wait_upfront=1)

                core_section = soup.find("div", {"class": "box-left"}).find("table",
                                                                            {"class": "profileSummary"}).contents
                capitalization = ""
                number_of_stocks = ""
                for i in range(len(core_section)):
                    section = core_section[i]
                    if isinstance(section, Tag):
                        if str(section.get_text()).__contains__("Liczba akcji:"):
                            number_of_stocks = section.get_text().split("Liczba akcji:")[1].strip().replace(" ", "")
                        if str(section.get_text()).__contains__("Kapitalizacja:"):
                            capitalization = section.get_text().split("Kapitalizacja:")[1].strip().replace(" ", "")

                summary_section = soup.find("table", {"id": "profileSummaryCurrent"})

                actual_stock_price = summary_section.find("span", {"class": "q_ch_act"}).get_text()
                open_stock_price = summary_section.find("span", {"class": "q_ch_open"}).get_text()
                min_stock_price = summary_section.find("span", {"class": "q_ch_min"}).get_text()
                max_stock_price = summary_section.find("span", {"class": "q_ch_max"}).get_text()
                volume = summary_section.find("span", {"class": "q_ch_vol"}).get_text()
                total_trading = summary_section.find("span", {"class": "q_ch_mc"}).get_text()
                number_of_transactions = summary_section.find("span", {"class": "q_ch_trnr"}).get_text()

                financial_statements_section = soup.find("div", {"class": "element reports"}) \
                    .find("table", {"class": "contentList"}).contents

                sales_revenue = ""
                operational_profit = ""
                profit_before_tax = ""
                assets = ""
                equity_capital = ""

                for i in range(0, len(financial_statements_section)):
                    section = financial_statements_section[i]
                    if isinstance(section, Tag):
                        if str(section.get_text()).__contains__("Przychody ze sprzedaży"):
                            sales_revenue = section.find("span", {"class": "pv"}).string
                        if str(section.get_text()).__contains__("Zysk operacyjny"):
                            operational_profit = section.find("span", {"class": "pv"}).string
                        if str(section.get_text()).__contains__("Zysk przed opodatkowaniem"):
                            profit_before_tax = section.find("span", {"class": "pv"}).string
                        if str(section.get_text()).__contains__("Zysk netto"):
                            net_profit = section.find("span", {"class": "pv"}).string
                        if str(section.get_text()).__contains__("Aktywa razem"):
                            assets = section.find("span", {"class": "pv"}).string
                        if str(section.get_text()).__contains__("Aktywa razem"):
                            equity_capital = section.find("span", {"class": "pv"}).string

                elements_ratios_section = soup.find("div", {"class": "element ratios"}) \
                    .find("table", {"class": "contentList"}).contents

                cwk = ""
                cp = ""
                roe = ""
                roa = ""

                for i in range(0, len(elements_ratios_section)):
                    ratio = elements_ratios_section[i]
                    if isinstance(ratio, Tag):
                        if str(ratio.get_text()).__contains__("C/WK"):
                            cwk = ratio.find("span", {"class": "pv"}).string
                        if str(ratio.get_text()).__contains__("C/P"):
                            cp = ratio.find("span", {"class": "pv"}).string
                        if str(ratio.get_text()).__contains__("ROE"):
                            roe = ratio.find("span", {"class": "pv"}).string
                        if str(ratio.get_text()).__contains__("ROA"):
                            roa = ratio.find("span", {"class": "pv"}).string

                altman = ""
                piotroski = ""

                # handle this part of process separately - if this one fails process can still be OK
                try:
                    elements_ratings_section = soup.find("div", {"class": "element ratings"}) \
                        .find("table", {"class": "contentList"}).contents

                    for i in range(0, len(elements_ratings_section)):
                        rating = elements_ratings_section[i]
                        if isinstance(rating, Tag):
                            if str(rating.get_text()).__contains__("Altman"):
                                altman = rating.find("span", {"class": "pv"}).string
                            if str(rating.get_text()).__contains__("Piotroski"):
                                piotroski = rating.find("span", {"class": "pv"}).string
                except:
                    print("Gathering raitings for " + str(shortcut) + " failed. Skipping.")

                cmp_info = BiznesRadarCompanyStockInfo(
                    shortcut=shortcut,
                    capitalization=capitalization,
                    number_of_stocks=number_of_stocks,
                    actual_stock_price=actual_stock_price,
                    open_stock_price=open_stock_price,
                    min_stock_price=min_stock_price,
                    max_stock_price=max_stock_price,
                    volume=volume,
                    total_trading=total_trading,
                    number_of_transactions=number_of_transactions,
                    sales_revenue=sales_revenue,
                    operational_profit=operational_profit,
                    profit_before_tax=profit_before_tax,
                    assets=assets,
                    equity_capital=equity_capital,
                    cwk=cwk,
                    cp=cp,
                    roe=roe,
                    roa=roa,
                    altman=altman,
                    piotroski=piotroski,
                    execution_date=execution_date)
                cmp_stock_infos.append(cmp_info)
                print("Finished biznesradar parsing for: " + str(shortcut))
            except Exception as e:
                print("Failed biznesradar parsing for: " + str(shortcut))
        return cmp_stock_infos

    @staticmethod
    def get_biznesradar_financial_analysis_company_urls(
            company_symbol: str) -> list:
        """
        Gets all biznesradar links appended with company symbol

        Parameters
        ----------
        company_symbol: str
            biznesradar's symbol of scraped company

        Returns
        -------
        list
            list of biznesradar's urls to be scraped

        """

        fr_profits_losses_url_pattern = \
            "https://www.biznesradar.pl/raporty-finansowe-rachunek-zyskow-i-strat/{},Q"
        fr_bilans_url_pattern = \
            "https://www.biznesradar.pl/raporty-finansowe-bilans/{},Q"
        fr_money_flow_url_pattern = \
            "https://www.biznesradar.pl/raporty-finansowe-przeplywy-pieniezne/{},Q"
        indic_market_value_url_pattern = \
            "https://www.biznesradar.pl/wskazniki-wartosci-rynkowej/{}"
        indic_profitability_url_pattern = \
            "https://www.biznesradar.pl/wskazniki-rentownosci/{}"
        indic_money_flow_url_pattern = \
            "https://www.biznesradar.pl/wskazniki-przeplywow-pienieznych/{}"
        indic_debt_url_pattern = \
            "https://www.biznesradar.pl/wskazniki-zadluzenia/{}"
        indic_liquidity_url_pattern = \
            "https://www.biznesradar.pl/wskazniki-plynnosci/{}"
        indic_activity_url_pattern = \
            "https://www.biznesradar.pl/wskazniki-aktywnosci/{}"

        all_urls_patterns = [
            fr_profits_losses_url_pattern,
            fr_bilans_url_pattern,
            fr_money_flow_url_pattern,
            indic_market_value_url_pattern,
            indic_profitability_url_pattern,
            indic_money_flow_url_pattern,
            indic_debt_url_pattern,
            indic_liquidity_url_pattern,
            indic_activity_url_pattern]

        all_urls_descriptions = [
            'profits_losses',
            'bilans',
            'money_flow_reports',
            'market_value',
            'profitability',
            'money_flow_indicators',
            'debt',
            'liquidity',
            'activity'
        ]

        return [url_pattern.format(company_symbol)
                for url_pattern in all_urls_patterns], all_urls_descriptions

    def get_biznesradar_reports_and_indicators(
            self, urls: list, urls_descriptions: list, use_proxy: bool,
            wait_upfront: int = 1):
        """
        gets dictionary with data frames per url

        Parameters
        ----------
        urls: list
            list of urls to biznesradar reports and indicators
        urls_descriptions: list
            list of descriptions of biznesradar urls
        use_proxy: bool
            flag - should proxy be used for this operation
        wait_upfront: int
            how long to wait before requesting

        Returns
        -------
        dict
            dict of lists of dfs

        """
        all_fin_analysis_dfs = {}
        for url, url_desc in zip(urls, urls_descriptions):
            url_page = \
                self.request_execution_engine.get_response(
                    url=url, use_proxy=use_proxy, wait_upfront=wait_upfront)
            if url_page.status_code != 200:
                print(
                    'Got error code: {} while attempting to scrape: {}'
                    .format(url_page.status_code, url))
            # last 2 data frames are always identical and describe company
            # address etc.
            all_fin_analysis_dfs[url_desc] = \
                pd.read_html(url_page.content)[:-2]
        return all_fin_analysis_dfs

    def find_string_in_split_table_row(
            self, split_table_row: list, desired_string: str) -> list or None:

        if not split_table_row or \
                (not isinstance(split_table_row, list)
                 and not isinstance(split_table_row, tuple)):
            return split_table_row

        for row_part in split_table_row:
            if (not isinstance(row_part, list) and
                    not isinstance(row_part, tuple)):
                continue
            has_row_part_desired_string = \
                any([desired_string in el for el in row_part])
            if has_row_part_desired_string:
                return row_part
        return None

    def _extract_data_from_cell(self, row_text: str):
        split_regex = 'r/r|k/k'
        split_idxs = \
            [0] + [el.start() for el in re.finditer(split_regex, row_text)]

        parts = \
            [row_text[i:j].split('~')
             for i, j in zip(split_idxs, split_idxs[1:] + [None])]

        # print(row_text)
        # print(parts)

        parts_first_el = parts[0]
        if isinstance(parts_first_el, list):
            parts_first_el = parts_first_el[0]

        raw_absolute_value = parts_first_el.replace(' ', '')

        if not raw_absolute_value.strip():
            absolute_value = None
        else:
            absolute_value = \
                float(raw_absolute_value) if '%' not in raw_absolute_value \
                else float(raw_absolute_value.replace('%', '')) / 100
        # print(absolute_value)

        year_to_year_search_string = 'r/r'
        year_to_year_row_part = \
            self.find_string_in_split_table_row(
                split_table_row=parts, desired_string=year_to_year_search_string)

        year_to_year_company_value = None
        year_to_year_sector_value = None

        if year_to_year_row_part:
            year_to_year_company_value = \
                float(
                    year_to_year_row_part[0]
                    .replace(year_to_year_search_string, '')
                    .replace('%', '').strip()) / 100
            if len(year_to_year_row_part) > 1:
                year_to_year_sector_value = \
                    float(
                        year_to_year_row_part[1].replace('sektor', '')
                        .replace('%', '').strip()) / 100

        quarter_to_quarter_search_string = 'k/k'
        quarter_to_quarter_row_part = \
            self.find_string_in_split_table_row(
                split_table_row=parts,
                desired_string=quarter_to_quarter_search_string)

        quarter_to_quarter_company_value = None
        quarter_to_quarter_sector_value = None

        if quarter_to_quarter_row_part:
            quarter_to_quarter_company_value = \
                float(
                    quarter_to_quarter_row_part[0]
                    .replace(quarter_to_quarter_search_string, '')
                    .replace('%', '').strip()) / 100
            if len(quarter_to_quarter_row_part) > 1:
                quarter_to_quarter_sector_value = \
                    float(
                        quarter_to_quarter_row_part[1].replace('sektor', '')
                        .replace('%', '').strip()) / 100

        row = [
            absolute_value, year_to_year_company_value,
            year_to_year_sector_value, quarter_to_quarter_company_value,
            quarter_to_quarter_sector_value]

        return row

    def process_biznesradar_profits_losses_reports(
            self, all_fin_analysis_dfs: dict):

        # all_urls_descriptions = [
        #     'profits_losses',
        #     'bilans',
        #     'money_flow_reports',
        #     'market_value',
        #     'profitability',
        #     'money_flow_indicators',
        #     'debt',
        #     'liquidity',
        #     'activity'
        # ]

        profits_losses_dfs = all_fin_analysis_dfs.get('profits_losses', None)
        if not profits_losses_dfs or len(profits_losses_dfs) < 2:
            return pd.DataFrame()

        # print('len(profits_losses_dfs)')
        # print(len(profits_losses_dfs))

        raw_profits_losses_df = profits_losses_dfs[-1]
        orig_columns = raw_profits_losses_df.columns

        profits_losses_df = raw_profits_losses_df[orig_columns[:-1]]
        profits_losses_df.columns = \
            ['metric'] + \
            [self._get_datetime_from_qstr(cn) for cn in orig_columns[1:-1]]

        # time indexed df
        trans_df = profits_losses_df.transpose().reset_index()
        trans_df.columns = \
            ['quarter_date', 'publish_date'] + trans_df.iloc[0][2:].to_list()
        to_be_stacked_df = trans_df.iloc[1:].reset_index(drop=True)
        stacked_df = \
            to_be_stacked_df.set_index(['quarter_date', 'publish_date'])\
            .stack().reset_index()
        stacked_df.columns = ['quarter_date', 'publish_date', 'metric', 'value']

        # print('stacked_df.to_string()')
        # print(stacked_df.to_string())

        processed_values = \
            pd.DataFrame(stacked_df['value'].apply(
                lambda x: self._extract_data_from_cell(row_text=str(x))).to_list())

        processed_values.columns = \
            ['absolute_value', 'year_to_year_company_value',
             'year_to_year_sector_value', 'quarter_to_quarter_company_value',
             'quarter_to_quarter_sector_value']

        result = \
            pd.concat(
                [stacked_df.drop(columns=['value']), processed_values], axis=1)

        return result

    def _get_datetime_from_qstr(self, colname):

        quarter_to_string_map = {
            'Q1': '03-01',
            'Q2': '06-01',
            'Q3': '09-01',
            'Q4': '12-01'}

        if '/' in colname:
            date_chunk = colname.split(' ')[0].strip()
            splt_colname = date_chunk.split('/')
            month_date_chunk = quarter_to_string_map.get(splt_colname[1], None)

            if not month_date_chunk:
                return colname

            fixed_colname = \
                '{}-{}'.format(splt_colname[0], month_date_chunk)

            return fixed_colname

        else:
            return colname

    def process_biznesradar_bilans_reports(
            self, all_fin_analysis_dfs: dict):

        # all_urls_descriptions = [
        #     'profits_losses',
        #     'bilans',
        #     'money_flow_reports',
        #     'market_value',
        #     'profitability',
        #     'money_flow_indicators',
        #     'debt',
        #     'liquidity',
        #     'activity'
        # ]

        bilans_dfs = all_fin_analysis_dfs.get('bilans', None)
        if not bilans_dfs or len(bilans_dfs) < 2:
            return pd.DataFrame()

        raw_bilans_df = bilans_dfs[-1]
        orig_columns = raw_bilans_df.columns

        bilans_df = raw_bilans_df[orig_columns[:-1]]
        bilans_df.columns = \
            ['metric'] + \
            [self._get_datetime_from_qstr(cn) for cn in orig_columns[1:-1]]

        # time indexed df
        trans_df = bilans_df.transpose().reset_index()
        trans_df.columns = \
            ['quarter_date', 'publish_date'] + trans_df.iloc[0][2:].to_list()
        to_be_stacked_df = trans_df.iloc[1:].reset_index(drop=True)
        stacked_df = \
            to_be_stacked_df.set_index(['quarter_date', 'publish_date'])\
            .stack().reset_index()
        stacked_df.columns = ['quarter_date', 'publish_date', 'metric', 'value']

        processed_values = \
            pd.DataFrame(stacked_df['value'].apply(
                lambda x: self._extract_data_from_cell(row_text=str(x))).to_list())

        processed_values.columns = \
            ['absolute_value', 'year_to_year_company_value',
             'year_to_year_sector_value', 'quarter_to_quarter_company_value',
             'quarter_to_quarter_sector_value']

        result = \
            pd.concat(
                [stacked_df.drop(columns=['value']), processed_values], axis=1)

        return result

    def process_biznesradar_money_flow_reports(
            self, all_fin_analysis_dfs: dict):

        # all_urls_descriptions = [
        #     'profits_losses',
        #     'bilans',
        #     'money_flow_reports',
        #     'market_value',
        #     'profitability',
        #     'money_flow_indicators',
        #     'debt',
        #     'liquidity',
        #     'activity'
        # ]

        money_flow_reports_dfs = all_fin_analysis_dfs.get('money_flow_reports', None)

        # print('len(money_flow_reports_dfs)')
        # print(len(money_flow_reports_dfs))

        # if not money_flow_reports_dfs or len(money_flow_reports_dfs) < 2:
        #     return pd.DataFrame()

        raw_money_flows_df = money_flow_reports_dfs[-1]
        orig_columns = raw_money_flows_df.columns

        money_flows_df = raw_money_flows_df[orig_columns[:-1]]
        money_flows_df.columns = \
            ['metric'] + \
            [self._get_datetime_from_qstr(cn) for cn in orig_columns[1:-1]]

        # time indexed df
        trans_df = money_flows_df.transpose().reset_index()
        trans_df.columns = \
            ['quarter_date', 'publish_date'] + trans_df.iloc[0][2:].to_list()
        to_be_stacked_df = trans_df.iloc[1:].reset_index(drop=True)
        stacked_df = \
            to_be_stacked_df.set_index(['quarter_date', 'publish_date'])\
            .stack().reset_index()
        stacked_df.columns = ['quarter_date', 'publish_date', 'metric', 'value']

        processed_values = \
            pd.DataFrame(stacked_df['value'].apply(
                lambda x: self._extract_data_from_cell(row_text=str(x))).to_list())

        processed_values.columns = \
            ['absolute_value', 'year_to_year_company_value',
             'year_to_year_sector_value', 'quarter_to_quarter_company_value',
             'quarter_to_quarter_sector_value']

        result = \
            pd.concat(
                [stacked_df.drop(columns=['value']), processed_values], axis=1)

        return result

    def process_biznesradar_market_value_reports(
            self, all_fin_analysis_dfs: dict):

        # all_urls_descriptions = [
        #     'profits_losses',
        #     'bilans',
        #     'money_flow_reports',
        #     'market_value',
        #     'profitability',
        #     'money_flow_indicators',
        #     'debt',
        #     'liquidity',
        #     'activity'
        # ]

        market_value_dfs = all_fin_analysis_dfs.get('market_value', None)

        # print('len(money_flow_reports_dfs)')
        # print(len(money_flow_reports_dfs))

        # if not money_flow_reports_dfs or len(money_flow_reports_dfs) < 2:
        #     return pd.DataFrame()

        raw_market_value_df = market_value_dfs[-1]
        orig_columns = raw_market_value_df.columns

        market_value_df = raw_market_value_df[orig_columns[:-1]]

        # print('market_value_df')
        # print(market_value_df.to_string())
        # input('sanity check')

        market_value_df.columns = \
            ['metric'] + \
            [self._get_datetime_from_qstr(cn) for cn in orig_columns[1:-1]]

        # time indexed df
        trans_df = market_value_df.transpose().reset_index()

        trans_df.columns = \
            ['quarter_date'] + trans_df.iloc[0][1:].to_list()

        trans_df['publish_date'] = \
            trans_df['quarter_date'].apply(
                lambda x:
                str((pd.to_datetime(x) + pd.DateOffset(months=6)).date())
                if x != 'metric' else x)

        to_be_stacked_df = trans_df.iloc[1:].reset_index(drop=True)
        stacked_df = \
            to_be_stacked_df.set_index(['quarter_date', 'publish_date'])\
            .stack().reset_index()
        stacked_df.columns = ['quarter_date', 'publish_date', 'metric', 'value']

        processed_values = \
            pd.DataFrame(stacked_df['value'].apply(
                lambda x: self._extract_data_from_cell(row_text=str(x))).to_list())

        processed_values.columns = \
            ['absolute_value', 'year_to_year_company_value',
             'year_to_year_sector_value', 'quarter_to_quarter_company_value',
             'quarter_to_quarter_sector_value']

        result = \
            pd.concat(
                [stacked_df.drop(columns=['value']), processed_values], axis=1)

        return result

    def process_biznesradar_profitability_reports(
            self, all_fin_analysis_dfs: dict):

        # all_urls_descriptions = [
        #     'profits_losses',
        #     'bilans',
        #     'money_flow_reports',
        #     'market_value',
        #     'profitability',
        #     'money_flow_indicators',
        #     'debt',
        #     'liquidity',
        #     'activity'
        # ]

        profitability_dfs = all_fin_analysis_dfs.get('profitability', None)

        # print('len(money_flow_reports_dfs)')
        # print(len(money_flow_reports_dfs))

        if not profitability_dfs or len(profitability_dfs) < 2:
            return pd.DataFrame()

        raw_profitability_df = profitability_dfs[-1]
        orig_columns = raw_profitability_df.columns

        profitability_df = raw_profitability_df[orig_columns[:-1]]

        # print('market_value_df')
        # print(market_value_df.to_string())
        # input('sanity check')

        profitability_df.columns = \
            ['metric'] + \
            [self._get_datetime_from_qstr(cn) for cn in orig_columns[1:-1]]

        # time indexed df
        trans_df = profitability_df.transpose().reset_index()

        trans_df.columns = \
            ['quarter_date'] + trans_df.iloc[0][1:].to_list()

        trans_df['publish_date'] = \
            trans_df['quarter_date'].apply(
                lambda x:
                str((pd.to_datetime(x) + pd.DateOffset(months=6)).date())
                if x != 'metric' else x)

        to_be_stacked_df = trans_df.iloc[1:].reset_index(drop=True)
        stacked_df = \
            to_be_stacked_df.set_index(['quarter_date', 'publish_date'])\
            .stack().reset_index()
        stacked_df.columns = ['quarter_date', 'publish_date', 'metric', 'value']

        processed_values = \
            pd.DataFrame(stacked_df['value'].apply(
                lambda x: self._extract_data_from_cell(row_text=str(x))).to_list())

        processed_values.columns = \
            ['absolute_value', 'year_to_year_company_value',
             'year_to_year_sector_value', 'quarter_to_quarter_company_value',
             'quarter_to_quarter_sector_value']

        result = \
            pd.concat(
                [stacked_df.drop(columns=['value']), processed_values], axis=1)

        return result

    def process_biznesradar_money_flow_indicators_reports(
            self, all_fin_analysis_dfs: dict):

        # all_urls_descriptions = [
        #     'profits_losses',
        #     'bilans',
        #     'money_flow_reports',
        #     'market_value',
        #     'profitability',
        #     'money_flow_indicators',
        #     'debt',
        #     'liquidity',
        #     'activity'
        # ]

        money_flow_indicators_dfs = all_fin_analysis_dfs.get('money_flow_indicators', None)

        # print('len(money_flow_reports_dfs)')
        # print(len(money_flow_reports_dfs))

        # if not money_flow_indicators_dfs or len(money_flow_indicators_dfs) < 2:
        #     return pd.DataFrame()

        raw_money_flow_indicators_df = money_flow_indicators_dfs[-1]
        orig_columns = raw_money_flow_indicators_df.columns

        money_flow_indicators_df = raw_money_flow_indicators_df[orig_columns[:-1]]

        # print('market_value_df')
        # print(market_value_df.to_string())
        # input('sanity check')

        money_flow_indicators_df.columns = \
            ['metric'] + \
            [self._get_datetime_from_qstr(cn) for cn in orig_columns[1:-1]]

        # time indexed df
        trans_df = money_flow_indicators_df.transpose().reset_index()

        trans_df.columns = \
            ['quarter_date'] + trans_df.iloc[0][1:].to_list()

        trans_df['publish_date'] = \
            trans_df['quarter_date'].apply(
                lambda x:
                str((pd.to_datetime(x) + pd.DateOffset(months=6)).date())
                if x != 'metric' else x)

        to_be_stacked_df = trans_df.iloc[1:].reset_index(drop=True)
        stacked_df = \
            to_be_stacked_df.set_index(['quarter_date', 'publish_date'])\
            .stack().reset_index()
        stacked_df.columns = ['quarter_date', 'publish_date', 'metric', 'value']

        processed_values = \
            pd.DataFrame(stacked_df['value'].apply(
                lambda x: self._extract_data_from_cell(row_text=str(x))).to_list())

        processed_values.columns = \
            ['absolute_value', 'year_to_year_company_value',
             'year_to_year_sector_value', 'quarter_to_quarter_company_value',
             'quarter_to_quarter_sector_value']

        result = \
            pd.concat(
                [stacked_df.drop(columns=['value']), processed_values], axis=1)

        return result

    def process_biznesradar_debt_reports(self, all_fin_analysis_dfs: dict):

        # all_urls_descriptions = [
        #     'profits_losses',
        #     'bilans',
        #     'money_flow_reports',
        #     'market_value',
        #     'profitability',
        #     'money_flow_indicators',
        #     'debt',
        #     'liquidity',
        #     'activity'
        # ]

        debt_dfs = all_fin_analysis_dfs.get('debt', None)

        # print('len(money_flow_reports_dfs)')
        # print(len(money_flow_reports_dfs))

        if not debt_dfs or len(debt_dfs) < 2:
            return pd.DataFrame()

        raw_debt_df = debt_dfs[-1]
        orig_columns = raw_debt_df.columns

        debt_df = raw_debt_df[orig_columns[:-1]]

        # print('market_value_df')
        # print(market_value_df.to_string())
        # input('sanity check')

        debt_df.columns = \
            ['metric'] + \
            [self._get_datetime_from_qstr(cn) for cn in orig_columns[1:-1]]

        # time indexed df
        trans_df = debt_df.transpose().reset_index()

        trans_df.columns = \
            ['quarter_date'] + trans_df.iloc[0][1:].to_list()

        trans_df['publish_date'] = \
            trans_df['quarter_date'].apply(
                lambda x:
                str((pd.to_datetime(x) + pd.DateOffset(months=6)).date())
                if x != 'metric' else x)

        to_be_stacked_df = trans_df.iloc[1:].reset_index(drop=True)
        stacked_df = \
            to_be_stacked_df.set_index(['quarter_date', 'publish_date'])\
            .stack().reset_index()
        stacked_df.columns = ['quarter_date', 'publish_date', 'metric', 'value']

        processed_values = \
            pd.DataFrame(stacked_df['value'].apply(
                lambda x: self._extract_data_from_cell(row_text=str(x))).to_list())

        processed_values.columns = \
            ['absolute_value', 'year_to_year_company_value',
             'year_to_year_sector_value', 'quarter_to_quarter_company_value',
             'quarter_to_quarter_sector_value']

        result = \
            pd.concat(
                [stacked_df.drop(columns=['value']), processed_values], axis=1)

        return result

    def process_biznesradar_liquidity_reports(self, all_fin_analysis_dfs: dict):
        # all_urls_descriptions = [
        #     'profits_losses',
        #     'bilans',
        #     'money_flow_reports',
        #     'market_value',
        #     'profitability',
        #     'money_flow_indicators',
        #     'debt',
        #     'liquidity',
        #     'activity'
        # ]

        liquidity_dfs = all_fin_analysis_dfs.get('liquidity', None)

        # print('len(money_flow_reports_dfs)')
        # print(len(money_flow_reports_dfs))

        if not liquidity_dfs or len(liquidity_dfs) < 2:
            return pd.DataFrame()

        raw_liquidity_df = liquidity_dfs[-1]
        orig_columns = raw_liquidity_df.columns

        liquidity_df = raw_liquidity_df[orig_columns[:-1]]

        # print('market_value_df')
        # print(market_value_df.to_string())
        # input('sanity check')

        liquidity_df.columns = \
            ['metric'] + \
            [self._get_datetime_from_qstr(cn) for cn in orig_columns[1:-1]]

        # time indexed df
        trans_df = liquidity_df.transpose().reset_index()

        trans_df.columns = \
            ['quarter_date'] + trans_df.iloc[0][1:].to_list()

        trans_df['publish_date'] = \
            trans_df['quarter_date'].apply(
                lambda x:
                str((pd.to_datetime(x) + pd.DateOffset(months=6)).date())
                if x != 'metric' else x)

        to_be_stacked_df = trans_df.iloc[1:].reset_index(drop=True)
        stacked_df = \
            to_be_stacked_df.set_index(['quarter_date', 'publish_date']) \
                .stack().reset_index()
        stacked_df.columns = ['quarter_date', 'publish_date', 'metric', 'value']

        processed_values = \
            pd.DataFrame(stacked_df['value'].apply(
                lambda x: self._extract_data_from_cell(row_text=str(x))).to_list())

        processed_values.columns = \
            ['absolute_value', 'year_to_year_company_value',
             'year_to_year_sector_value', 'quarter_to_quarter_company_value',
             'quarter_to_quarter_sector_value']

        result = \
            pd.concat(
                [stacked_df.drop(columns=['value']), processed_values], axis=1)

        return result

    def process_biznesradar_activity_reports(self, all_fin_analysis_dfs: dict):
        # all_urls_descriptions = [
        #     'profits_losses',
        #     'bilans',
        #     'money_flow_reports',
        #     'market_value',
        #     'profitability',
        #     'money_flow_indicators',
        #     'debt',
        #     'liquidity',
        #     'activity'
        # ]

        activity_dfs = all_fin_analysis_dfs.get('activity', None)

        # print('len(money_flow_reports_dfs)')
        # print(len(money_flow_reports_dfs))

        if not activity_dfs or len(activity_dfs) < 3:
            return pd.DataFrame()

        raw_activity_df = activity_dfs[-1]
        orig_columns = raw_activity_df.columns

        activity_df = raw_activity_df[orig_columns[:-1]]

        # print('market_value_df')
        # print(activity_df.to_string())
        # input('sanity check')

        activity_df.columns = \
            ['metric'] + \
            [self._get_datetime_from_qstr(cn) for cn in orig_columns[1:-1]]

        # time indexed df
        trans_df = activity_df.transpose().reset_index()

        trans_df.columns = \
            ['quarter_date'] + trans_df.iloc[0][1:].to_list()

        trans_df['publish_date'] = \
            trans_df['quarter_date'].apply(
                lambda x:
                str((pd.to_datetime(x) + pd.DateOffset(months=6)).date())
                if x != 'metric' else x)

        to_be_stacked_df = trans_df.iloc[1:].reset_index(drop=True)
        stacked_df = \
            to_be_stacked_df.set_index(['quarter_date', 'publish_date']) \
            .stack().reset_index()
        stacked_df.columns = ['quarter_date', 'publish_date', 'metric', 'value']

        processed_values = \
            pd.DataFrame(stacked_df['value'].apply(
                lambda x: self._extract_data_from_cell(row_text=str(x))).to_list())

        processed_values.columns = \
            ['absolute_value', 'year_to_year_company_value',
             'year_to_year_sector_value', 'quarter_to_quarter_company_value',
             'quarter_to_quarter_sector_value']

        result = \
            pd.concat(
                [stacked_df.drop(columns=['value']), processed_values], axis=1)

        return result

    def get_biznesradar_wse_company_financial_report(
            self, shortcuts: list, use_proxy: bool = False,
            wait_upfront: int = 1):

        # all_urls_descriptions = [
        #     'profits_losses',
        #     'bilans',
        #     'money_flow_reports',
        #     'market_value',
        #     'profitability',
        #     'money_flow_indicators',
        #     'debt',
        #     'liquidity',
        #     'activity'
        # ]

        per_company_df = []
        for company_symbol in shortcuts:
            print('Processing reports for: {}'.format(company_symbol))
            urls, urls_descriptions = \
                StockDataProcessingEngine.get_biznesradar_financial_analysis_company_urls(
                    company_symbol=company_symbol)
            all_fin_analysis_dfs = self.get_biznesradar_reports_and_indicators(
                urls=urls, urls_descriptions=urls_descriptions,
                use_proxy=use_proxy, wait_upfront=wait_upfront)

            all_company_dfs = []

            for br_df_processing_method in [
                self.process_biznesradar_profits_losses_reports,
                self.process_biznesradar_bilans_reports,
                self.process_biznesradar_money_flow_reports,
                self.process_biznesradar_market_value_reports,
                self.process_biznesradar_profitability_reports,
                self.process_biznesradar_money_flow_indicators_reports,
                self.process_biznesradar_debt_reports,
                self.process_biznesradar_liquidity_reports,
                self.process_biznesradar_activity_reports]:

                all_company_dfs.append(br_df_processing_method(
                    all_fin_analysis_dfs))

            full_company_df = pd.concat(all_company_dfs)
            full_company_df['company_symbol'] = company_symbol

            per_company_df.append(full_company_df)

        full_financial_reports_df = \
            pd.concat(per_company_df).reset_index(drop=True)

        return full_financial_reports_df


if __name__ == '__main__':
    # swre = SingleWebRequestExecutor()
    # sdpe = StockDataProcessingEngine(request_execution_engine=swre)
    # banker_shortcuts = ['ULTGAMES', 'THEDUST', 'VARSAV']
    # companies = sdpe.get_banker_company_data(banker_shortcuts=banker_shortcuts)
    # for company in companies:
    #     print(company)

    swre = SingleWebRequestExecutor()
    sdpe = StockDataProcessingEngine(request_execution_engine=swre)
    shortcut = ['THD', 'MOV']
    # companies = sdpe.get_biznesradar_wse_company_data(shortcuts=shortcut)
    # for company in companies:
    #     print(company)

    company_symbol = 'KERNEL'

    urls, descs = \
        sdpe.get_biznesradar_financial_analysis_company_urls(
            company_symbol=company_symbol)

    q = sdpe.get_biznesradar_wse_company_financial_report(shortcuts=['CRB'])

    print(q.head().to_string())

    # all_dfs = \
    #     sdpe.get_biznesradar_reports_and_indicators(
    #         urls=urls, urls_descriptions=descs, use_proxy=False)
    #
    # print(all_dfs.keys())
    #
    # des_df = \
    #     sdpe.process_biznesradar_debt_reports(
    #         all_fin_analysis_dfs=all_dfs)
    #
    # print(des_df.to_string())
