import json
from datetime import datetime

import requests
from joblib import Parallel, delayed

from src.engine.executor.web_request_executors import WebRequestExecutor, SimpleWebRequestExecutor
from src.parsed_models.steam_app import SteamApplicationInfo
from src.parsed_models.steam_app_info_lkp import SteamAppInfoLkp
from src.parsed_models.steam_app_live_stats import SteamAppLiveStats
from src.parsed_models.whishlist_ranked_title import WhishlistRankedTitle


class SteamProcessingEngine:
    _url_per_type = \
        {"WHISHLIST": "https://store.steampowered.com/search/?sort_by=&sort_order=0&filter=popularwishlist&page=",
         "BESTSELLER": "https://store.steampowered.com/search/?l=polish&os=win&filter=topsellers&page="}

    _all_apps_steam_url = 'http://api.steampowered.com/ISteamApps/GetAppList/v0002/?key=STEAMKEY&format=json'
    _single_app_steam_api_url = 'https://store.steampowered.com/api/appdetails?appids='
    _single_app_steam_community_api_url = 'https://steamcommunity.com/games/'
    _steam_app_url = "https://store.steampowered.com/app/"

    @property
    def request_execution_engine(self) -> WebRequestExecutor:
        return self._request_execution_engine

    @request_execution_engine.setter
    def request_execution_engine(self, new_val):
        self._request_execution_engine = new_val

    def __init__(self, request_execution_engine: WebRequestExecutor = SimpleWebRequestExecutor()):
        self.request_execution_engine = request_execution_engine

    def run_steam_ranked_title(self,
                               exec_type: str,
                               n_pages: int = 27,
                               page_titles: int = 25,
                               init_page: int = 1):
        # url = "https://store.steampowered.com/search/?sort_by=&sort_order=0&filter=popularwishlist&page="
        url = self._url_per_type[exec_type]
        whishlist_titles = []
        whishlist_titles_appids = []
        execution_date = datetime.today().strftime('%Y-%m-%d')
        whistlist_position = 1

        number_of_whishlist_pages = n_pages + 1
        number_of_titles_per_page = page_titles
        start_page = init_page

        for i in range(start_page, number_of_whishlist_pages):
            try:
                print("Processing page: " + str(i) + " of " + exec_type +
                      " with number of titles per site: " + str(number_of_titles_per_page))
                actual_url = url + str(i)

                soup = self.request_execution_engine.get_soup(url=actual_url,
                                                              use_local_machine=True,
                                                              use_proxy=False,
                                                              content_type="html.parser")

                whishlist = soup.find("div", {"id": "search_result_container"}).findAll("a")
                per_site_limit = number_of_titles_per_page
                for one_position in whishlist:
                    curr_title_pos = whistlist_position
                    curr_title_href = str(one_position['href']).strip()
                    try:
                        curr_title_appid = str(one_position.attrs['data-ds-appid']).strip()
                        if curr_title_appid.__contains__(","):
                            curr_title_appid = curr_title_appid.split(",")[0]
                        curr_title_itemkey = str(one_position.attrs['data-ds-itemkey']).strip()
                    except Exception:
                        curr_title_appid = "UNK"
                        curr_title_itemkey = "UNK"
                    curr_title_title = str(one_position.span.get_text()).strip()
                    curr_title_release_date = str(one_position.find_all("div")[1].find_all("div")[1].get_text()).strip()
                    curr_title_price = str(one_position.find_all("div")[1].find_all("div")[5].get_text()).strip()[:-2]
                    curr_wishlist_ranked_title = WhishlistRankedTitle(appid=curr_title_appid,
                                                                      itemkey=curr_title_itemkey,
                                                                      position=str(curr_title_pos),
                                                                      href=curr_title_href,
                                                                      title=curr_title_title,
                                                                      release_date=curr_title_release_date,
                                                                      price=curr_title_price,
                                                                      created_date=execution_date)
                    whishlist_titles.append(curr_wishlist_ranked_title)
                    whishlist_titles_appids.append(curr_title_appid)
                    per_site_limit -= 1
                    whistlist_position += 1
                    if per_site_limit <= 0:
                        break
            except Exception:
                print("Cannot process " + str(i) + " element of steam whishlist/bestsellers")
        return whishlist_titles_appids, whishlist_titles

    def _clean_multiple_ids(self, app_id_list: list):
        ret_list = []
        for app_id in app_id_list:
            if str(app_id).__contains__(","):
                ret_list = ret_list + str(app_id).split(",")
            else:
                ret_list.append(app_id)
        return ret_list

    def run_steam_app(self,
                      app_id_list: list):
        # url = "https://store.steampowered.com/app/"
        app_id_list_clean = self._clean_multiple_ids(app_id_list)

        if len(app_id_list_clean) > 2500:
            raise ValueError("At this moment only 2500 ids are allowed.")
        execution_date = datetime.today().strftime('%Y-%m-%d')

        # steam_app_info_list = Parallel(n_jobs=4)(
        #     delayed(self._process_single_steam_app_position)(app_id, url, execution_date) for app_id in
        #     app_id_list_clean)
        steam_app_info_list = [self._process_single_steam_app_position(app_id, execution_date)
                               for app_id in app_id_list_clean]

        steam_app_info_list_res = [app_record for app_record in steam_app_info_list if app_record is not None]

        return steam_app_info_list_res

    def _process_single_steam_app_position(self, app_id: str, execution_date: str):
        try:
            if app_id == "UNK":
                raise ValueError("APPID cannot be UNK")

            app_url = self._steam_app_url
            comm_url = self._single_app_steam_community_api_url

            actual_url = app_url + str(app_id)
            actual_comm_url = comm_url + str(app_id)
            # response = requests.get(actual_url)
            # soup = BeautifulSoup(response.text, "html.parser")

            soup = self.request_execution_engine.get_soup(url=actual_url,
                                                          use_local_machine=True,
                                                          use_proxy=False,
                                                          content_type="html.parser",
                                                          wait_upfront=2)

            games_comm = self.request_execution_engine.get_soup(url=actual_comm_url,
                                                                use_local_machine=True,
                                                                use_proxy=False,
                                                                content_type="html.parser",
                                                                wait_upfront=2)

            curr_app_id = app_id

            curr_app_title = soup.find("div", {"class": "apphub_AppName"}).get_text()

            # curr_app_general_type = soup.find("div", {"class": "details_block"}).findAll("a")[0].text

            elements = soup.findAll("div", {"class": "details_block"})

            try:
                release_date = soup.find("div", {"class": "release_date"}).get_text().split("Release Date:")[1].strip()
            except Exception:
                release_date = "UNK"

            curr_app_general_type = "UNK"
            for el in elements:
                if str(el.text).strip().startswith("Title"):
                    curr_app_general_type = el.findAll("a")[0].text
                    break

            try:
                curr_app_game_description_snippet = str(soup
                                                        .find("div", {"id": "game_highlights"})
                                                        .find("div", {"class": "game_description_snippet"})
                                                        .get_text()).strip()
            except Exception:
                curr_app_game_description_snippet = "UKN"

            try:
                curr_app_game_developer_list = str(soup
                                                   .find("div", {"id": "game_highlights"})
                                                   .findAll("div", {"class": "dev_row"})[0]
                                                   .text) \
                    .strip() \
                    .replace("Developer:\n\n", "")
                # curr_app_game_developer_list = soup.find("div", {"class": "details_block"}).findAll("a")[1].text
            except Exception:
                curr_app_game_developer_list = "UKN"

            try:
                curr_app_games_publisher_list = str(
                    soup.find("div", {"id": "game_highlights"}).findAll("div", {"class": "dev_row"})[
                        1].text).strip().replace("Publisher:\n\n", "")
                # curr_app_games_publisher_list = soup.find("div", {"class": "details_block"}).findAll("a")[2].text
            except Exception:
                curr_app_games_publisher_list = "UKN"

            try:
                curr_app_steam_reviews_summary = str(
                    soup.find("div", {"id": "game_highlights"}).find("div", {"class": "user_reviews"}).contents[
                        1].text).replace("\t", "").replace("\n", "").replace("\r", "")
            except Exception:
                curr_app_steam_reviews_summary = "UKN"

            try:
                curr_app_review_count = str(
                    soup.find("div", {"id": "game_highlights"}).find("meta", {"itemprop": "reviewCount"}).attrs[
                        'content'])

                curr_app_rating_value = str(
                    soup.find("div", {"id": "game_highlights"}).find("meta", {"itemprop": "ratingValue"}).attrs[
                        'content'])

                curr_app_best_rating = str(
                    soup.find("div", {"id": "game_highlights"}).find("meta", {"itemprop": "bestRating"}).attrs[
                        'content'])

                curr_app_worst_rating = str(
                    soup.find("div", {"id": "game_highlights"}).find("meta", {"itemprop": "worstRating"}).attrs[
                        'content'])
            except Exception:
                curr_app_review_count = None
                curr_app_rating_value = None
                curr_app_best_rating = None
                curr_app_worst_rating = None

            try:
                steam_members = \
                    games_comm.find("div", {"id": "profileBlock"}).get_text().strip().split("|")[0].strip().split(" ")[
                        0].replace(".","").replace(",","")
                if not steam_members.isnumeric():
                    steam_members = None
            except Exception:
                steam_members = None

            try:
                steam_in_games = \
                    games_comm.find("div", {"id": "profileBlock"}).get_text().strip().split("|")[1].strip().split(" ")[
                        0].replace(".","").replace(",","")
                if not steam_in_games.isnumeric():
                    steam_in_games = None
            except Exception:
                steam_in_games = None

            try:
                steam_online_cnt = \
                    games_comm.find("div", {"id": "profileBlock"}).get_text().strip().split("|")[2].strip().split(" ")[
                        0].replace(".","").replace(",","")
                if not steam_online_cnt.isnumeric():
                    steam_online_cnt = None
            except Exception:
                steam_online_cnt = None

            try:
                steam_on_group_chat = \
                    games_comm.find("div", {"id": "profileBlock"}).get_text().strip().split("|")[3].strip().split(" ")[
                        0].replace(".","").replace(",","")
                if not steam_on_group_chat.isnumeric():
                    steam_on_group_chat = None
            except Exception:
                steam_on_group_chat = None

            curr_app_popular_tags = str(soup.find("div", {"id": "game_highlights"}).find("div", {
                "class": "glance_tags popular_tags"}).text.replace("\t", "").replace("\r", "").replace("\n", ",")[
                                        2:-1])

            app_info = SteamApplicationInfo(appid=curr_app_id,
                                            title=curr_app_title,
                                            app_general_type=curr_app_general_type,
                                            release_date=release_date,
                                            app_category="UNK",
                                            game_description_snippet=curr_app_game_description_snippet,
                                            producer=curr_app_game_developer_list,
                                            publisher=curr_app_games_publisher_list,
                                            steam_reviews_summary=curr_app_steam_reviews_summary,
                                            steam_reviews_description="UNK",
                                            review_count=curr_app_review_count,
                                            rating_value=curr_app_rating_value,
                                            best_rating=curr_app_best_rating,
                                            worst_rating=curr_app_worst_rating,
                                            popular_tags=curr_app_popular_tags,
                                            members_count=steam_members,
                                            in_game_players=steam_in_games,
                                            online_players_cnt=steam_online_cnt,
                                            on_groups_chat=steam_on_group_chat,
                                            created_date=execution_date)

            print("Finished: " + str(app_id))
            return app_info
        except Exception as exc:
            print(exc)
            print("Failed for id: " + str(app_id))
            return None

    def run_steam_active_players(self, ids_list: list):
        url = "https://steamcommunity.com/app/"
        execution_date = datetime.today().strftime('%Y-%m-%d %H:00:00')
        steam_app_info_list = Parallel(n_jobs=4)(
            delayed(self._process_live_stats_parallel)(app_id, url, execution_date) for app_id in ids_list)
        return steam_app_info_list

    def _process_live_stats_parallel(self, app_id: str, url: str, execution_date: str):
        try:
            actual_url = url + app_id
            # response = requests.get(actual_url)
            # soup = BeautifulSoup(response.text, "html.parser")

            soup = self.request_execution_engine.get_soup(url=actual_url,
                                                          use_local_machine=True,
                                                          use_proxy=False,
                                                          content_type="html.parser")

            curr_app_id = app_id
            curr_app_active_players = "UNK"
            try:
                curr_app_active_players = str(soup
                                              .find("div", {"class": "apphub_Stats"})
                                              .find("span", {"class", "apphub_NumInApp"})
                                              .get_text()).strip().split(" ")[0].replace(",", "").replace(".", "")
            except Exception:
                pass
            curr_app_active_in_groupchat = "UNK"
            try:
                curr_app_active_in_groupchat = str(soup.find("div", {"class": "apphub_Stats"})
                                                   .find("span", {"class", "apphub_Chat apphub_ResponsiveMenuItem"})
                                                   .get_text()).strip().split(" ")[0].replace(",", "").replace(".", "")
            except Exception:
                pass
            curr_app_created_date = execution_date
            app_stat = SteamAppLiveStats(curr_app_id, curr_app_active_players, curr_app_active_in_groupchat,
                                         curr_app_created_date)
            print("Finished for: " + str(app_id))
            return app_stat
        except Exception:
            print("Failed for: " + str(app_id))
            return SteamAppLiveStats(app_id, "UNK", "UNK", execution_date)

    def get_all_steam_apps_ids(self):
        all_apps_json = json.loads(requests.get(self._all_apps_steam_url).text)['applist']['apps']
        return [i['appid'] for i in all_apps_json]

    def get_all_steam_apps_lookup_info(self, steam_app_ids: list):
        execution_date = datetime.today().strftime('%Y-%m-%d')
        steam_app_info_list = []
        # I got banned for couple of minutes for pulling many ids, let's leave simple loop
        # instead of pulling multiple
        for appid in steam_app_ids:
            steam_app_info_list.append(self._gel_steam_app_lkp(appid, execution_date))
            # time.sleep(0.5)
        # steam_app_info_list = Parallel(n_jobs=4)(
        #     delayed(self._gel_steam_app_lkp)(app_id, execution_date) for app_id in steam_app_ids)
        return [app_info_lkp for app_info_lkp in steam_app_info_list if app_info_lkp is not None]

    def _gel_steam_app_lkp(self, appid: str, execution_date: str):
        try:
            actual_url = self._single_app_steam_api_url + str(appid)
            response = self.request_execution_engine.get_response(url=actual_url,
                                                                  use_local_machine=True,
                                                                  use_proxy=False,
                                                                  wait_upfront=2)
            json_app_data = json.loads(response.text)[str(appid)]['data']
            curr_app_title = json_app_data['name']
            curr_app_header_image = json_app_data['header_image']
            price = "UNK"
            try:
                price = json_app_data['price_overview']['final_formatted']
            except Exception:
                pass
            curr_app_publishers = "UNK"
            try:
                curr_app_publishers = ' | '.join(str(elem) for elem in json_app_data['publishers'])
            except Exception:
                pass
            curr_app_developers = "UNK"
            try:
                curr_app_developers = ' | '.join(str(elem) for elem in json_app_data['developers'])
            except Exception:
                pass
            sail = SteamAppInfoLkp(appid=str(appid),
                                   title=curr_app_title,
                                   header_image=curr_app_header_image,
                                   publishers=curr_app_publishers,
                                   developers=curr_app_developers,
                                   created_date=execution_date,
                                   price=price)
            print("Finished for: " + str(appid))
            return sail
        except Exception:
            print("Failed for id: " + str(appid))
            # traceback.print_exc()
            sail = SteamAppInfoLkp(appid=str(appid),
                                   title="INVALID",
                                   header_image="",
                                   publishers="",
                                   developers="",
                                   created_date=execution_date,
                                   price="")
            return sail


if __name__ == "__main__":
    spe = SteamProcessingEngine()
    res = spe.run_steam_app(["789510"])
    print(res[0])

    # spe.run_steam_app([265100])

    # ids = spe.get_all_steam_apps_ids()
    # tmp_ids = ids[0:10]
    # app_lkps = spe.get_all_steam_apps_lookup_info(tmp_ids)
    # for i in app_lkps:
    #     print(str(i))

    # ids, titles = spe.run_steam_whishlist(exec_type="WHISHLIST", n_pages=1, page_titles=25)
    # ids_2, best_sellers = spe.run_steam_whishlist(exec_type="BESTSELLER", n_pages=1, page_titles=25)
    # ids = ["359320"]
    # to_run_ids = list(set(ids + ids_2))
    # results = spe.run_steam_app(ids)
    # for res in results:
    #     print(res)

    # res = pdl.get_all_steam_app_ids()
    # spe.run_steam_active_players(ids_list=res)

    # for title in titles:
    #     print(title)
    # ids_2, titles_2 = spe.run_steam_whishlist(exec_type="BESTSELLER", n_pages=1, page_titles=10)
    # for title in titles_2:
    #     print(title)
