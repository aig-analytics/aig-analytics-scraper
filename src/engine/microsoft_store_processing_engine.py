import json
from datetime import datetime

import requests
from bs4 import BeautifulSoup
from joblib import Parallel, delayed

from src.parsed_models.microsoft_app_info import MicrosoftAppInfo
from src.parsed_models.microsoft_ranked_title import MicrosoftRankedTitle


class MicrosoftStoreProcessingEngine:
    _BASE_URL = 'https://www.microsoft.com'

    _ALLWD_MARKETS = {'PL': 'pl-pl'}
    _ALLWD_RANKINGS = ['top-paid', 'best-selling', 'best-rated', 'most-popular']
    _ALLWD_DEVICES = ['pc', 'xbox']

    # _ALLWD_RANKINGS = ['top-paid']
    # _ALLWD_DEVICES = ['pc']

    _GAMES_PER_SITE = 90
    # _PAGES_LIMIT = 10
    _PAGES_LIMIT = 10

    def get_country_top_paid_list(self, limit: int = 10):
        init_url = self._BASE_URL
        execution_date = datetime.today().strftime('%Y-%m-%d')
        all_microsoft_ranked_titles = []
        for country, localization in self._ALLWD_MARKETS.items():
            curr_app_country = country
            part_url = init_url + "/" + localization + "/store"
            for ranking_type in self._ALLWD_RANKINGS:
                ranked_url = part_url + "/" + ranking_type + "/games"
                for device in self._ALLWD_DEVICES:
                    full_url = ranked_url + "/" + device
                    for i in range(0, self._PAGES_LIMIT):
                        try:
                            skip_pages = i * self._GAMES_PER_SITE
                            actual_url = full_url + '?s=store&skipitems=' + str(skip_pages)
                            print(actual_url)
                            response = requests.get(actual_url)
                            soup = BeautifulSoup(response.text, "html.parser")
                            head_contents = soup.findAll("div", {"class": "m-channel-placement-item"})
                            for k in range(0, self._GAMES_PER_SITE):
                                head_app_contents_root = head_contents[k].contents[1]
                                curr_app_data_aid = head_app_contents_root['data-aid']
                                curr_app_app_info_href = head_app_contents_root['href']
                                json_head_contents_attrs = json.loads(head_app_contents_root.attrs['data-m'])
                                curr_app_pid = json_head_contents_attrs['pid']
                                curr_app_title = json_head_contents_attrs['cN']
                                curr_app_rating_value = "UNK"
                                try:
                                    curr_app_rating_value = head_contents[k].find("span",
                                                                                  {'itemprop': "ratingValue"}).text
                                except Exception:
                                    pass

                                curr_app_best_rating = "UNK"
                                try:
                                    curr_app_best_rating = head_contents[k].find("span",
                                                                                 {'itemprop': "bestRating"}).text
                                except Exception:
                                    pass

                                curr_app_review_count = "UNK"
                                try:
                                    curr_app_review_count = head_contents[k].find("span",
                                                                                  {'itemprop': "reviewCount"}).text
                                except Exception:
                                    pass

                                curr_app_position = skip_pages + k + 1
                                curr_app_country = curr_app_country
                                curr_app_ranking_type = ranking_type
                                curr_app_device_type = device
                                curr_app_created_time = execution_date

                                ms_ranked_title = MicrosoftRankedTitle(curr_app_data_aid,
                                                                       curr_app_app_info_href,
                                                                       curr_app_pid,
                                                                       curr_app_title,
                                                                       curr_app_rating_value,
                                                                       curr_app_best_rating,
                                                                       curr_app_review_count,
                                                                       str(curr_app_position),
                                                                       curr_app_country,
                                                                       curr_app_ranking_type,
                                                                       curr_app_device_type,
                                                                       curr_app_created_time)
                                all_microsoft_ranked_titles.append(ms_ranked_title)
                                print("Finished processing for: " + str(actual_url))
                        except Exception:
                            print("Failed for link: " + str(full_url))
        return all_microsoft_ranked_titles

    def get_distinct_hrefs(self, ms_ranked_titles: list):
        distinct_ids = {}
        for ms_ranked_title in ms_ranked_titles:
            if not (ms_ranked_title.pid in distinct_ids):
                distinct_ids[ms_ranked_title.pid] = ms_ranked_title.app_info_href
        return distinct_ids

    def get_microsoft_store_app_info(self, app_pids_ids_dict: dict):
        execution_date = datetime.today().strftime('%Y-%m-%d')
        steam_app_info_list = Parallel(n_jobs=4)(
            delayed(self._get_ms_app_info)(execution_date, pid, actual_url) for pid, actual_url in
            app_pids_ids_dict.items())
        return [app_info for app_info in steam_app_info_list if app_info is not None]

    def _get_ms_app_info(self, execution_date: str, pid: str, actual_url: str):
        try:
            response = requests.get(self._BASE_URL + actual_url)
            soup = BeautifulSoup(response.text, "html.parser")

            curr_app_pid = pid

            curr_app_prod_category = "UNK"
            try:
                curr_app_prod_category = soup.find("meta", {"name": "ms.prod_cat"}).attrs['content']
            except Exception:
                pass

            curr_app_tags = "UNK"
            try:
                curr_app_tags = soup.find("div", {"class": "c-paragraph c-min-two-lines"}).text
            except Exception:
                pass

            div_document_role = soup.find("div", {"id": "information"}).findAll("div", {"role": "document"})

            title = "UNK" #@TODO add title scrapping

            curr_app_publisher = "UNK"
            curr_app_developer = "UNK"
            curr_app_release_date = "UNK"
            curr_app_estimated_size = "UNK"
            has_publisher = False
            has_dev = False
            has_data = False
            has_size = False
            for doc_role in div_document_role:
                if has_size and has_data and has_dev and has_publisher:
                    break
                if str(doc_role.contents[1].text.strip()).__contains__("Wydawca"):
                    curr_app_publisher = doc_role.contents[3].text.strip()
                    has_publisher = True
                elif str(doc_role.contents[1].text.strip()).__contains__("Deweloper"):
                    curr_app_developer = doc_role.contents[3].text.strip()
                    has_dev = True
                elif str(doc_role.contents[1].text.strip()).__contains__("Data wydania"):
                    curr_app_release_date = doc_role.contents[3].text.strip()
                    has_data = True
                elif str(doc_role.contents[1].text.strip()).__contains__("rozmiar"):
                    curr_app_estimated_size = doc_role.contents[3].text.strip()
                    has_size = True

            curr_app_price = "UNK"
            try:
                curr_app_price = soup.find("div", {"id": "productPrice"}).contents[1].findAll("meta")[0].attrs[
                    'content']
            except Exception:
                pass

            store_app_info = MicrosoftAppInfo(pid=curr_app_pid,
                                              title=title,
                                              prod_category=curr_app_prod_category,
                                              tags=curr_app_tags,
                                              publisher=curr_app_publisher,
                                              developer=curr_app_developer,
                                              release_date=curr_app_release_date,
                                              estimated_size=curr_app_estimated_size,
                                              price=curr_app_price,
                                              created_date=execution_date)
            print("Processed correctly: " + str(pid))
            return store_app_info
        except Exception:
            print("Failed for: " + str(pid))
            return None


if __name__ == "__main__":
    mspe = MicrosoftStoreProcessingEngine()
    res = mspe.get_country_top_paid_list()
    for i in res:
        print(str(i))
    app_ids_hrefs_dict = mspe.get_distinct_hrefs(res)
    print(app_ids_hrefs_dict)
    ms_apps = mspe.get_microsoft_store_app_info(app_ids_hrefs_dict)
    for ms_app in ms_apps:
        print(ms_app)
