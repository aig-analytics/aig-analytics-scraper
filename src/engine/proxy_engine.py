import pandas as pd

from src.data_uploaders.datamart_connection import DataMartConnection
from src.utils.configs import ApplicationConfig
from src.engine.executor.proxy_engines import PublicProxyEngine, BaseProxyEngine
from src.engine.executor.web_request_executors import WebRequestExecutor, \
    SingleWebRequestExecutor
from src.utils.gen_purp_utils import constant, warn_and_set_default, \
    safe_to_int, read_yaml_safely, drop_df_cns_safely, \
    append_curr_year_when_missing, safe_to_pd_dt
from src.utils.meta_utils import get_run_metadata


class ProxyParsingEngine:

    @constant
    def PROXY_LIST_URL() -> str:
        return 'https://www.sslproxies.org/'

    @property
    def request_executor(self):
        return self._request_executor

    @request_executor.setter
    def request_executor(self, value):
        self._request_executor = value

    def __init__(self, datamart_connection=None):
        self.datamart_connection = datamart_connection
        self.request_executor = SingleWebRequestExecutor()

    def get_proxy_df(self):
        soup = self.request_executor.get_soup(url=self.PROXY_LIST_URL)
        table = soup.find('table')
        td_entries = table.findAll('td', attrs={'class': ''})

        ip_indices = \
            [el_idx + 1 for el_idx, el in enumerate(td_entries) if el.text == 'anonymous']
        port_indices = [el + 1 for el in ip_indices]

        ips = [td_entries[i].text for i in ip_indices if i < len(td_entries)]
        ports = [td_entries[j].text for j in port_indices if j < len(td_entries)]

        dates = [str(pd.datetime.now()).split(' ')[0] for _ in range(len(ips))]
        unk = ['UNK' for _ in range(len(ips))]
        is_active = ['1' for _ in range(len(ips))]

        proxy_df = pd.DataFrame({
            'hostname': ips,
            'port': ports,
            'code': unk,
            'country': unk,
            'anonimity': unk,
            'google': unk,
            'https': unk,
            'last_checked': unk,
            'is_active': is_active,
            'created_date': dates,
            'created_by': dates,
            'last_modified_date': dates,
            'last_modified_by': dates})

        return proxy_df


if __name__ == '__main__':
    q = ProxyParsingEngine()
    proxy_df = q.get_proxy_df()
    print(proxy_df.to_string())
