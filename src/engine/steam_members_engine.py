from bs4 import BeautifulSoup
from collections.abc import Iterable
from copy import deepcopy
from dagster import execute_pipeline, DagsterInstance
from datetime import datetime
import numpy as np
import os
import pandas as pd
import re
import requests as rq
import swifter
import time
from warnings import warn

from src.data_uploaders.datamart_connection import DataMartConnection
from src.utils.configs import ApplicationConfig
from src.engine.executor.proxy_engines import PublicProxyEngine, BaseProxyEngine
from src.engine.executor.web_request_executors import WebRequestExecutor, \
    SingleWebRequestExecutor
from src.utils.gen_purp_utils import constant, warn_and_set_default, \
    safe_to_int, read_yaml_safely, drop_df_cns_safely, \
    append_curr_year_when_missing, safe_to_pd_dt
from src.utils.meta_utils import get_run_metadata


class SteamMembersEngine:

    @constant
    def DEFAULT_MASTER_URL_PTRN() -> str:
        return 'https://steamcommunity.com/games/steam/members?p={}'

    @constant
    def DEFAULT_PAGE_NUM_SECTION_N() -> str:
        return 'a'

    @constant
    def DEFAULT_PAGE_NUM_RGX() -> str:
        return 'pagelink'

    @constant
    def DEFAULT_USER_PROFILE_SECTION_N() -> str:
        return 'a'

    @constant
    def DEFAULT_USER_PROFILE_RGX() -> str:
        return 'linkFriend_*'

    @constant
    def DEFAULT_FRIENDS_LINK_PTRN() -> str:
        return 'https://steamcommunity.com/{}/{}/friends/'

    # proxies_fname
    @constant
    def DEFAULT_PROXIES_FNAME():
        return '../utils/free_proxy_list.csv'

    @constant
    def DEFAULT_FRIENDS_HREF_RGX() -> str:
        return 'selectable_overlay'

    @constant
    def DEFAULT_USER_OWNED_GAMES_LINK_PTTRN() -> str:
        return 'https://steamcommunity.com/{}/{}/games/?tab=all'

    @constant
    def DEFAULT_USER_FLLWD_GAMES_LINK_PTTRN() -> str:
        return 'https://steamcommunity.com/{}/{}/followedgames/'

    @constant
    def DEFAULT_USER_WISHLIST_LINK_PTTRN() -> str:
        return 'https://store.steampowered.com/wishlist/{}/{}/#sort=order'

    @constant
    def DEFAULT_USER_REVIEWS_LINK_PTTRN() -> str:
        return 'https://steamcommunity.com/{}/{}/reviews/?p={}'

    @constant
    def DEFAULT_USER_RECENT_GAMES_LINK_PTTRN():
        return 'https://steamcommunity.com/{}/{}/games?tab=recent'

    @constant
    def DEFAULT_USER_BADGES_LINK_PTTRN():
        return 'https://steamcommunity.com/{}/{}/badges'

    @constant
    def DEFAULT_MASTER_PROJECT_DIRNAME() -> str:
        return 'aig-analytics-scrapper'

    @constant
    def DEFAULT_CFG_RELATIVE_PTH() -> str:
        return 'src/utils/app.yaml'

    # @constant
    # def DEFAULT_ALL_GAMES_SECTION_N() -> str:
    #     return 'script'
    #
    # @constant
    # def DEFAULT_ALL_GAMES_SOUP_FINDALL_KWGS() -> dict:
    #     return {'lamgauge': 'javascript'}
    #
    # @property
    # def all_games_section_n(self) -> str:
    #     return self._all_games_section_n
    #
    # @all_games_section_n.setter
    # def all_games_section_n(self, new_val: str):
    #     self._set_default_only_if_not(
    #         set_attrn='_all_games_section_n', new_val=new_val,
    #         default=self.DEFAULT_ALL_GAMES_SECTION_N)
    #
    # # this should be fixed
    # @property
    # def all_games_soup_findall_kwgs(self) -> dict:
    #     return self._all_games_soup_findall_kwgs
    #
    # @all_games_soup_findall_kwgs.setter
    # def all_games_soup_findall_kwgs(self, new_val: dict):
    #     self._set_default_only_if_not(
    #         set_attrn='_all_games_soup_findall_kwgs', new_val=new_val,
    #         default=self.DEFAULT_ALL_GAMES_SOUP_FINDALL_KWGS)

    @property
    def user_owned_games_link_pttrn(self) -> str:
        return self._user_owned_games_link_pttrn

    @user_owned_games_link_pttrn.setter
    def user_owned_games_link_pttrn(self, new_val: str):
        self._set_default_only_if_not(
            set_attrn='_user_owned_games_link_pttrn', new_val=new_val,
            default=self.DEFAULT_USER_OWNED_GAMES_LINK_PTTRN)

    @property
    def user_fllwd_games_link_pttrn(self) -> str:
        return self._user_fllwd_games_link_pttrn

    @user_fllwd_games_link_pttrn.setter
    def user_fllwd_games_link_pttrn(self, new_val: str):
        self._set_default_only_if_not(
            set_attrn='_user_fllwd_games_link_pttrn', new_val=new_val,
            default=self.DEFAULT_USER_FLLWD_GAMES_LINK_PTTRN)

    @property
    def user_wishlist_link_pttrn(self) -> str:
        return self._user_wishlist_link_pttrn

    @user_wishlist_link_pttrn.setter
    def user_wishlist_link_pttrn(self, new_val: str):
        self._set_default_only_if_not(
            set_attrn='_user_wishlist_link_pttrn', new_val=new_val,
            default=self.DEFAULT_USER_WISHLIST_LINK_PTTRN)

    @property
    def user_reviews_link_pttrn(self) -> str:
        return self._user_reviews_link_pttrn

    @user_reviews_link_pttrn.setter
    def user_reviews_link_pttrn(self, new_val: str):
        self._set_default_only_if_not(
            set_attrn='_user_reviews_link_pttrn', new_val=new_val,
            default=self.DEFAULT_USER_REVIEWS_LINK_PTTRN)

    @property
    def user_recent_games_link_pttrn(self) -> str:
        return self._user_recent_games_link_pttrn

    @user_recent_games_link_pttrn.setter
    def user_recent_games_link_pttrn(self, new_val: str):
        self._set_default_only_if_not(
            set_attrn='_user_recent_games_link_pttrn', new_val=new_val,
            default=self.DEFAULT_USER_RECENT_GAMES_LINK_PTTRN)

    @property
    def steam_members_master_url_ptrn(self) -> str:
        return self._steam_members_master_url_ptrn

    @steam_members_master_url_ptrn.setter
    def steam_members_master_url_ptrn(self, new_val: str):
        # set_attrn: str, new_val: str, default: str
        self._set_default_only_if_not(
            set_attrn='_steam_members_master_url_ptrn', new_val=new_val,
            default=self.DEFAULT_MASTER_URL_PTRN)

    # DEFAULT_USER_BADGES_LINK_PTTRN
    @property
    def steam_members_badges_url_pttrn(self) -> str:
        return self._steam_members_badges_url_pttrn

    @steam_members_badges_url_pttrn.setter
    def steam_members_badges_url_pttrn(self, new_val: str):
        self._set_default_only_if_not(
            set_attrn='_steam_members_badges_url_pttrn', new_val=new_val,
            default=self.DEFAULT_USER_BADGES_LINK_PTTRN)

        # if not new_val:
        #     default = self.DEFAULT_MASTER_URL_PTRN
        #     warn_msg = \
        #         'No `steam_members_url_ptrn` provided - setting to default: {}'\
        #         .format(default)
        #
        #     warn_and_set_default(
        #         warn_msg=warn_msg, trgt_obj=self,
        #         set_attrn='_steam_members_master_url_ptrn', default_val=default)
        # else:
        #     self._steam_members_master_url_ptrn = new_val

    @property
    def page_num_section_n(self):
        return self._page_num_section_n

    @page_num_section_n.setter
    def page_num_section_n(self, new_val: str):
        self._set_default_only_if_not(
            set_attrn='_page_num_section_n', new_val=new_val,
            default=self.DEFAULT_PAGE_NUM_SECTION_N)
        # self._page_num_section_n = new_val

    @property
    def user_profile_section_n(self) -> str:
        return self._user_profile_section_n

    @user_profile_section_n.setter
    def user_profile_section_n(self, new_val: str):
        self._set_default_only_if_not(
            set_attrn='_user_profile_section_n', new_val=new_val,
            default=self.DEFAULT_USER_PROFILE_SECTION_N)
        # self._user_profile_section_n = new_val

    @property
    def page_num_rgx(self) -> str:
        return self._page_num_rgx

    @page_num_rgx.setter
    def page_num_rgx(self, new_val: str):
        self._set_default_only_if_not(
            set_attrn='_page_num_rgx', new_val=new_val,
            default=self.DEFAULT_PAGE_NUM_RGX)
        # self._page_num_rgx = new_val

    @property
    def user_profile_rgx(self) -> str:
        return self._user_profile_rgx

    @user_profile_rgx.setter
    def user_profile_rgx(self, new_val: str):
        self._set_default_only_if_not(
            set_attrn='_user_profile_rgx', new_val=new_val,
            default=self.DEFAULT_USER_PROFILE_RGX)

    @property
    def proxies_fname(self) -> str:
        return self._proxies_fname

    @proxies_fname.setter
    def proxies_fname(self, new_val: str):
        self._set_default_only_if_not(
            set_attrn='_proxies_fname', new_val=new_val,
            default=self.DEFAULT_PROXIES_FNAME)

    @property
    def proxies(self) -> pd.DataFrame:
        return self._proxies

    @proxies.setter
    def proxies(self, new_val: pd.DataFrame):
        self._proxies = new_val

    @property
    def friends_link_ptrn(self) -> str:
        return self._friends_link_ptrn

    @friends_link_ptrn.setter
    def friends_link_ptrn(self, new_val: str):
        self._set_default_only_if_not(
            set_attrn='_friends_link_ptrn', new_val=new_val,
            default=self.DEFAULT_FRIENDS_LINK_PTRN)

    @property
    def friends_href_rgx(self) -> str:
        return self._friends_href_rgx

    @friends_href_rgx.setter
    def friends_href_rgx(self, new_val: str):
        self._set_default_only_if_not(
            set_attrn='_friends_href_rgx', new_val=new_val,
            default=self.DEFAULT_FRIENDS_HREF_RGX)

    @property
    def usrs_w_alerady_extracted_friends(self) -> Iterable:
        return self._usrs_w_alerady_extracted_friends

    @usrs_w_alerady_extracted_friends.setter
    def usrs_w_alerady_extracted_friends(self, new_val: Iterable):
        self._usrs_w_alerady_extracted_friends = new_val

    # TODO probably deprecated
    @property
    def parsed_steam_members(self) -> Iterable:
        return self._parsed_steam_members

    @parsed_steam_members.setter
    def parsed_steam_members(self, new_val: Iterable):
        self._parsed_steam_members = new_val

    @property
    def cfg_relative_pth(self) -> str:
        return self._cfg_relative_pth

    @cfg_relative_pth.setter
    def cfg_relative_pth(self, new_val: str):
        self._set_default_only_if_not(
            set_attrn='_cfg_relative_pth', new_val=new_val,
            default=self.DEFAULT_CFG_RELATIVE_PTH)

    @property
    def cfg_full_pth(self) -> str:
        return self._cfg_full_pth

    @cfg_full_pth.setter
    def cfg_full_pth(self, new_val: str):
        self._cfg_full_pth = new_val

    @property
    def master_project_dirname(self) -> str:
        return self._master_project_dirname

    @master_project_dirname.setter
    def master_project_dirname(self, new_val: str):
        self._set_default_only_if_not(
            set_attrn='_master_project_dirname', new_val=new_val,
            default=self.DEFAULT_MASTER_PROJECT_DIRNAME)

    # SimpleWebRequestExecutor()
    @property
    def request_execution_engine(self) -> WebRequestExecutor:
        return self._request_execution_engine

    @request_execution_engine.setter
    def request_execution_engine(self, new_val: WebRequestExecutor):
        self._request_execution_engine = new_val

    # friends, games, wl, rec_played, revs

    # @property
    # def friends_list(self) -> list:
    #     return self._friends_list
    #
    # @friends_list.setter
    # def friends_list(self, new_val: list):
    #     if not isinstance(new_val, list):
    #         raise TypeError('Must asign pd.DataFrame to friends_df')
    #     self._friends_list = new_val

    @property
    def games_df(self) -> pd.DataFrame:
        return self._games_df

    @games_df.setter
    def games_df(self, new_val: pd.DataFrame):
        self._games_df = new_val
        
    @property
    def wl_df(self) -> pd.DataFrame:
        return self._wl_df

    @wl_df.setter
    def wl_df(self, new_val: pd.DataFrame):
        self._wl_df = new_val
        
    @property
    def rec_played_df(self) -> pd.DataFrame:
        return self._rec_played_df

    @rec_played_df.setter
    def rec_played_df(self, new_val: pd.DataFrame):
        self._rec_played_df = new_val
        
    @property
    def revs_df(self) -> pd.DataFrame:
        return self._revs_df

    @revs_df.setter
    def revs_df(self, new_val: pd.DataFrame):
        self._revs_df = new_val

    @property
    def fllwd_games_df(self) -> pd.DataFrame:
        return self._fllwd_games_df

    @fllwd_games_df.setter
    def fllwd_games_df(self, new_val: pd.DataFrame):
        self._fllwd_games_df = new_val

    @property
    def friends_df(self) -> pd.DataFrame:
        return self._friends_df

    @friends_df.setter
    def friends_df(self, new_val: pd.DataFrame):
        self._friends_df = new_val

    @property
    def parsed_steam_members_ids(self):
        return self._parsed_steam_members_ids

    @parsed_steam_members_ids.setter
    def parsed_steam_members_ids(self, value):
        self._parsed_steam_members_ids = value

    @property
    def parsed_steam_members_urls(self) -> list:
        return self._parsed_steam_members_urls

    @parsed_steam_members_urls.setter
    def parsed_steam_members_urls(self, new_val: list):
        self._parsed_steam_members_urls = new_val

    @property
    def parsed_steam_members_locations(self) -> list:
        return self._parsed_steam_members_locations

    @parsed_steam_members_locations.setter
    def parsed_steam_members_locations(self, new_val: list):
        self._parsed_steam_members_locations = new_val

    @property
    def parsed_steam_members_lvls(self) -> list:
        return self._parsed_steam_members_lvls

    @parsed_steam_members_lvls.setter
    def parsed_steam_members_lvls(self, new_val: list):
        self._parsed_steam_members_lvls = new_val

    def __init__(
            self, request_execution_engine: WebRequestExecutor,
            steam_members_master_url_ptrn: str = '',
            page_num_section_n: str = 'a', user_profile_section_n: str = 'a',
            page_num_rgx: str = '', user_profile_rgx: str = '',
            proxies_fname: str = '', friends_link_ptrn: str = '',
            friends_href_rgx: str = '', user_owned_games_link_pttrn: str = '',
            user_fllwd_games_link_pttrn: str = '',
            user_wishlist_link_pttrn: str = '',
            user_reviews_link_pttrn: str = '',
            user_recent_games_link_pttrn: str = '',
            steam_members_badges_url_pttrn: str = ''):

        self.page_num_section_n = page_num_section_n
        self.user_profile_section_n = user_profile_section_n
        self.page_num_rgx = page_num_rgx
        self.user_profile_rgx = user_profile_rgx
        # self.proxies_fname = proxies_fname
        self.friends_href_rgx = friends_href_rgx

        self.steam_members_master_url_ptrn = steam_members_master_url_ptrn
        self.friends_link_ptrn = friends_link_ptrn
        self.user_owned_games_link_pttrn = user_owned_games_link_pttrn
        self.user_fllwd_games_link_pttrn = user_fllwd_games_link_pttrn
        self.user_wishlist_link_pttrn = user_wishlist_link_pttrn
        self.user_reviews_link_pttrn = user_reviews_link_pttrn
        self.user_recent_games_link_pttrn = user_recent_games_link_pttrn
        self.steam_members_badges_url_pttrn = steam_members_badges_url_pttrn

        self.request_execution_engine = request_execution_engine

        # self.master_project_dirname = master_project_dirname
        # self.cfg_relative_pth = cfg_relative_pth

        # self._load_proxies_file()

        self.usrs_w_alerady_extracted_friends = []
        self.parsed_steam_members_ids = []
        self.parsed_steam_members = []
        self.parsed_steam_members_urls = []
        self.parsed_steam_members_locations = []
        self.parsed_steam_members_lvls = []

        self.init_data_props()

        # self.cfg_full_pth = ''

    # this may be stupid as fuck
    # def get_min_page_num(self):
    #     pass

    # def _set_full_cfg_pth(self):
    #     """
    #     Setter for the full path to config
    #
    #     Returns
    #     -------
    #     None
    #         None
    #
    #     """
    #     curr_file_dir_as_list = str(__file__).split(os.sep)[:-1]
    #     master_dir_idx = np.argmax(
    #         np.array(curr_file_dir_as_list) == self.master_project_dirname)
    #
    #     master_dir_super_pth = \
    #         os.sep.join(curr_file_dir_as_list[:master_dir_idx])
    #
    #     self.cfg_full_pth = \
    #         '{}/{}/{}'.format(
    #             master_dir_super_pth, self.master_project_dirname,
    #             self.cfg_relative_pth)

    # def i_accept_list(self, dummy_param: list):
    #     pass

    def init_data_props(self):
        # self.friends_list = []
        self.friends_df = pd.DataFrame()
        self.games_df = pd.DataFrame()
        self.wl_df = pd.DataFrame()
        self.rec_played_df = pd.DataFrame()
        self.revs_df = pd.DataFrame()
        self.fllwd_games_df = pd.DataFrame()

    def _load_proxies_file(self):
        """
        Loads csv full of proxies
        Returns
        -------

        """
        self.proxies = pd.read_csv(self.proxies_fname)

    def _set_default_only_if_not(
            self, set_attrn: str, new_val: object, default: object):
        """
        Wrapper for setting defaults with warning

        Parameters
        ----------
        set_attrn: str
            name of set attr
        new_val: object
            provided new value
        default: object
            default value

        Returns
        -------
        None
            None

        """
        if not new_val:
            warn_msg = \
                'No `{}` provided - setting it to default: {}' \
                .format(set_attrn, default)

            warn_and_set_default(
                warn_msg=warn_msg, trgt_obj=self, set_attrn=set_attrn,
                default_val=default, verbose=False)
        else:
            setattr(self, set_attrn, new_val)

    def get_pub_usrs_max_page_num(self, des_proxy: str = ''):
        """
        Getter for maximum page number from steam members page

        Returns:
        int
            maximum found on page pagenum

        """

        des_page_link = \
            self.steam_members_master_url_ptrn.format(np.random.randint(1, 100))

        first_page_soup = \
            self._get_des_page_soup(
                des_page_link=des_page_link, des_proxy=des_proxy)
        # print(first_page_soup)
        # print(first_page_soup.find_all(
        #         self.page_num_section_n,
        #         class_=re.compile(r'{}'.format(self.page_num_rgx))))

        # print(first_page_soup)

        page_displayed_page_nums = \
            [safe_to_int(pagenum_tag.string)
             for pagenum_tag in first_page_soup.find_all(
                self.page_num_section_n,
                class_=re.compile(r'{}'.format(self.page_num_rgx)))
             if safe_to_int(pagenum_tag.string)]

        max_page_num = max(page_displayed_page_nums)
        return max_page_num

    def _get_des_page(self, des_page_link: str, des_proxy: str = ''):
        """
        Getter for desired page request result

        Parameters
        ----------
        des_page_link: int
            number of desired page to be pasted into master url

        Returns
        -------
        bytes
            `content` property of recieved request

        """

        if not des_proxy:
            req_res = \
                rq.get(des_page_link)
        else:
            proxies = {
                'http:': des_proxy,
                'https:': des_proxy}
            req_res = \
                rq.get(des_page_link, proxies=proxies)

        return req_res

    def _get_des_proxy_str(self, des_proxy_iloc: int = -1):
        """
        Getter for proxy str for provided iloc

        Parameters
        ----------
        des_proxy_iloc: int
            des proxy iloc

        Returns
        -------
        str
            proxy str for desired iloc

        """

        if des_proxy_iloc == -1:
            return ''

        des_proxy_row = self.proxies.iloc[des_proxy_iloc]
        # hostname, port
        host_address = des_proxy_row['hostname']
        port = des_proxy_row['port']
        des_proxy_str = 'http://{}:{}'.format(host_address, port)
        return des_proxy_str

    def _get_des_page_soup(
            self, des_page_link: str, des_proxy: str = '') -> BeautifulSoup:
        """
        Getter for BeautifulSoup parsed website

        Parameters
        ----------
        des_page_link: int
            number of desired page to be fetched and converted to souop

        Returns
        -------
        BeautifulSoup
            the result of get + BeautifulSoup call

        """

        # des_page = \
        #     self._get_des_page(des_page_link=des_page_link, des_proxy=des_proxy)

        des_page_soup: BeautifulSoup = \
            self.request_execution_engine.get_soup(
                url=des_page_link, use_proxy=True)

        # des_page_soup = BeautifulSoup(des_page.content)
        return des_page_soup

    def get_all_member_link_per_page(
            self, curr_page_num: int, des_proxy: str = '') -> list:

        des_page_link = \
            self.steam_members_master_url_ptrn.format(curr_page_num)

        curr_page_soup = \
            self._get_des_page_soup(
                des_page_link=des_page_link, des_proxy=des_proxy)

        curr_page_members_tags = \
            curr_page_soup.find_all(
                self.user_profile_section_n,
                class_=re.compile(r'{}'.format(self.user_profile_rgx)))

        curr_page_members_hrefs = \
            [member_tag['href'] for member_tag in curr_page_members_tags
             if member_tag.get('href', None)]

        return curr_page_members_hrefs

    def get_friends_links(
            self, player_id: str, des_proxy: str = '',
            go_recursive: bool = False):
        """
        Getter for provided id friends links

        Parameters
        ----------
        player_id: str
            steam player id
        des_proxy: str
            des proxy to use with get

        Returns
        -------
        list
            list of all friends of provided player id

        """

        print('Processing player: {}'.format(player_id))

        # take this out of here myby?
        player_friends_link = \
            self.friends_link_ptrn.format(
                'profiles' if safe_to_int(player_id) and len(player_id) == 17
                else 'id',
                player_id)

        friends_page_soup = \
            self._get_des_page_soup(
                des_page_link=player_friends_link, des_proxy=des_proxy)
        if friends_page_soup is None:
            raise ValueError("soup can't be None")
        # sanity_check = \
        #     friends_page_soup.find_all('a', class_=self.friends_href_rgx)

        all_friends_links = \
            [friend['href'] for friend
             in friends_page_soup.find_all('a', class_=self.friends_href_rgx)
             if friend.get('href', None)]

        if not go_recursive:
            return all_friends_links

        self.usrs_w_alerady_extracted_friends.append(player_id)
        self.parsed_steam_members += list(np.unique(all_friends_links))
        self.parsed_steam_members = list(np.unique(self.parsed_steam_members))

        # print(
        #     'Found : {} friends of a: {}'
        #     .format(len(all_friends_links), player_id))

        # print(sanity_check[0])
        # print(len(sanity_check))
        # print(all_friends_links[0])
        # input('sanity check')

        if go_recursive and all_friends_links:

            plain_ids = \
                [player_href.split('/')[-1]
                 for player_href in all_friends_links]

            print('Going deeper!')

            for friend_id in plain_ids:

                if friend_id in self.usrs_w_alerady_extracted_friends:
                    print(
                        'I have already extracted friends for: {} -> skipping'
                        .format(friend_id))
                    continue

                self.generic_try_catch_w_proxy(
                    des_callable=self.get_friends_links,
                    callable_kwargs={
                        'player_id': friend_id, 'go_recursive': go_recursive},
                    wait_upfront=True)

                # all_friends_links += fof_list

        # return all_friends_links

    def generic_try_catch_w_proxy(
            self, des_callable: callable, callable_kwargs,
            wait_upfront: bool = False):
        """
        Wrapper around common try-catch expressoins

        Parameters
        ----------
        des_callable: callable
            function / method to call
        callable_kwargs: dict
            kwargs for des_callable

        Returns
        -------
        object
            response

        """

        if wait_upfront:
            time.sleep(
                2 + abs(np.random.randn()) if abs(np.random.randn()) < 2.5
                else 1)

        data_extracted = False
        curr_proxy_iloc = -1

        usd_kwargs = deepcopy(callable_kwargs)

        while not data_extracted:
            curr_proxy_str = \
                self._get_des_proxy_str(des_proxy_iloc=curr_proxy_iloc)

            usd_kwargs['des_proxy'] = curr_proxy_str

            try:

                res = des_callable(**usd_kwargs)
                data_extracted = True
            except Exception as exc:
                # print('Failed to parse max with: {}'.format(curr_proxy_str))
                data_extracted = False
            time.sleep(
                2 + abs(np.random.randn()) if abs(
                    np.random.randn()) < 2.5 else 1)
        return res

    def get_all_member_links(self, test_run: bool = False):
        all_member_links = []

        max_page_num = \
            self.generic_try_catch_w_proxy(
                des_callable=self.get_pub_usrs_max_page_num, callable_kwargs={})

        for curr_page_num in range(1, max_page_num + 1, 1):

            curr_page_links = \
                self.generic_try_catch_w_proxy(
                    des_callable=self.get_all_member_link_per_page,
                    callable_kwargs={'curr_page_num': curr_page_num})

            all_member_links += curr_page_links

            # print('Parsed users: {}'.format(len(all_member_links)))

        all_members_links_series = pd.Series(all_member_links)
        all_members_links_series.to_csv('sample_parsed_users_links.csv')

    def get_all_usrs_friends(
            self, links_for_friends: Iterable,
            go_recursive: bool = False) -> None:
        # friends = []
        for parsed_user_link in links_for_friends:
            player_id = parsed_user_link.split('/')[-1]
            # print('Currently checking friends for user: {}'.format(player_id))
            curr_user_kwargs = \
                {'player_id': player_id, 'go_recursive': go_recursive}
            # curr_players_friends = \
            self.generic_try_catch_w_proxy(
                des_callable=self.get_friends_links,
                callable_kwargs=curr_user_kwargs)

            # print('friends count for user: {} is: {}'
            #       .format(player_id, len(self.parsed_steam_members)))

            # friends += curr_players_friends
            # friends = list(np.unique(friends))

            # print('Got: {} users so far'.format(len(self.parsed_steam_members)))
        pd.Series(self.parsed_steam_members)\
            .to_csv('parsed_friends_recursive.csv')
        # return friends

    def get_usr_games_df(
            self, player_id: str, all_games_section_n: str = 'script',
            soup_findall_kwgrs: dict = {'language': 'javascript'}):
        """
        Gets df with all user games

        Parameters
        ----------
        player_id: str
            player's id
        all_games_section_n: str
            name of section holding all games info
        soup_findall_kwgrs: dict
            kwargs for find_all() method

        Returns
        -------
        pd.DataFrame
            df with all games

        """
        user_all_games_link = self.user_owned_games_link_pttrn.format(
            # 'id' if safe_to_int(player_id) is None else 'profiles',
            'profiles' if safe_to_int(player_id) and len(player_id) == 17
            else 'id',
            player_id)

        soup: BeautifulSoup = \
            self.request_execution_engine.get_soup(
                url=user_all_games_link, use_proxy=True)
        if soup is None:
            raise ValueError("soup can't be None")
        # soup = BeautifulSoup(rq.get(user_all_games_link).content)

        all_games_json_str = \
            self._get_all_games_json_str(
                all_games_soup=soup, all_games_section_n=all_games_section_n,
                soup_findall_kwgrs=soup_findall_kwgrs)

        all_games_df = pd.read_json(all_games_json_str)
        if "is_visible_in_steam_china" in all_games_df.columns:
            all_games_df =\
                all_games_df.drop(columns=["is_visible_in_steam_china"])
        if all_games_df.empty:
            return all_games_df

        all_games_df['player_id'] = player_id

        games_cols_drpd = \
            drop_df_cns_safely(
                input_df=all_games_df, drpd_cns=['logo', 'availStatLinks', 'app_type'])

        games_cols_drpd['appid'] = \
            games_cols_drpd['appid'].apply(lambda x: str(int(x)))

        games_cols_drpd['last_played'] = \
            games_cols_drpd['last_played']\
            .apply(lambda x: pd.to_datetime(x, unit='s'))

        # for drpd_cn in ['logo', 'availStatLinks']:
        #     if drpd_cn in games_cols_drpd.columns:
        #         games_cols_drpd = games_cols_drpd.drop(columns=[drpd_cn])
        #
        # # games_cols_drpd = all_games_df.drop(columns=['logo', 'availStatLinks'])
        # if not games_cols_drpd.empty:
        #     print(games_cols_drpd.head().to_string())
        #     input('check')
        return games_cols_drpd

    def _get_all_games_json_str(
            self, all_games_soup: BeautifulSoup, all_games_section_n: str,
            soup_findall_kwgrs: dict):
        """
        Hardcoded getter for all games json

        Parameters
        ----------
        all_games_soup: BeautifulSoup
            soup from all games page
        all_games_section_n: str
            name of section holding all games info
        soup_findall_kwgrs: dict
            kwargs for find_all() method

        Returns
        -------
        str
            json str with all games

        """

        # print(all_games_soup)
        # print(all_games_soup.find_all(
        #             all_games_section_n, **soup_findall_kwgrs))
        #
        # input('sanity check')

        all_games_script_raw = '{}'
        try:
            all_games_script_raw = \
                r'{}'.format(
                    all_games_soup.find_all(
                        all_games_section_n, **soup_findall_kwgrs)[0].contents[0])\
                .strip().split('\n')[0].strip().replace('var rgGames = ', '')\
                .strip()
        except Exception as exc:
            pass
            # print(
            #     'On attempt to read user games json following error occured: '
            #     '{}'.format(exc))

        if all_games_script_raw == '{}':
            return all_games_script_raw

        all_games_script = all_games_script_raw
        if all_games_script_raw[-1] == ';':
            all_games_script = all_games_script_raw[:-1]

        return all_games_script

    def get_usr_fllwd_games_df(
            self, player_id: str, fllwd_games_section_n: str = 'div',
            soup_findall_kwgrs: dict = {'class_': 'gameListRowItemName'}):
        """
        Gets followed games df for user

        Parameters
        ----------
        player_id: str
            player's id
        fllwd_games_section_n: str
            name of the section holding the fllwd games
        soup_findall_kwgrs: str
            kwargs for findall method

        Returns
        -------
        pd.DataFrame
            df with followed  games

        """

        user_fllwd_games_link = self.user_fllwd_games_link_pttrn.format(
            # 'id' if safe_to_int(player_id) is None else 'profiles',
            'profiles' if safe_to_int(player_id) and len(player_id) == 17
            else 'id',
            player_id)

        soup: BeautifulSoup = \
            self.request_execution_engine.get_soup(
                url=user_fllwd_games_link, use_proxy=True)

        if soup is None:
            raise ValueError("soup can't be None")

        # soup = BeautifulSoup(rq.get(user_fllwd_games_link).content)

        fllwd_games_list = \
            [fllwd_game_entry.text for fllwd_game_entry in
             soup.find_all(fllwd_games_section_n, **soup_findall_kwgrs)]

        if not fllwd_games_list:
            return pd.DataFrame()

        # TODO get appid as well
        fllwd_games_df = pd.DataFrame({'game_name': fllwd_games_list})
        fllwd_games_df['player_id'] = player_id

        return fllwd_games_df

    def get_usr_wishlist_df(
            self, player_id: str, wishlist_section_n: str = 'script',
            wishlist_findall_kwgrs: dict = {'type': None}):
        """
        Getter for user wishlist df

        Parameters
        ----------
        player_id: str
            player id
        wishlist_section_n: str
            name of section to look for wishlist
        wishlist_findall_kwgrs: dict
            kwargs for findall

        Returns
        -------
        pd.DataFrame
            df with wishlist of a user

        """

        user_wishlist_link = self.user_wishlist_link_pttrn.format(
            # 'id' if safe_to_int(player_id) is None else 'profiles',
            'profiles' if safe_to_int(player_id) and len(player_id) == 17
            else 'id',
            player_id)

        soup: BeautifulSoup = \
            self.request_execution_engine.get_soup(
                url=user_wishlist_link, use_proxy=True)

        if soup is None:
            raise ValueError('soup can`t be None')

        # soup = BeautifulSoup(rq.get(user_wishlist_link).content)

        wl_json_str = \
            self._get_wishlist_json_str(
                wishlist_soup=soup, wishlist_section_n=wishlist_section_n,
                wishlist_findall_kwgrs=wishlist_findall_kwgrs)
        wl_df = pd.read_json(wl_json_str)

        if wl_df.empty:
            return wl_df

        wl_df['player_id'] = player_id

        wl_df['appid'] = wl_df['appid'].apply(lambda x: str(int(x)))

        return wl_df

    def _get_wishlist_json_str(
            self, wishlist_soup: BeautifulSoup,
            wishlist_section_n: str = 'script',
            wishlist_findall_kwgrs: dict = {'type': None}):
        """
        Getter for hardcoded wishlist json

        Parameters
        ----------
        wishlist_soup: BeautifulSoup
            soup with wishlist page
        wishlist_section_n: str
            name of section holding wishlist info
        wishlist_findall_kwgrs: dict
            kwargs for findall

        Returns
        -------

        """

        # print(wishlist_soup)
        # print(wishlist_soup.find_all(
        #         wishlist_section_n, **wishlist_findall_kwgrs))
        #
        # input('sanity_check')

        wl_json_script_raw = '{}'

        try:
            wl_json_script_raw = \
                [wl_soup_bit.contents[0] for wl_soup_bit
                 in wishlist_soup.find_all(
                    wishlist_section_n, **wishlist_findall_kwgrs) if
                 'var g_rgWishlistData = ' in wl_soup_bit.contents[0]][0]\
                .strip().split('\n')[0].replace('var g_rgWishlistData = ', '')\
                .strip()
        except Exception as exc:
            # print(
            #     'On attempt to read user wishlist json following error occured: '
            #     '{}'.format(exc))
            pass

        if wl_json_script_raw == '{}':
            return wl_json_script_raw

        wl_json_script = wl_json_script_raw
        if wl_json_script_raw[-1] == ';':
            wl_json_script = wl_json_script_raw[:-1]

        return wl_json_script

    # TODO finish this and recently played games
    def get_usr_reviews(
            self, player_id: str, rec_played_section_n: str = 'script',
            rec_played_findall_kwgrs: dict = {'language': 'javascript'}):

        curr_rev_pg_num = 1
        app_ids = []
        revs = []
        posted_dts = []
        edited_dts = []
        hours_played = []
        hours_when_reved = []

        while True:
            curr_user_revs_link = \
                self.user_reviews_link_pttrn.format(
                    # 'profiles' if safe_to_int(player_id) is None else 'id',
                    'profiles' if safe_to_int(player_id) and len(
                        player_id) == 17
                    else 'id',
                    player_id, curr_rev_pg_num)

            # print('curr_user_revs_link')
            # print(curr_user_revs_link)

            curr_page_soup: BeautifulSoup = \
                self.request_execution_engine.get_soup(
                    url=curr_user_revs_link, use_proxy=True)

            if curr_page_soup is None:
                raise ValueError("soup can't be None")

            # curr_page_soup = BeautifulSoup(rq.get(curr_user_revs_link).content)

            # print('### curr_page_soup strarts ###')
            # print(curr_page_soup)
            # print('### curr_page_soup ends ###')
            #
            # print('### findall startas ###')
            # print(curr_page_soup.find_all('div', **{'class_': 'title'}))
            # print('### findall ends ###')

            if not curr_page_soup.find_all('div', **{'class_': 'title'}):
                break

            app_ids += \
                [el.find('a')['href'].split('/')[-2] for el in
                 curr_page_soup.find_all('div', **{'class_': 'title'})]

            revs += \
                [el.find('a').text
                 for el
                 in curr_page_soup.find_all('div', **{'class_': 'title'})]

            posted_dts += \
                [safe_to_pd_dt(
                    input_str=append_curr_year_when_missing(
                        input_str_dt=el.text.strip().split('.')[0].replace(
                            'Posted ', '').strip()))
                 for el in curr_page_soup.find_all('div', **{'class_': 'posted'})]

            # print(curr_page_soup.find_all('div', **{'class_': 'posted'}))
            # print(
            #     [el.text.strip().split('.')[1].replace('Last edited ', '').strip()
            #      for el in
            #      curr_page_soup.find_all('div', **{'class_': 'posted'})])
            # append_curr_year_when_missing, safe_to_pd_dt

            edited_dts += \
                [safe_to_pd_dt(
                    input_str=append_curr_year_when_missing(
                        input_str_dt=el.text.strip().split('.')[1].replace(
                            'Last edited ', '').strip()))
                 for el in
                 curr_page_soup.find_all('div', **{'class_': 'posted'})]

            hours_played += \
                [el.text.split('hrs')[0].strip()
                 for el in curr_page_soup.find_all('div', **{'class_': 'hours'})]

            hours_when_reved += \
                [el.text.split('hrs')[1].split('(')[1].strip()
                 if len(el.text.split('hrs')[1].split('(')) > 1 else None
                 for el in curr_page_soup.find_all('div', **{'class_': 'hours'})]

            curr_rev_pg_num += 1

        des_cns = \
            ['player_id', 'appid', 'review', 'posted_dts', 'edited_dts',
             'hours_played', 'hours_when_reved']

        if not app_ids:
            return pd.DataFrame(columns=des_cns)

        usr_reviews_df = \
            pd.DataFrame(
                dict(
                    zip(tuple(des_cns),
                        ([player_id for _ in app_ids],
                         app_ids, revs, posted_dts, edited_dts, hours_played,
                         hours_when_reved))))
        usr_reviews_df['app_id'] = \
            usr_reviews_df['app_id'].apply(lambda x: str(int(x)))

        return usr_reviews_df

        # return pd.DataFrame(
        #     dict(
        #         zip(tuple(des_cns),
        #             ([player_id for _ in app_ids],
        #              app_ids, revs, posted_dts, edited_dts, hours_played,
        #              hours_when_reved))))

    def get_recently_played_games(
            self, player_id: str, rec_played_section_n: str = 'script',
            rec_played_findall_kwgrs: dict = {'language': 'javascript'}):

        user_rec_played_link = self.user_recent_games_link_pttrn.format(
            # 'profiles' if safe_to_int(player_id) is None else 'id',
            'profiles' if safe_to_int(player_id) and len(player_id) == 17
            else 'id',
            player_id)

        soup: BeautifulSoup = \
            self.request_execution_engine.get_soup(
                url=user_rec_played_link, use_proxy=True)

        if soup is None:
            raise ValueError('soup can`t be None')

        # soup = BeautifulSoup(rq.get(user_rec_played_link).content)

        rec_played_games_json = self._get_all_games_json_str(
            all_games_soup=soup, all_games_section_n=rec_played_section_n,
            soup_findall_kwgrs=rec_played_findall_kwgrs)

        rec_played_df = pd.read_json(rec_played_games_json)

        if rec_played_df.empty:
            return rec_played_df

        rec_played_df['player_id'] = player_id
        # rec_played_cols_drpd = \
        #     rec_played_df.drop(columns=[
        #         'logo', 'availStatLinks', 'ach_completion'])

        rec_played_cols_drpd = \
            drop_df_cns_safely(
                input_df=rec_played_df,
                drpd_cns=['logo', 'availStatLinks', 'ach_completion'])

        rec_played_cols_drpd['appid'] = \
            rec_played_cols_drpd['appid'].apply(lambda x: str(int(x)))

        rec_played_cols_drpd['last_played'] = \
            rec_played_cols_drpd['last_played']\
            .apply(lambda x: pd.to_datetime(x, unit='s'))

        return rec_played_cols_drpd

    def _append_player_metadata(self, player_id: str):

        player_url = \
            self.DEFAULT_FRIENDS_LINK_PTRN.format(
                'profiles' if safe_to_int(player_id) and len(player_id) == 17
                else 'id',
                player_id).replace('/friends/', '')

        self.parsed_steam_members_urls.append(player_url)
        self.parsed_steam_members_ids.append(player_id)

        soup_location: BeautifulSoup = \
            self.request_execution_engine.get_soup(
                url=player_url, use_proxy=True)

        if soup_location is None:
            raise ValueError('soup can`t be None')

        # soup_location = BeautifulSoup(rq.get(player_url).content)

        player_location = ''
        location_info = \
            soup_location.find_all('div', class_="header_real_name ellipsis")

        if location_info:
            player_location = location_info[0].contents[-1].strip()

        self.parsed_steam_members_locations.append(player_location)

        soup_lvl: BeautifulSoup = \
            self.request_execution_engine.get_soup(
                url=player_url, use_proxy=True)

        if soup_lvl is None:
            raise ValueError('soup can`t be None')

        # soup_lvl = BeautifulSoup(rq.get(player_url + '/badges').content)

        lvl_info = soup_lvl.find_all('span', class_="friendPlayerLevelNum")

        player_lvl = ''
        if lvl_info:
            player_lvl = lvl_info[0].text

        self.parsed_steam_members_lvls.append(player_lvl)

    def parse_player(self, player_id: str):
        """
        Parses all info about desired player

        Parameters
        ----------
        player_id: str
            desired player id

        Returns
        -------
        tuple
            tuple of list and pd.DataFrames containing scrapped info

        """
        try:
            friends_urls = self.get_friends_links(player_id=player_id)
            friends = self.get_friends_df(friends_urls=friends_urls)
            games = self.get_usr_games_df(player_id=player_id)
            wl = self.get_usr_wishlist_df(player_id=player_id)
            rec_played = self.get_recently_played_games(player_id=player_id)
            revs = self.get_usr_reviews(player_id=player_id)
            fllwd_games = self.get_usr_fllwd_games_df(player_id=player_id)
            self._append_player_metadata(player_id=player_id)
        except Exception as exc:
            print('Faield to parse {}'.format(player_id))
            friends = pd.DataFrame()
            games = pd.DataFrame()
            wl = pd.DataFrame()
            rec_played = pd.DataFrame()
            revs = pd.DataFrame()
            fllwd_games = pd.DataFrame()

        return friends, games, wl, rec_played, revs, fllwd_games

    def get_player_id_from_url(self, player_url: str) -> str:
        """
        Getter for user id / profile number from provided member link
        
        Parameters
        ----------
        player_url: str
            full member link

        Returns
        -------
        str
            player_id extracted from provided link 

        """

        return player_url.split('/')[-1]

    def get_friends_df(self, friends_urls: Iterable) -> pd.DataFrame:
        """
        Gets friends df from list of frients urls

        Parameters
        ----------
        friends_urls: Iterable
            collection of members urls

        Returns
        -------
        pd.DataFrame
            df with friends ready to be concated and uploaded

        """

        if not friends_urls:
            return pd.DataFrame()

        friends_ids = \
            [self.get_player_id_from_url(player_url=url)
             for url in friends_urls]

        friends_df = pd.DataFrame(
            {'player_url': friends_urls,
             'player_id': friends_ids})

        create_modif_df = \
            str(pd.datetime.now().replace(microsecond=0))

        friends_df['created_date'] = create_modif_df

        friends_df['last_modified_date'] = create_modif_df

        friends_df['to_be_scraped'] = 1

        return friends_df

    def append_scraped_dt(self, input_df: pd.DataFrame, df_attr_n: str):
        if df_attr_n != 'friends_df':
            input_df['scraped_dt'] = \
                str(pd.datetime.now().replace(microsecond=0))
        return input_df

    def append_scraper_caches(
            self, friends, games, wl, rec_played, revs, fllwd_games, *args):

        # self.friends_list += friends

        updates_dfs = [friends, games, wl, rec_played, revs, fllwd_games]
        targets_df = \
            [self.friends_df, self.games_df, self.wl_df, self.rec_played_df,
             self.revs_df, self.fllwd_games_df]
        targets_attrs_ns = \
            ['friends_df', 'games_df', 'wl_df', 'rec_played_df', 'revs_df',
             'fllwd_games_df']

        # for df, df_name in \
        #         zip(updates_dfs,
        #             ['frnds', 'games', 'wl', 'rec_played', 'revs', 'fllwd_games']):
        #
        #     print(df_name)
        #     print(df.head().to_string())

        for target_df, target_df_attr_n, update_df in \
                zip(targets_df, targets_attrs_ns, updates_dfs):
            concat_target_update_df = \
                pd.concat([target_df, update_df]).reset_index(drop=True)

            if target_df_attr_n == 'friends_df':
                concat_target_update_df = \
                    concat_target_update_df.drop_duplicates()

            setattr(self, target_df_attr_n,
                    self.append_scraped_dt(
                        input_df=concat_target_update_df,
                        df_attr_n=target_df_attr_n))

        # self.friends_list = []
        # self.games_df = pd.DataFrame()
        # self.wl_df = pd.DataFrame()
        # self.rec_played_df = pd.DataFrame()
        # self.revs_df = pd.DataFrame()
        # self.fllwd_games_df = pd.DataFrame()

    def _update_friends_df_with_parsed_players(self, players_ids: Iterable):

        # print(len(self.parsed_steam_members_urls))
        # print(len(players_ids))

        curr_run_parsed_players_df = pd.DataFrame(
            {'player_url': self.parsed_steam_members_urls,
             'player_id': self.parsed_steam_members_ids,
             'player_lvl': self.parsed_steam_members_lvls,
             'player_location': self.parsed_steam_members_locations})

        create_modif_df = \
            str(datetime.now().replace(microsecond=0))

        curr_run_parsed_players_df['created_date'] = create_modif_df

        curr_run_parsed_players_df['last_modified_date'] = create_modif_df

        curr_run_parsed_players_df['to_be_scraped'] = 0

        # print(self.friends_df.head(20).to_string())

        scraped_friends_df = pd.DataFrame()
        if not self.friends_df.empty:
            scraped_friends_df = self.friends_df[
                         self.friends_df['player_url'].swifter
                             .apply(
                             lambda x: True if x not in self.parsed_steam_members_urls
                             else False)]

        self.friends_df = \
            pd.concat(
                [scraped_friends_df, curr_run_parsed_players_df])\
            .fillna('')\
            .reset_index(drop=True)

        # self.parsed_steam_members_links

    def parse_desired_players(self, players_ids: list):

        for player_id in players_ids:
            curr_player_res = self.parse_player(player_id=player_id)
            self.append_scraper_caches(*curr_player_res)
        # append parsed players
        self._update_friends_df_with_parsed_players(players_ids=players_ids)
        # print('sanity check')
        # print(self.revs_df.head().to_string())


if __name__ == '__main__':
    pass

    # app_config = ApplicationConfig(cfg_path='../utils/app.yaml').config
    #
    # # print(app_config)
    # # input('sanity check')
    #
    # datamart_connection = DataMartConnection(
    #     dbname=app_config['database.games-analytics.database'],
    #     dbuser=app_config['database.games-analytics.user'],
    #     dbhost=app_config['database.games-analytics.ip'],
    #     dbpass=app_config['database.games-analytics.password'],
    #     dbport=app_config['database.games-analytics.port'])
    #
    # pub_proxy_engine = \
    #     PublicProxyEngine(datamart_connection=datamart_connection)
    #
    # single_req_executor = \
    #     SingleWebRequestExecutor(proxy_engine=pub_proxy_engine)
    #
    # sme = SteamMembersEngine(request_execution_engine=single_req_executor)
    # # sme.get_all_member_links()
    # # res = sme.get_friends_links(player_id='afarnsworth')
    # # print(res[0])
    #
    # parsed_users = \
    #     pd.read_csv('/home/os/Projects/aig-analytics-scrapper/src/engine/'
    #                 'sample_parsed_users_links.csv', names=['idx', 'href'])
    #
    # # TODO drop unnecessary cols with jsons
    # # res_wl = sme.get_usr_wishlist_df(player_id='slowash')
    # # res_rec_played = sme.get_recently_played_games(player_id='slowash')
    # # print(res_rec_played.iloc[0]['ach_completion'])
    # # revs = sme.get_usr_reviews(player_id='015208377707')
    # # print(revs.to_string())
    # # input('sanity check')
    # # games = sme.get_usr_fllwd_games_df(player_id='slowash')
    # # print(games.to_string())
    #
    # # frnds = sme.get_friends_links(player_id='slowash')
    # # print(frnds)
    #
    # # testing ids
    # # sample_ids = ['slowash', '-darkness-', '-daitan', '015208377707']
    # sample_ids = \
    #     ['015208377707', '76561198018113426', '76561198108867803',
    #      '76561197999078663', '76561198014626401', '76561198002681203']
    #
    # sme.parse_desired_players(players_ids=sample_ids)
    # print(sme.games_df.to_string())
    # print(sme.friends_df.to_string())

    # sme.get_all_usrs_friends(
    #     links_for_friends=parsed_users['href'], go_recursive=True)
