from datetime import datetime, time

from dagster import daily_schedule, hourly_schedule, repository

from src.dagster_objects.pipelines.steam_pipelines import \
    process_steam_members_pipeline, process_steam_data_pipeline
from src.dagster_objects.pipelines.proxy_pipelines import proxy_update_pipeline
from src.dagster_objects.pipelines.stock_pipelines import \
    process_wse_stock_data_pipeline
from src.utils.meta_utils import get_run_metadata
# from src.workflows.dagster_example import process_steam_members_pipeline


@hourly_schedule(
    pipeline_name="process_steam_members_pipeline",
    start_date=datetime(2021, 3, 20),
    execution_time=time(0, 5),
    execution_timezone="Etc/UTC",
)
def steam_members_hourly_schedule(date):
    run_cfg = get_run_metadata(
        pipeline_name='process_steam_members_pipeline', schedule_name='hourly')
    return run_cfg


@daily_schedule(
    pipeline_name="process_steam_members_pipeline",
    start_date=datetime(2021, 2, 1),
    execution_time=time(6, 00),
    execution_timezone="Etc/UTC",
)
def steam_members_daily_morning_schedule(date):
    run_cfg = get_run_metadata(
        pipeline_name='process_steam_members_pipeline', schedule_name='daily')
    return run_cfg


@daily_schedule(
    pipeline_name="process_steam_members_pipeline",
    start_date=datetime(2021, 2, 1),
    execution_time=time(12, 00),
    execution_timezone="Etc/UTC",
)
def steam_members_daily_noon_schedule(date):
    run_cfg = get_run_metadata(
        pipeline_name='process_steam_members_pipeline', schedule_name='daily')
    return run_cfg


@daily_schedule(
    pipeline_name="process_steam_members_pipeline",
    start_date=datetime(2021, 2, 1),
    execution_time=time(15, 30),
    execution_timezone="Etc/UTC",
)
def steam_members_daily_evening_schedule(date):
    run_cfg = get_run_metadata(
        pipeline_name='process_steam_members_pipeline', schedule_name='daily')
    return run_cfg


@daily_schedule(
    pipeline_name="process_steam_members_pipeline",
    start_date=datetime(2021, 2, 1),
    execution_time=time(23, 00),
    execution_timezone="Etc/UTC",
)
def steam_members_daily_night_schedule(date):
    run_cfg = get_run_metadata(
        pipeline_name='process_steam_members_pipeline', schedule_name='daily')
    return run_cfg


@daily_schedule(
    pipeline_name="process_wse_stock_data_pipeline",
    start_date=datetime(2021, 2, 1),
    execution_time=time(17, 15),
    execution_timezone="Etc/UTC",
)
def wse_stock_data_daily_schedule(date):
    run_cfg = get_run_metadata(
        pipeline_name='process_wse_stock_data_pipeline', schedule_name='daily')
    return run_cfg


@daily_schedule(
    pipeline_name="process_steam_data_pipeline",
    start_date=datetime(2021, 2, 1),
    execution_time=time(9, 00),
    execution_timezone="Etc/UTC",
)
def morning_process_steam_data_pipeline(date):
    run_cfg = get_run_metadata(
        pipeline_name='process_steam_data_pipeline', schedule_name='daily')
    return run_cfg


@daily_schedule(
    pipeline_name="process_steam_data_pipeline",
    start_date=datetime(2021, 2, 1),
    execution_time=time(18, 00),
    execution_timezone="Etc/UTC",
)
def evening_process_steam_data_pipeline(date):
    run_cfg = get_run_metadata(
        pipeline_name='process_steam_data_pipeline', schedule_name='daily')
    return run_cfg


@daily_schedule(
    pipeline_name="proxy_update_pipeline",
    start_date=datetime(2021, 2, 1),
    execution_time=time(2, 00),
    execution_timezone="Etc/UTC",
)
def proxy_scraping_schedule(date):
    run_config = \
        get_run_metadata(
            pipeline_name='proxy_update_pipeline',
            schedule_name='daily')
    return run_config


@repository
def aig_analytics_repo():
    return [process_steam_members_pipeline, process_steam_data_pipeline,
            process_wse_stock_data_pipeline, proxy_update_pipeline,
            steam_members_daily_morning_schedule, steam_members_daily_noon_schedule,
            steam_members_daily_evening_schedule, steam_members_daily_night_schedule,
            wse_stock_data_daily_schedule, morning_process_steam_data_pipeline,
            evening_process_steam_data_pipeline, proxy_scraping_schedule]
