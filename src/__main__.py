from argparse import ArgumentParser

import pandas as pd

from src.data_uploaders.datamart_connection import DataMartConnection
from src.data_uploaders.db_data_loaders import PostgresDataLoader
from src.data_uploaders.db_data_uploaders import PostgresDataUploader
from src.engine.executor.proxy_engines import PublicProxyEngine
from src.engine.executor.web_request_executors import SingleWebRequestExecutor
from src.engine.microsoft_store_processing_engine import MicrosoftStoreProcessingEngine
from src.engine.steam_members_engine import SteamMembersEngine
from src.engine.steam_processing_engine import SteamProcessingEngine
from src.utils.configs import ApplicationConfig
from src.utils.parsers import str2bool
from src.workflows.scrapper_workflows import ScrappingWorkflows

if __name__ == '__main__':

    arg = ArgumentParser()

    arg.add_argument('--app.run.wse.comp.kp', dest='run_wse_comp_lkp', default=True, type=str2bool)
    arg.add_argument('--app.run.steam.lkp.refresh', dest='run_steam_lkp_refresh', default=True, type=str2bool)
    arg.add_argument('--app.run.steam.lkp', dest='run_steam_lkp', default=True, type=str2bool)
    arg.add_argument('--app.run.steam.data', dest='run_steam_data', default=True, type=str2bool)
    arg.add_argument('--app.run.steam.stats', dest='run_steam_stats', default=True, type=str2bool)
    arg.add_argument('--app.run.ms.data', dest='run_ms_data', default=True, type=str2bool)
    arg.add_argument('--app.run.steam.members', dest='run_steam_members', default=True, type=str2bool)
    arg.add_argument('--app.run.config.file', dest='config_file_location', default='../utils/local-app-never-commit'
                                                                                   '.yaml')

    params = arg.parse_args()

    app_config = ApplicationConfig(cfg_path=params.config_file_location).config

    datamart_connection = DataMartConnection(dbname=app_config['database.games-analytics.database'],
                                             dbuser=app_config['database.games-analytics.user'],
                                             dbhost=app_config['database.games-analytics.ip'],
                                             dbpass=app_config['database.games-analytics.password'],
                                             dbport=app_config['database.games-analytics.port'])
    pd_loader = PostgresDataLoader(datamart_connection=datamart_connection)
    pd_uploader = PostgresDataUploader(datamart_connection=datamart_connection)

    proxy_engine = PublicProxyEngine(datamart_connection=datamart_connection)
    web_executor = SingleWebRequestExecutor(proxy_engine=proxy_engine)

    sw = ScrappingWorkflows(global_cfg=app_config, postgres_data_loader=pd_loader, postgres_data_uploader=pd_uploader)

    # MOVED TO DAGSTER
    # if params.run_wse_comp_lkp:
    #     sdpe = StockDataProcessingEngine(request_execution_engine=web_executor)
    #     sw.stock_data_processing_engine = sdpe
    #     print("Start pulling wse companies data")
    #     sw.process_wse_comp_data()
    #     print("Finished pulling wse companies data")

    # Steam processing
    if params.run_steam_data or params.run_steam_stats or params.run_steam_lkp:
        spe = SteamProcessingEngine()
        sw.steam_processing_engine = spe

        if params.run_steam_members:
            start_time = pd.datetime.now()
            print("STARTING PROCESS: run_steam_members")
            sme = SteamMembersEngine(request_execution_engine=web_executor)
            sw.steam_members_processing_engine = sme
            sw.process_steam_members(
                benchmark_run=False, fresh_members=200, updated_members=100)
            print("FINISHED PROCESS: run_steam_members")
            print("Scrapping {} users took {}."
                  .format(len(sme.parsed_steam_members_urls),
                          (pd.datetime.now() - start_time).total_seconds() / 60))
            # MOVED TO DAGSTER
        # if params.run_steam_lkp_refresh:
        #     print("STARTING PROCESS: run_steam_lkp_refresh")
        #     sw.steam_app_info_lkp_batch_process_refresh(ids_limit_size=10, batch_size_limit=10)
        #     print("FINISHED PROCESS: run_steam_lkp_refresh")
        # if params.run_steam_lkp:
        #     print("STARTING PROCESS: run_steam_lkp")
        #     sw.steam_app_info_lkp_batch_process(ids_limit_size=10, batch_size_limit=10)
        #     print("FINISHED PROCESS: run_steam_lkp")

        # if params.run_steam_data:
        #     print("STARTING PROCESS: run_steam_data")
        #     sw.process_steam_data()
        #     print("FINISHED PROCESS: run_steam_data")
        if params.run_steam_stats:
            print("STARTING PROCESS: run_steam_stats")
            sw.pull_live_stats()
            print("FINISHED PROCESS: run_steam_stats")

    # MS Processing
    if params.run_ms_data:
        print("STARTING PROCESS: run_ms_data")
        sw.microsoft_store_processing_engine = MicrosoftStoreProcessingEngine()
        sw.pull_ms_store_info()
        print("FINISHED PROCESS: run_ms_data")
