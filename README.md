# aig-analytics-scrapper

## Virtualenv installation

1. cd alter_py
2. bash install_alter_py.sh 3.7.9 /home/os/Projects/aig-analytics-scrapper/alter_py/ /home/os/Projects/aig-analytics-scrapper/dev_venv


## Composing application(early alpha)
1. install docker
2. install docker-compose
3. cd <pth/to/>aig_analytics_scraper

4. zbuduj:
   docker build -t aig-analytics-scraper:0.1 .

5. cd dagit-deploy

6. compose application:
   docker-compose up --build -d --force-recreate

7. recreate single service:
   docker-compose up -d --force-recreate --no-deps aig_analytics_scraper
